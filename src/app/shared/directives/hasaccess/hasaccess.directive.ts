
import { Directive, Input, TemplateRef , ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appHasaccess]'
})
export class HasaccessDirective {

 @Input() set appHasaccess(condition : boolean) {
 	
 	///console.log("condition........" + condition);

 	if(condition){
 		this.vRef.createEmbeddedView(this.templateRef);
 	}else {
 		this.vRef.clear();
 	}
 } 
   constructor( private templateRef : TemplateRef<any>
  				, private vRef: ViewContainerRef
  	
  	) {}

}
