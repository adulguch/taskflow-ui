import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';

const routes: Routes = [
    { path: 'datachecks', loadChildren: './datachecks/datachecks.module#DatachecksModule' } , 
  	/*{ path: 'kri', loadChildren: './kri/kri.module#KRIModule' }*/      
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: []
})
export class ReportsModule { }
