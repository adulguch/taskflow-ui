import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatachecksListComponent } from './datachecks-list.component';

describe('DatachecksListComponent', () => {
  let component: DatachecksListComponent;
  let fixture: ComponentFixture<DatachecksListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatachecksListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatachecksListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
