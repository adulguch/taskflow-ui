import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DatachecksListComponent } from './datachecks-list/datachecks-list.component';
import { AuthGuard } from 'app/guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'list',  canActivate: [AuthGuard] },
  { path: 'list', component: DatachecksListComponent,   canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DatachecksRoutingModule { }
