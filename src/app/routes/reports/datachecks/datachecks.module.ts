import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DatachecksRoutingModule } from './datachecks-routing.module';
import { DatachecksListComponent } from './datachecks-list/datachecks-list.component';

@NgModule({
  imports: [
    CommonModule,
    DatachecksRoutingModule
  ],
  declarations: [DatachecksListComponent]
})
export class DatachecksModule { }
