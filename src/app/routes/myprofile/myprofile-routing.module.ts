import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MyprofileComponent } from './myprofile.component';
import { AuthGuard } from 'app/guards/auth.guard';


const routes: Routes = [
  { path: '', redirectTo: 'main',  canActivate: [AuthGuard] },
  { path: 'main', component: MyprofileComponent,  canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyprofileRoutingModule { }
