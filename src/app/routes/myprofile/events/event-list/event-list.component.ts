import { Component, OnInit, NgZone, Input } from '@angular/core';
import { EventService } from 'app/services/event.service';
import { Event } from 'app/model/event';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { User } from 'app/model/user';
import { SelectData } from 'app/model/selected-data';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit {

  @Input() user : User;
  private eventList : Event[];
  private assignedEventList : Event[];
  private tempEventList : Event[];
  private showSpinner: number = 1;
  private errorMessage;
  private success_toaster: any;
  private fail_toaster: any;  
  
  private toasterconfig: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-bottom-right',
    showCloseButton: true
  });


  constructor(private zone: NgZone, 
    private eventService : EventService,
    private toasterService: ToasterService) {

    this.success_toaster = {
      type: 'success',
      title: 'Successful',
      text: 'The record has been updated successfully.'
    };
    this.fail_toaster = {
      type: 'error',
      title: 'Failed',
      text: 'The record was not updated.'
    };

    this.tempEventList = [];
    this.eventList = [];
    this.assignedEventList = [];
  }

  ngOnInit() {
    this.eventService.getFilteredEvents().subscribe((data: SelectData) => {
        this.zone.run(() => {
            this.eventList = data.filteredData;
            this.assignedEventList = data.assignedData;
            this.showSpinner = 0;                
        });
    }, (error) => {
        //console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

    /*this.eventService.getEventsByUser().subscribe((data : Event[]) => {
      this.zone.run(() => {
        this.assignedEventList = data;
        //this.filterEvents();
        this.showSpinner = 0;
      })
    }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });*/
  }

  changeEventList(selectedEvent){
    for(let i = 0; i < this.eventList.length; i++){
      let event = this.eventList[i];
      if(event.id == selectedEvent.value){
        this.assignedEventList.push(event);
        this.eventList.splice(i, 1);
      }
    }
  }

  changeAssignedEventList(selectedEvent){
    for(let i = 0; i < this.assignedEventList.length; i++){
      let event = this.assignedEventList[i];
      if(event.id == selectedEvent.value){
        this.assignedEventList.splice(i, 1);
        this.eventList.push(event);
      }
    }
  }

  selectAllEvents(){
    this.eventList.forEach(event => {this.assignedEventList.push(event)});
    this.eventList = [];
  }

  deselectAllEvents(){
    this.assignedEventList.forEach(event => {this.eventList.push(event)});
    this.assignedEventList = [];
  }

  filterEvents(){
    for(let i = 0; i < this.assignedEventList.length; i++){
      for(let j = 0; j < this.tempEventList.length; j++){   
        if(this.tempEventList[j].id === this.assignedEventList[i].id){
          this.tempEventList.splice(j, 1);
          break;
        }
      }
    }
    this.eventList = this.tempEventList;
  }

  submitEvents(){
    this.showSpinner = 1;
    this.eventService.updateEvents(this.assignedEventList).subscribe(
      entry => this.handleSubmitSuccess(entry),
      error => this.handleSubmitError(error)
      );
  }

  protected handleSubmitSuccess(entry) {
    this.showSpinner = 0; 
    this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text); 
    setTimeout(()=>{
          
    },1500);        
  }

  protected handleSubmitError(error: any) {    
    this.showSpinner = 0;
    //this.submitted = false;
    //console.log(error);
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, error);    
  }
}
