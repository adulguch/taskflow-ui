import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Location } from '@angular/common';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { Http } from '@angular/http';
import { Userprivileges } from 'app/model/userprivileges';
import { PrivilageService } from 'app/services/privilage.service';

import { MyprofileService } from 'app/services/myprofile.service';
import { User } from 'app/model/user';

@Component({
  selector: 'app-myprofile',
  templateUrl: './myprofile.component.html',
  styleUrls: ['./myprofile.component.scss']
})
export class MyprofileComponent implements OnInit {
  private settingActive = -99;
  private updateUserForm: FormGroup;
  private passwordForm: FormGroup;
  private user: User;
  private profile_submitted: boolean = false;
  private pw_submitted: boolean = false;
  private toUpdate;
  private item: any;
  private showSpinner: number = 1;
  private errorMessage;
  private kriPrivilege : Userprivileges;
  
  private success_toaster: any;
  private fail_toaster: any;  
  private toasterconfig: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-bottom-right',
    showCloseButton: true
  });
  
  constructor(private zone: NgZone, 
    private fb: FormBuilder, 
    private router: Router,
    private myprofileService: MyprofileService,
    private _location: Location, 
    private toasterService: ToasterService,
    private http: Http,
    private privilege : PrivilageService) {

    this.success_toaster = {
      type: 'success',
      title: 'Successful',
      text: 'The record has been updated successfully.'
    };
    this.fail_toaster = {
      type: 'error',
      title: 'Failed',
      text: 'The record was not updated.'
    };    

    this.initUser();
    this.kriPrivilege = new Userprivileges();

  }

  private initUser() {
    this.user = new User();

    this.privilege.CheckKRIAccess().subscribe((data : Userprivileges) => {
      this.zone.run(() => {
        this.assignPrivileges(data, this.kriPrivilege);
      });
    });
    
    this.updateUserForm = this.fb.group({
        'userIdPk': [this.user.userIdPk, Validators.required],
        'userName': [this.user.userName, Validators.required],        
        'firstName': [this.user.firstName, Validators.required],
        'lastName': [this.user.lastName, Validators.required],     
        'email': [this.user.email, Validators.compose([Validators.required, CustomValidators.email])],
        'telephone': [this.user.telephone],
    });

    let updatePassword = new FormControl('', Validators.required);
    let confirmPassword = new FormControl('', CustomValidators.equalTo(updatePassword));

    this.passwordForm = this.fb.group({      
        'updatePassword': updatePassword,
        'confirmPassword': confirmPassword
    });

    let password = new FormControl('', Validators.required);
    let certainPassword = new FormControl('', CustomValidators.equalTo(password));

    

  }

  ngOnInit() {
      this.settingActive = 1;
      this.myprofileService.getMyProfile().subscribe((data) => {
        this.zone.run(() => {
          this.user = data;
          this.toUpdate = true;
          //console.log(this.user);
          this.updateUserForm.patchValue(this.user); 
          this.updateUserForm.get('userName').disable();
          this.showSpinner = 0;
        });
      },(error) => {
            // console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });
  }

  private submitUpdateUserForm($ev, value: any) {
    $ev.preventDefault();    
    if (this.updateUserForm.valid) {
      this.profile_submitted = true;
      if (this.toUpdate) {        
        this.showSpinner = 1;
        this.myprofileService.updateMyProfile(this.updateUserForm.value)
          .subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );
      } 
    }
  }
   
  private submitPasswordResetForm($ev, value: any) {
    $ev.preventDefault();
    if (this.passwordForm.valid) {
      this.pw_submitted = true;
      if (this.toUpdate == true) {        
        this.showSpinner = 1;
        this.user.password = this.passwordForm.get('updatePassword').value;     
        // call resetPassword rest api
        this.myprofileService.updateMyPassword(this.user)
          .subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );
        //this.showSpinner = 0;    
        //this.backClicked();  
      } 
    } 
  }

  protected handleSubmitSuccess(entry) {  
    if (this.toUpdate) {
      this.showSpinner = 0;
      this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);      
      setTimeout(()=>{
        this.backClicked();  
      },1500);
    }
    else {
      setTimeout(()=>{
        this.showSpinner = 0;
        this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);        
        this.initUser();
        this.backClicked();        
      },1500);
    }
  }
  protected handleSubmitError(error: any) {
    this.showSpinner = 0;
    this.profile_submitted = false;
    this.pw_submitted = false;
    //console.log(error);
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, error);  
   // this.backClicked();     
  }

  private backClicked() {    
    //  this.router.navigate(['/myprofile/main']);
  }

  assignPrivileges(responseData : Userprivileges, privilege : Userprivileges){
    privilege.view = responseData.view;
    privilege.create = responseData.create;
    privilege.edit = responseData.edit;
    privilege.delete = responseData.delete;
  }

}