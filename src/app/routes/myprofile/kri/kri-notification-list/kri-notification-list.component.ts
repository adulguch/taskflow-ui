import { Component, OnInit, NgZone } from '@angular/core';
import { KriService } from 'app/services/kri.service';
import { Kri } from 'app/model/kri';
import { UserKRIRule } from 'app/model/userKriRule';
import { ToasterService, ToasterConfig } from 'angular2-toaster';

@Component({
  selector: 'app-kri-notification-list',
  templateUrl: './kri-notification-list.component.html',
  styleUrls: ['./kri-notification-list.component.scss']
})
export class KriNotificationListComponent implements OnInit {

  private KRIrules : UserKRIRule;
  private userKriList : UserKRIRule[];
  private showSpinner: number = 1;
  private errorMessage;
  private success_toaster: any;
  private fail_toaster: any; 
  
  private toasterconfig: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-bottom-right',
    showCloseButton: true
  });

  constructor(private zone: NgZone, 
    private toasterService: ToasterService,
    private kriService : KriService) { 
      this.success_toaster = {
        type: 'success',
        title: 'Successful',
        text: 'The record has been updated successfully.'
      };
      this.fail_toaster = {
        type: 'error',
        title: 'Failed',
        text: 'The record was not updated.'
      };

      this.userKriList = [];
    }

  ngOnInit() {
    this.kriService.getUserKriSubscriptionList().subscribe((data: UserKRIRule) => {
      this.zone.run(() => {
          this.KRIrules = data;
          this.addExistingKriRules(this.KRIrules);
          this.showSpinner = 0;
          console.log(this.KRIrules);                
      });
      }, (error) => {
          //console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });
  }

  submitEvents(){
    this.showSpinner = 1;
    

     this.kriService.userKriSubscribe(this.userKriList).subscribe(
      entry => this.handleSubmitSuccess(entry),
      error => this.handleSubmitError(error)
    );
     
  }

  onKRISelection(event, rule){
    let checked = event.target.checked;
    let value = event.target.value;
    console.log(rule);
    if(checked){
      this.addUserKRIRule(rule, value);
    }else{
      this.removeUserKRIRule(rule, value);
    }

    console.log(this.userKriList);
  }

  addUserKRIRule(rule, value){
    var userKri : UserKRIRule = new UserKRIRule();
    for(let i = 0; i < this.userKriList.length; i++){
      if(this.userKriList[i].kriRuleId == rule.kriRuleId){
        if(value == 'pass')
          this.userKriList[i].rulePass = 1;
        else if(value == 'fail')
          this.userKriList[i].ruleFail = 1;
        return;
      }
    }
      
    userKri.kriRuleId = rule.kriRuleId;
    if(value == 'pass')
      userKri.rulePass = 1;
    else if(value == 'fail')
      userKri.ruleFail = 1;
    this.userKriList.push(userKri);
  }

  removeUserKRIRule(rule, value){
    for(let i = 0; i < this.userKriList.length; i++){
      if(this.userKriList[i].kriRuleId == rule.kriRuleId){
        if(value == 'pass')
          this.userKriList[i].rulePass = 0;
        else if(value == 'fail')
          this.userKriList[i].ruleFail = 0;
        if(this.userKriList[i].rulePass == 0 && this.userKriList[i].ruleFail == 0)
          this.userKriList.splice(i, 1);
      }
    }
  }

  protected handleSubmitSuccess(entry) {
    this.showSpinner = 0; 
    this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text); 
    setTimeout(()=>{
          
    },1500);        
  }

  protected handleSubmitError(error: any) {    
    this.showSpinner = 0;
    //this.submitted = false;
    //console.log(error);
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, error);    
  }

  addExistingKriRules(kriRules){
    for(let i = 0; i < kriRules.length; i++){
      if(kriRules[i].ruleFail == 1 || kriRules[i].rulePass == 1)
        this.userKriList.push(kriRules[i]);
    }
  }

}
