import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyprofileRoutingModule } from './myprofile-routing.module';
import { SharedModule } from 'app/shared/shared.module';
import { MyprofileComponent } from './myprofile.component';
import { MyprofileService } from 'app/services/myprofile.service';
import { EventService } from 'app/services/event.service';
import { EventListComponent } from './events/event-list/event-list.component';
import { KriService } from 'app/services/kri.service';
import { KriNotificationListComponent } from './kri/kri-notification-list/kri-notification-list.component';
import { PrivilageService } from 'app/services/privilage.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MyprofileRoutingModule
  ],
  declarations: [MyprofileComponent, EventListComponent, KriNotificationListComponent],
  providers: [MyprofileService, EventService, KriService, PrivilageService]
})
export class MyprofileModule { }
