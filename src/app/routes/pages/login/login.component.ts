import { Router } from '@angular/router';
import { AuthenticationService } from './../../../services/authentication.service';
import { Component, OnInit, NgZone} from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { MenuService } from '../../../core/menu/menu.service';
import { UserService } from 'app/services/user.service';
import { User } from 'app/model/user';
import { Role } from 'app/model/role';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { menu } from '../../../routes/menu';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    valForm: FormGroup;
    loading = false;
    errorMsg = '';
    private showSpinner = 0;
    private user: User;
    private errorMessage;
    private success_toaster: any;
    private fail_toaster: any;  
    private toasterconfig: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-bottom-right',
    showCloseButton: true
    });

    menu: { text: string,heading?: boolean
                    ,link?: string
                    ,elink?: string
                    ,target?: string
                    ,icon?: string
                    ,alert?: string
                    ,submenu?: Array<any>} []; 

    constructor(public settings: SettingsService
        , fb: FormBuilder
        , public menuService: MenuService
        , public authenticationService: AuthenticationService
        , public router: Router
        //, private userService: UserService
        , private zone: NgZone
        , private toasterService: ToasterService
     
        ) {
    
        this.valForm = fb.group({
            'username': [null, Validators.compose([Validators.required])],
            'password': [null, Validators.required]
        });

    this.initUser();

    }

    private initUser() {
        this.user = new User();    
    };


    submitForm($ev, value: any) {
        this.showSpinner = 1;
        $ev.preventDefault();
        for (let c in this.valForm.controls) {
            this.valForm.controls[c].markAsTouched();
        }
        if (this.valForm.valid) {

            this.authenticationService.login(value.username, value.password ).subscribe(
                result => {
                    if (result === true) {
                        this.showSpinner = 0;
                        //console.log("Called construct Menu");
                       
                        // Get User Details
                        console.log("Calling getAuthUserDetails... "); 
                        this.authenticationService.getAuthUserDetails().subscribe((data) => {
                            this.zone.run(() => {
                              this.user = data; 
                              this.showSpinner = 0;
                              //console.log('Pass' + data.userName);
                                
                            });
                          },(error) => {                                
                                console.log(error);
                                this.errorMessage = error;
                                this.showSpinner = 0;
                                this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
                          });

                        // Get user Menu
                        console.log("Calling Constructmenu... ");                         
                        this.menuService.constructmenu().subscribe(
                        result => {
                            this.menuService.addMenu(result);
                        });                        
                        //this.router.navigate(['/user']);
                        
                        this.router.navigate(['/dashboard']);
                    } else {
                        this.errorMsg= 'Username or password incorrect';
                        this.loading =false;
                    }
                }  , (error) => {
                     
                     this.showSpinner = 0;                    
                     console.log("error " + error);
                     if (error  === 'Unauthorized') { 
                         this.errorMsg= 'Username or password incorrect';
                         this.router.navigateByUrl('/login');

                     }
                    }
                );
        }
    }

    ngOnInit() {
//        this.authenticationService.logout();
    }

}
