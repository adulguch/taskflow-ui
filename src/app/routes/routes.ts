import { AuthGuard } from './../guards/auth.guard';

import { LayoutComponent } from '../layout/layout.component';

import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { RecoverComponent } from './pages/recover/recover.component';
import { LockComponent } from './pages/lock/lock.component';
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { Error404Component } from './pages/error404/error404.component';
import { Error500Component } from './pages/error500/error500.component';

export const routes = [

    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule', canActivate: [AuthGuard] },     
            { path: 'study', loadChildren: './study/study.module#StudyModule', canActivate: [AuthGuard] },                 
            { path: 'deliverable', loadChildren: './deliverable/deliverable.module#DeliverableModule', canActivate: [AuthGuard] },                             
            { path: 'task', loadChildren: './task/task.module#TaskModule', canActivate: [AuthGuard] },                             
            { path: 'issue-log', loadChildren: './issue-log/issue-log.module#IssueLogModule', canActivate: [AuthGuard] },                             
            { path: 'administration', loadChildren: './administration/administration.module#AdministrationModule', canActivate: [AuthGuard] },
            { path: 'myprofile', loadChildren: './myprofile/myprofile.module#MyprofileModule', canActivate: [AuthGuard] },
            { path: 'notification', loadChildren: './notification/notification.module#NotificationModule', canActivate: [AuthGuard] },       
            { path: 'reports', loadChildren: './report/report.module#ReportModule', canActivate: [AuthGuard] }
            
        ]

    },
 
    // Not lazy-loaded routes
    { path: 'login', component: LoginComponent },
    // { path: 'register', component: RegisterComponent },
    // { path: 'recover', component: RecoverComponent },
    // { path: 'lock', component: LockComponent },
    // { path: 'maintenance', component: MaintenanceComponent },
    // { path: '404', component: Error404Component },
    // { path: '500', component: Error500Component },

    // Not found
    { path: '**', redirectTo: 'dashboard' }

];
