import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliverableRoutingModule } from './deliverable-routing.module';
import { DeliverableListComponent } from './deliverable-list/deliverable-list.component';
import { DeliverableDetailsComponent } from './deliverable-details/deliverable-details.component';
import { PrivilageService } from 'app/services/privilage.service';
import { DataTableModule } from 'angular2-datatable';
import { SharedModule } from 'app/shared/shared.module';
import { DeliverableService } from '../../services/deliverable.service';
import { DeliverableInfoComponent } from './deliverable-info/deliverable-info.component';
import { StudyService } from '../../services/study.service';

@NgModule({
  imports: [
    CommonModule,
    DeliverableRoutingModule,
    DataTableModule,
    SharedModule
  ],
  declarations: [DeliverableListComponent, DeliverableDetailsComponent, DeliverableInfoComponent],
  providers: [DeliverableService , PrivilageService, StudyService]
})
export class DeliverableModule { }
