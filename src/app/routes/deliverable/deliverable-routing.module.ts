import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeliverableListComponent } from './deliverable-list/deliverable-list.component';
import { DeliverableDetailsComponent } from './deliverable-details/deliverable-details.component';
import { AuthGuard } from 'app/guards/auth.guard';
import { DeliverableInfoComponent } from './deliverable-info/deliverable-info.component';


const routes: Routes = [
	{ path: '', redirectTo: 'list' , canActivate: [AuthGuard] },
  	{ path: 'details', component: DeliverableDetailsComponent, canActivate: [AuthGuard] },
	  { path: 'list', component: DeliverableListComponent, canActivate: [AuthGuard] },
	  { path: 'info', component: DeliverableInfoComponent, canActivate: [AuthGuard] }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliverableRoutingModule { }
