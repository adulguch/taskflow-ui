import { Component, OnInit, NgZone, ViewChild, Renderer } from '@angular/core';
import { CommanConfig } from 'app/routes/CommanConfig';
import * as _ from 'lodash';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { DeliverableService } from '../../../services/deliverable.service';
import { ToasterService } from 'angular2-toaster';
import { PrivilageService } from 'app/services/privilage.service';
import { Userprivileges } from 'app/model/userprivileges';
import { Deliverable } from 'app/model/deliverable';
import * as $ from 'jquery' ;


@Component({
  selector: 'app-deliverable-list',
  templateUrl: './deliverable-list.component.html',
  styleUrls: ['./deliverable-list.component.scss']
})
export class DeliverableListComponent extends CommanConfig implements OnInit {
  private writeAccess : Boolean = false ;
  private deleteAccess : Boolean = false ;
  private createAccess : Boolean = false ;
  private singleData;
  private originalData;
  private selectedRow: number;
  private errorMessage: string;        
  public success_toaster: any;
  private showSpinner: number = 1;
  private totalRecords: number = 0;
  private delDelv: Deliverable ;
  start: number;
  pressed: boolean;
  startX: number;
  startWidth: number;
	
  
  @ViewChild('deleteModal') deleteModal: any;

  constructor(private zone: NgZone, 
    private http: Http, 
    private router: Router, 
    private deliverableService: DeliverableService, 
    public toasterService: ToasterService,
    private privilage: PrivilageService,
    public renderer: Renderer

    , ) {
   
   super(toasterService);  

  }

  ngOnInit() :void {              
    this.deliverableService.getAll().subscribe((data) => {
        this.zone.run(() => {
            this.singleData = data;
            this.originalData = this.singleData;
            this.showSpinner = 0;
            this.totalRecords = this.singleData.length;                
        });
    }, (error) => {
        //console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

    this.privilage.CheckDeliverableAccess().subscribe((data: Userprivileges) => {
      this.zone.run(() => {
          if(data.edit === true ){
              this.writeAccess = true ;                
          }if(data.delete === true){
              this.deleteAccess = true ;                
          }
          if(data.create === true){
              this.createAccess = true ;                
          }
      });
  }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
  });
}

  public setClickedRow(index: number) {
    this.selectedRow = index;
  }
  public editRow(item) {    	
    const id = { 'Id': item.deliverableId }
  this.router.navigate(['/deliverable/details',id], { queryParams: id, skipLocationChange: true});
  }
  public addNew() {
    this.router.navigate(['/deliverable/details']);
  }

public removeItem(item: Deliverable) { 
       const id = { 'Id': item.deliverableId }
      this.router.navigate(['/deliverable/info',id], { queryParams: id, skipLocationChange: true});   
  }

private delete() {
        // This method deletes the selected record.
        this.deleteModal.hide();
        this.showSpinner = 1;
        this.deliverableService.delete(this.delDelv.deliverableId).subscribe((data) => {
            this.zone.run(() => {            
                    this.showSpinner = 0;
                    this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);
                    this.singleData = _.filter(this.originalData, (elem) => elem != this.delDelv);                                       
                    this.totalRecords = this.singleData.length;
                    this.originalData = this.singleData;
                });
            }, (error) => {
              // console.log(error);
              this.errorMessage = error;
              this.showSpinner = 0;
              this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
            }
        );
    }  
    
  public refresh() {
    this.showSpinner = 1;
    this.deliverableService.getAll().subscribe((data) => {
        this.zone.run(() => {
            this.singleData = data;
            this.originalData = this.singleData;
            this.showSpinner = 0;
            this.totalRecords = this.singleData.length;
        });
    }, (error) => {
        //console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });
  }
  private onQuickFilterChanged($event) {
    console.log($event.key);
    console.log($event.target.value);         
    if ($event.target.value) {
        this.singleData = this.originalData.filter(item => {
            return Object.keys(item).some(keyName => {                     
                if (item[keyName] !== null) {
                    return item[keyName].toString().match(new RegExp($event.target.value, 'i'));
                }
            });
        });
    } else {
        this.singleData = this.originalData;
    }
  }  
  
  onMouseDown(event) {
    this.start = event.target;
    this.pressed = true;
    this.startX = event.x;
    this.startWidth = $(this.start).parent().width();
    this.initResizableColumns();
  }

  private initResizableColumns() {
    this.renderer.listenGlobal('body', 'mousemove', (event) => {
       if (this.pressed) {
         const width = this.startWidth + (event.x - this.startX);
         $(this.start).parent().css({ 'min-width': width, 'max-   width': width });
         const index = $(this.start).parent().index() + 1;
         $('.glowTableBody tr td:nth-child(' + index + ')').css({ 'min-width': width, 'max-width': width });
       }
    });

    this.renderer.listenGlobal('body', 'mouseup', (event) => {
      if (this.pressed) {
        this.pressed = false;
      }
    });
  }
  
}
