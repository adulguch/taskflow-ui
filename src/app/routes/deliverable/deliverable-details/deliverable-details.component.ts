import { Component, OnInit, NgZone } from '@angular/core';
import { CommanConfig } from '../../CommanConfig';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Deliverable } from '../../../model/deliverable';
import { ActivatedRoute, Router } from '@angular/router';
import { DeliverableService } from '../../../services/deliverable.service';
import { ToasterService } from 'angular2-toaster';
import { Http } from '@angular/http';

@Component({
  selector: 'app-deliverable-details',
  templateUrl: './deliverable-details.component.html',
  styleUrls: ['./deliverable-details.component.scss']
})
export class DeliverableDetailsComponent extends CommanConfig implements OnInit {

  private settingActive = -99;
  private valForm: FormGroup;
  private passwordForm: FormGroup;
  private activeStudyForm: FormGroup;
  private deliverable: Deliverable;
  private submitted: boolean = false;
  private toUpdate;
  private item: any;
  private showSpinner: number = 1;
  private errorMessage;
    

  constructor(
    private zone: NgZone, 
    private fb: FormBuilder, 
    private route: ActivatedRoute,
    private router: Router,
    private deliverableService: DeliverableService,    
    // private _location: Location,
    public toasterService: ToasterService) {
    
    super(toasterService);  
    this.initDeliverable();

    
   }
   private initDeliverable() {
    this.deliverable = new Deliverable();
    
    
    this.valForm = this.fb.group({        
        'deliverableId': [this.deliverable.deliverableId],
        'deliverableName': [this.deliverable.deliverableName, Validators.required],  
        'deliverableCode': [this.deliverable.deliverableCode, Validators.required], 
        'deliverableType':[this.deliverable.deliverableType, Validators.required],
        'scope':[this.deliverable.scope, Validators.required],
        'deliverableDescription':[this.deliverable.deliverableDescription, Validators.required],
        });
  }

  ngOnInit() {
    this.showSpinner = 1;    
    this.item = this.route.snapshot.params;
    if (this.item.Id) {      
      this.settingActive = 1;
      this.deliverableService.getDeliverableById(this.item.Id).subscribe((data) => {
        this.zone.run(() => {
          this.deliverable = data;
          this.toUpdate = true;
          this.valForm.patchValue(this.deliverable);
          this.showSpinner = 0;
        });
      },(error) => {
            // console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });

    }
    this.showSpinner = 0;
  }

  private submitCreateDeliverableForm($ev, value: any) {
    $ev.preventDefault();    

    if (this.valForm.valid) {
      this.submitted = true;
      if (this.toUpdate) {        
        this.showSpinner = 1;  
        this.deliverableService.update(this.item.Id, this.valForm.value)
        .subscribe(
        entry => this.handleSubmitSuccess(entry),
        error => this.handleSubmitError(error)
        );
      }else{
        this.deliverableService.create(this.valForm.value)
          .subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );
        }
      }
      
    else{
      this.toasterService.pop(this.required_validation_toaster.type, this.required_validation_toaster.title, this.required_validation_toaster.text);
    }
  }

  
   private submitDeleteStudy() {
        this.deliverableService.deleteDeliverable(this.deliverable.deliverableId, this.deliverable)
          .subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );
        this.showSpinner = 0;    
        this.backClicked();      
   }
   

  protected handleSubmitSuccess(entry) {  
    if (this.toUpdate) {
      this.showSpinner = 0;
      this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);      
      setTimeout(()=>{
        this.backClicked();  
      },1500);
    }
    else {
      this.showSpinner = 0;
      this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);        
      setTimeout(()=>{
        this.initDeliverable();
        this.backClicked();        
      },1500);
    }
  }
  protected handleSubmitError(error: any) {
    this.showSpinner = 0;
    this.submitted = false;
    //console.log(error);
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, error);      
  }

  private backClicked() {    
    this.router.navigate(['/deliverable/list']);
  }


}
