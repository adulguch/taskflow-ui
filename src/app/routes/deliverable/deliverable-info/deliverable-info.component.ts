import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { CommanConfig } from 'app/routes/CommanConfig';
import { Deliverable } from '../../../model/deliverable';
import { Http } from '../../../../../node_modules/@angular/http';
import { Router, ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { DeliverableService } from '../../../services/deliverable.service';
import { ToasterService } from '../../../../../node_modules/angular2-toaster';
import { PrivilageService } from '../../../services/privilage.service';
import { StudyService } from 'app/services/study.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-deliverable-info',
  templateUrl: './deliverable-info.component.html',
  styleUrls: ['./deliverable-info.component.scss']
})
export class DeliverableInfoComponent extends CommanConfig implements OnInit {

  private item: any ;
  private writeAccess : Boolean = false ;
  private deleteAccess : Boolean = false ;
  private createAccess : Boolean = false ;
  private singleData;
  private originalData;
  private selectedRow: number;
  private errorMessage: string;        
  public success_toaster: any;
  private showSpinner: number = 1;
  private totalRecords: number = 0;
  private totalTaskRecords: number = 0;
  private delDeliverable : Deliverable ; 
  private studyList ;    

  @ViewChild('deleteModal') deleteModal: any;
  taskList: any;

    constructor(private zone: NgZone, 
    private http: Http, 
    private router: Router, 
    private deliverableService: DeliverableService, 
    private studyService: StudyService, 
    public toasterService: ToasterService,
    private privilage: PrivilageService,
    private route: ActivatedRoute , 
  ) {
  
  super(toasterService);  

    }
  ngOnInit() {
    this.item = this.route.snapshot.params;
    this.studyService.getStudiesByDeliverableId(this.item.Id).subscribe((data) => {
          this.zone.run(() => {
              this.studyList = data;
              this.showSpinner = 0;
              this.totalRecords = this.studyList.length;                
          });
      }, (error) => {
          //console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });

      this.deliverableService.getTasksByDeliverableId(this.item.Id).subscribe((data) => {
        this.zone.run(() => {
            this.taskList = data;
            this.showSpinner = 0;
            this.totalTaskRecords = this.taskList.length;                
        });
    }, (error) => {
        //console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });
  }


  private submitDeleteStudy() {
        this.deleteModal.show();
    }  

    private delete() {
      // This method deletes the selected record.
      this.deleteModal.hide();
      this.showSpinner = 1;
      console.log(this.item.Id);
      this.deliverableService.delete(this.item.Id).subscribe((data) => {
        this.zone.run(() => {            
                this.showSpinner = 0;
                this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);
                this.singleData = _.filter(this.originalData, (elem) => elem != this.delDeliverable);                                       
                this.totalRecords = this.singleData.length;
                this.originalData = this.singleData;
            });
          }, (error) => {
            // console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
          }
      );
      setTimeout(()=>{
        this.backClicked();        
      },1500);    
  }   
  
  private backClicked() {    
    this.router.navigate(['/deliverable/list']);
  }

}
