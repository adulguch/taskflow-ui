import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliverableInfoComponent } from './deliverable-info.component';

describe('DeliverableInfoComponent', () => {
  let component: DeliverableInfoComponent;
  let fixture: ComponentFixture<DeliverableInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverableInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverableInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
