import { ToasterService, ToasterConfig  , BodyOutputType } from 'angular2-toaster';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload/ng2-file-upload';

export class FileUploadsConfig {

    public uploader: FileUploader;

    public setup(url: string) {

        const authHeader: Array<{
            name: string;
            value: string;
        }> = [];

        const currentUser = JSON.parse(localStorage.getItem('currentUser'));

        if (currentUser && currentUser.token) {
            authHeader.push({name: 'Authorization', value: 'Bearer ' + currentUser.token});
        }
        const uploadOptions = <FileUploaderOptions>{headers : authHeader};

        this.uploader = new FileUploader({ url:  url});
        this.uploader.setOptions(uploadOptions);

        return this.uploader;
    }
}