import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportRoutingModule } from './report-routing.module';
import { StudyReportComponent } from './study-report/study-report.component';
import { StudyService } from 'app/services/study.service';
import { PrivilageService } from 'app/services/privilage.service';
import { DataTableModule } from '../../../../node_modules/angular2-datatable/lib/DataTableModule';
import { SharedModule } from 'app/shared/shared.module';
import { DeliverableReportComponent } from './deliverable-report/deliverable-report.component';
import { DeliverableService } from '../../services/deliverable.service';
import { TreeModule } from 'angular-tree-component';
import { TaskService } from '../../services/task.service';
import { ImpactAssessmentComponent } from './impact-assessment/impact-assessment.component';

@NgModule({
  imports: [
    CommonModule,
    ReportRoutingModule,
    DataTableModule,
    TreeModule,
    SharedModule
  ],
  declarations: [StudyReportComponent, DeliverableReportComponent, ImpactAssessmentComponent],
  providers: [StudyService , PrivilageService, DeliverableService, TaskService]
})
export class ReportModule { }
