import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudyReportComponent } from './study-report/study-report.component';
import { AuthGuard } from 'app/guards/auth.guard';
import { DeliverableReportComponent } from './deliverable-report/deliverable-report.component';
import { ImpactAssessmentComponent } from './impact-assessment/impact-assessment.component';


const routes: Routes = [

  { path: 'studyReport', component: StudyReportComponent, canActivate: [AuthGuard] },
  { path: 'deliverableReport', component: DeliverableReportComponent, canActivate: [AuthGuard] },
  { path: 'impactAssessment', component: ImpactAssessmentComponent, canActivate: [AuthGuard] },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
