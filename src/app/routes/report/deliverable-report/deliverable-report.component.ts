import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { ToasterService } from '../../../../../node_modules/angular2-toaster';
import { Router } from '../../../../../node_modules/@angular/router';
import { PrivilageService } from '../../../services/privilage.service';
import { DeliverableService } from '../../../services/deliverable.service';
import { CommanConfig } from 'app/routes/CommanConfig';
import { TaskSearch } from 'app/model/task-search';
import { TaskService } from 'app/services/task.service';

@Component({
  selector: 'app-deliverable-report',
  templateUrl: './deliverable-report.component.html',
  styleUrls: ['./deliverable-report.component.scss']
})
export class DeliverableReportComponent extends CommanConfig  implements OnInit {

  private deliverableList;
  private showSpinner: number = 1;
  private showTaskSpinner: number = 1;
  private errorMessage: string;
  private valForm: FormGroup;
  private id;
  private selectedDeliverableID;
  private deliverableStats: any;
  private studyStats: any;
  private studyTimeStats: any;
  private tableClicked: boolean = false;
  private taskList;
  private filteredTaskList;
  private totalRecords: number = 0;
  private taskSearch: TaskSearch;

  constructor( private deliverableService: DeliverableService,
    private zone: NgZone,
    private taskService: TaskService,
    public toasterService: ToasterService,
    private router: Router,
    private fb: FormBuilder,
    private privilage : PrivilageService,) 
      { 
        super(toasterService);
        this.init();
      }

      init(){
        this.valForm = this.fb.group({        
          'deliverableId': [this.id, Validators.required]
        });
      }

  ngOnInit() {
    this.deliverableService.getAll().subscribe((data) => {
      this.zone.run(() => {
          this.deliverableList = data;
          this.showSpinner = 0;            
      });
     }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

  }

  onDeliverableChange(deliverableId){
    this.showSpinner = 1; 
    this.tableClicked = false;
    this.totalRecords = 0;
    this.showTaskSpinner = 0;
    this.selectedDeliverableID = deliverableId;
    this.deliverableService.getDeliverableStatById(deliverableId).subscribe((data) => {
      this.zone.run(() => {
          this.deliverableStats = data;
          this.studyStats  = this.deliverableStats.studyStats;
          this.studyTimeStats  = this.deliverableStats.studyStats;
          console.log(this.studyTimeStats);
          this.showSpinner = 0;            
      });
     }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });
  }


  public getTask(taskType, studyId, combineStatus) {
    this.tableClicked = true;
    this.showTaskSpinner = 1;
    var deliverableId = this.valForm.get('deliverableId').value;
    this.taskSearch = new TaskSearch();
    this.taskSearch.studyId = studyId;
    this.taskSearch.deliverableId = deliverableId;
    this.taskSearch.type = taskType;
    this.taskSearch.combineStatus = combineStatus;
  
    // console.log("Task Serach",this.taskSearch);
  
    this.taskService.getFilteredTasks(this.taskSearch).subscribe((data) => {
      this.zone.run(() => {
        this.showTaskSpinner = 1;
          this.filteredTaskList = data;
          this.totalRecords = this.filteredTaskList.length;
          this.showTaskSpinner = 0;
  
      });
  }, (error) => {
      console.log(error);
      this.errorMessage = error;
      this.showTaskSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
  });
  
  }
  
  public getFilteredTask(taskType, studyId, totalEstimationTime, totalActualTime ) {
    this.tableClicked = true;
    this.showTaskSpinner = 1;
    var deliverableId = this.valForm.get('deliverableId').value; 
    this.taskSearch = new TaskSearch();
    this.taskSearch.studyId = studyId;
    this.taskSearch.deliverableId = deliverableId;
    this.taskSearch.type = taskType;
    // this.taskSearch.totalEstimationTime = totalEstimationTime;
    // this.taskSearch.totalActualTime = totalActualTime;
  
    // console.log("Task Serach",this.taskSearch);
  
    this.taskService.getFilteredTasks(this.taskSearch).subscribe((data) => {
      this.zone.run(() => {
        this.showTaskSpinner = 1;
          this.filteredTaskList = data;
          this.totalRecords = this.filteredTaskList.length;
          this.showTaskSpinner = 0;
  
      });
  }, (error) => {
      console.log(error);
      this.errorMessage = error;
      this.showTaskSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
  });
  
  } 
  public editRow(item) {
    this.router.navigate(['/task/details-read/' + item.taskId ]);
   }

}
