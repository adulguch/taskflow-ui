import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliverableReportComponent } from './deliverable-report.component';

describe('DeliverableReportComponent', () => {
  let component: DeliverableReportComponent;
  let fixture: ComponentFixture<DeliverableReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliverableReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliverableReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
