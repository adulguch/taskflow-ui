import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyReportComponent } from './study-report.component';

describe('StudyReportComponent', () => {
  let component: StudyReportComponent;
  let fixture: ComponentFixture<StudyReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
