import { Component, OnInit, NgZone } from '@angular/core';
import { StudyService } from 'app/services/study.service';
import { ToasterService } from '../../../../../node_modules/angular2-toaster/src/toaster.service';
import { PrivilageService } from 'app/services/privilage.service';
import { CommanConfig } from 'app/routes/CommanConfig';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '../../../../../node_modules/@angular/forms';
import { TaskService } from 'app/services/task.service';
import { DeliverableService } from 'app/services/deliverable.service';
import { Task } from 'app/model/task';
import { TreeNode } from 'angular-tree-component/dist/models/tree-node.model';
import { TaskSearch } from 'app/model/task-search';

@Component({
  selector: 'app-study-report',
  templateUrl: './study-report.component.html',
  styleUrls: ['./study-report.component.scss']
})
export class StudyReportComponent extends CommanConfig implements OnInit {


  private studyList;
  private showSpinner: number = 1;
  private showTaskSpinner: number = 1;
  private errorMessage: string;
  private valForm: FormGroup;
  private selectedRow: number;
  private id;
  private selectedStudyID;
  private studyStats: any;
  private deliverableStats2 = [];
  private deliverableStats: any;
  private studySelected : boolean = false;
  private tableClicked: boolean = false;
  private parentTaskList = [];
  private subTaskList = [];
  private childTaskList = [];
  private taskList;
  private filteredTaskList;
  private totalRecords: number = 0;
  private taskSearch: TaskSearch;

  // private showTaskSpinner: number = 1;
  private studyId;
  private deliverableId;
  private deliverablesList;
  private allDeliverablesList;
  private assigndeliverablesList;
  private nodes; 
  private nodes2; 
  private childNodes ; 
  private options;

  constructor(private studyService : StudyService,
    private deliverableService : DeliverableService,
    private taskService: TaskService,
    private zone: NgZone,
    public toasterService: ToasterService,
    private router: Router,
    private fb: FormBuilder,
    private privilage : PrivilageService,) 
      { 
        super(toasterService);
        this.init();
      }

      init(){
        this.valForm = this.fb.group({        
          'studyId': [this.id, Validators.required]
        });
      }

  ngOnInit() {

    this.studyService.getAll().subscribe((data) => {
      this.zone.run(() => {
          this.studyList = data;
          this.showSpinner = 0;            
      });
     }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

  }

  onStudyChange(studyId){
    this.showSpinner = 1; 
    this.tableClicked = false;
    this.totalRecords = 0;
    this.showTaskSpinner = 0;
    this.selectedStudyID = studyId;
    this.studySelected = true;
    this.studyService.getStudyStatById(studyId).subscribe((data) => {
      this.zone.run(() => {
          this.studyStats = data;
          this.deliverableStats  = this.studyStats.deliverableState;
          this.deliverableStats2 = this.studyStats.deliverableState;
          this.showSpinner = 0;            
      });
     }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

    this.parentTaskList = [];
  this.childTaskList = [];
  this.subTaskList = [];
  this.nodes = [];
  this.studyService.getTasksByStudyId(studyId).subscribe((data) => {
     this.zone.run(() => {
     this.taskList = data;
     this.taskList.forEach(element => {
       if(element.parentId != null){
        this.subTaskList.push(element)
          this.options = {
            getChildren: (node:TreeNode) => {  
              this.childTaskList = [];
                this.subTaskList.forEach(element => {
                  if(element.parentId == node.id){
                    this.childTaskList.push(element)
                  }
                });   
              return this.getTreeDate(this.childTaskList);  
            }
          }
      }
       else{
        if(element.parentId == null){
        this.parentTaskList.push(element)
        this.nodes = this.getTreeDate(this.parentTaskList);
        }
       }
       this.showSpinner = 0;
     });
     this.showSpinner = 0;

    });
  }, (error) => {
      console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
  });
}

extractNode (task : Task){
  return Object.assign({} , { 
                   name : task.name,
                   id: task.taskId, 
                   hasChildren: true } );
  
  }

private getTreeDate(data : any) {
	return this.convertTree(data) ;  
}

private convertTree(data: Array<Task> ){
	return Array.from(data).map( (a) => 
			Object.assign({} , { 
                 name : '#'+ a.taskId + ': ' + a.type + ': ' + a.name,
								 id: a.taskId, 
								 hasChildren: true }
              )
          );
}

private taskById(taskId) {
  const id = { 'taskId': taskId }
  this.router.navigate(['/task/details-read/' + taskId]);

}

public setClickedRow(index :number) {
  this.selectedRow = index;
}


public getTask(taskType, deliverableId, combineStatus) {
  this.tableClicked = true;
  this.showTaskSpinner = 1;
  var studyId = this.valForm.get('studyId').value;
  this.taskSearch = new TaskSearch();
  this.taskSearch.studyId = studyId;
  this.taskSearch.deliverableId = deliverableId;
  this.taskSearch.type = taskType;
  this.taskSearch.combineStatus = combineStatus;

  // console.log("Task Serach",this.taskSearch);

  this.taskService.getFilteredTasks(this.taskSearch).subscribe((data) => {
    this.zone.run(() => {
      this.showTaskSpinner = 1;
        this.filteredTaskList = data;
        this.totalRecords = this.filteredTaskList.length;
        this.showTaskSpinner = 0;

    });
}, (error) => {
    console.log(error);
    this.errorMessage = error;
    this.showTaskSpinner = 0;
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
});

}

public getFilteredTask(taskType, deliverableId, totalEstimationTime, totalActualTime ) {
  this.tableClicked = true;
  this.showTaskSpinner = 1;
  var studyId = this.valForm.get('studyId').value; 
  this.taskSearch = new TaskSearch();
  this.taskSearch.studyId = studyId;
  this.taskSearch.deliverableId = deliverableId;
  this.taskSearch.type = taskType;
  // this.taskSearch.totalEstimationTime = totalEstimationTime;
  // this.taskSearch.totalActualTime = totalActualTime;

  // console.log("Task Serach",this.taskSearch);

  this.taskService.getFilteredTasks(this.taskSearch).subscribe((data) => {
    this.zone.run(() => {
      this.showTaskSpinner = 1;
        this.filteredTaskList = data;
        this.totalRecords = this.filteredTaskList.length;
        this.showTaskSpinner = 0;

    });
}, (error) => {
    console.log(error);
    this.errorMessage = error;
    this.showTaskSpinner = 0;
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
});

} 
public editRow(item) {
  this.router.navigate(['/task/details-read/' + item.taskId ]);
 }
}
