import { Component, OnInit, NgZone } from '@angular/core';
import { CommanConfig } from 'app/routes/CommanConfig';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DeliverableService } from 'app/services/deliverable.service';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { PrivilageService } from '../../../services/privilage.service';
import { TaskService } from 'app/services/task.service';

@Component({
  selector: 'app-impact-assessment',
  templateUrl: './impact-assessment.component.html',
  styleUrls: ['./impact-assessment.component.scss']
})
export class ImpactAssessmentComponent extends CommanConfig implements OnInit {

  private deliverableList;
  private showSpinner: number = 1;
  private errorMessage: string;
  private valForm: FormGroup;
  private id;
  private selectedDeliverableId;
  private deliverableStats: any;
  private impactedDeliverableTaskList: any;
  private impactedTaskListStatics: any;
  private impactedTaskListStaticsByType: any;
  private totalImpactedTaskListStatics: any;
  private taskList = [];
  private totalTaskRecords: number = 0;
  private runImpactClicked: Boolean = false;
  private submitted: boolean = true;

  constructor( private deliverableService: DeliverableService,
    private taskService: TaskService,
    private zone: NgZone,
    public toasterService: ToasterService,
    private router: Router,
    private fb: FormBuilder,
    private privilage : PrivilageService,) 
      { 
        super(toasterService);
        this.init();
      }

      init(){
        this.valForm = this.fb.group({        
          'deliverableId': [this.id, Validators.required]
        });
      }

  ngOnInit() {

    this.deliverableService.getAll().subscribe((data) => {
      this.zone.run(() => {
          this.deliverableList = data;
          this.showSpinner = 0;            
      });
     }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

  }
  onDeliverableSelect(){
    this.submitted = false;
  }
    public editRow(item) {
      this.router.navigate(['/task/details-read/' + item.taskId ]);
  }

  runImpactAssessment(){
    this.showSpinner = 1;
    this.selectedDeliverableId = this.valForm.get('deliverableId').value;
    this.runImpactClicked = true;
    this.deliverableService.getImpactedTaskByDeliverable(this.selectedDeliverableId).subscribe((data) => {
      this.zone.run(() => {
          this.impactedDeliverableTaskList = data;
          this.taskList =  this.impactedDeliverableTaskList;
          this.totalTaskRecords = this.taskList.length;  
          this.showSpinner = 0;
      });
  }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
  });
  
  this.deliverableService.getImpactedTaskStaticsByDeliverable(this.selectedDeliverableId).subscribe((data) => {
    this.zone.run(() => {
        this.impactedTaskListStatics = data;
        this.impactedTaskListStaticsByType = this.impactedTaskListStatics.taskImpactStatics;
        this.totalImpactedTaskListStatics  =  this.impactedTaskListStatics.totalTaskImpactStatics; 
        this.showSpinner = 0;
    });
}, (error) => {
    //console.log(error);
    this.errorMessage = error;
    this.showSpinner = 0;
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
});


   }

   reset(){
    this.valForm.get('deliverableId').reset();
    this.submitted = true;
    this.taskList = [];
    this.totalTaskRecords = 0;
    this.runImpactClicked = false;
   }
}

