import { Component, OnInit, NgZone, ViewChild, Renderer } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { ToasterService, ToasterConfig } from 'angular2-toaster';

import { Role } from 'app/model/role';
import { RoleService } from 'app/services/role.service';
import { PrivilageService } from 'app/services/privilage.service';
import { Userprivileges } from 'app/model/userprivileges';
import * as $ from 'jquery' ;



@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss']
})

export class RoleListComponent implements OnInit {

    private writeAccess : Boolean = false ;
    private deleteAccess : Boolean = false ;        
    private createAccess : Boolean = false 
    private singleData;
    private originalData;
    private selectedRow: number;
    private errorMessage: string;        
    private fail_toaster: any;
    private success_toaster: any;
    private showSpinner: number = 1;
    private totalRecords: number = 0;
    private delRole: Role;
    start: number;
    pressed: boolean;
    startX: number;
    startWidth: number;


    toasterconfig: ToasterConfig = new ToasterConfig({
        positionClass: 'toast-bottom-right',
        showCloseButton: true
    });        

    @ViewChild('deleteModal') delModal: any;
    
    constructor(private zone: NgZone, 
    private http: Http, 
    private router: Router, 
    private roleService: RoleService, 
    private toasterService: ToasterService, 
    private privilage: PrivilageService,
    public renderer: Renderer


    ) {

        this.fail_toaster = {
            type: 'error',
            title: 'Failed',
            text: 'Data could not be loaded.'
        };

        this.success_toaster = {
            type: 'success',
            title: 'Successful',
            text: 'The record has been deleted successfully.'
        };        
    }

    public ngOnInit(): void {        
            
        this.roleService.getAll().subscribe((data) => {
            this.zone.run(() => {
                this.singleData = data;
                this.originalData = this.singleData;
                this.showSpinner = 0;
                this.totalRecords = this.singleData.length;                
            });
        }, (error) => {
            //console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });
        this.privilage.CheckRoleAccess().subscribe((data: Userprivileges) => {
            this.zone.run(() => {
                if(data.edit === true ){
                    this.writeAccess = true ;                
                }if(data.delete === true){
                    this.deleteAccess = true ;
                }if(data.create === true ){
                    this.createAccess = true ;                
                }

            });
        }, (error) => {
            //console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });
        
    }

    private setClickedRow(index: number) {
        this.selectedRow = index;
    }
    private editRow(item) {
        const id = { 'roleName': item.roleName }
        // this.router.navigate(['/administration/role/details', id]);
        this.router.navigate(['/administration/role/details',id], { queryParams: id, skipLocationChange: true});
    }
    private addNew() {
        this.router.navigate(['/administration/role/details']);
    }

    private removeItem(item: Role) {
        // This method shows the pop up dialog for confirmation of delete record.

        this.delRole = item;
        this.delModal.show();
    }

    private delete() {
        // This method deletes the selected record.

        this.delModal.hide();
        this.showSpinner = 1;
        this.roleService.delete(this.delRole.roleName).subscribe((data) => {
            this.zone.run(() => {            
                    this.showSpinner = 0;
                    this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);
                    this.singleData = _.filter(this.originalData, (elem) => elem != this.delRole);                                       
                    this.totalRecords = this.singleData.length;
                    this.originalData = this.singleData;
                });
            }, (error) => {
              // console.log(error);
              this.errorMessage = error;
              this.showSpinner = 0;
              this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
            }
        );
    }

    private refresh() {
        this.showSpinner = 1;
        this.roleService.getAll().subscribe((data) => {
            this.zone.run(() => {
                this.singleData = data;
                this.originalData = this.singleData;
                this.showSpinner = 0;
                this.totalRecords = this.singleData.length;
            });
        }, (error) => {
            //console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });
    }
    private onQuickFilterChanged($event) {
        console.log($event.key);
        console.log($event.target.value);         
        if ($event.target.value) {
            this.singleData = this.originalData.filter(item => {
                return Object.keys(item).some(keyName => {                     
                     if (item[keyName] !== null) {
                        return item[keyName].toString().match(new RegExp($event.target.value, 'i'));
                     }
                });
            });
        } else {
            this.singleData = this.originalData;
        }
    }  
      
    onMouseDown(event) {
        this.start = event.target;
        this.pressed = true;
        this.startX = event.x;
        this.startWidth = $(this.start).parent().width();
        this.initResizableColumns();
      }
    
      private initResizableColumns() {
        this.renderer.listenGlobal('body', 'mousemove', (event) => {
           if (this.pressed) {
             const width = this.startWidth + (event.x - this.startX);
             $(this.start).parent().css({ 'min-width': width, 'max-   width': width });
             const index = $(this.start).parent().index() + 1;
             $('.glowTableBody tr td:nth-child(' + index + ')').css({ 'min-width': width, 'max-width': width });
           }
        });
    
        this.renderer.listenGlobal('body', 'mouseup', (event) => {
          if (this.pressed) {
            this.pressed = false;
          }
        });
      }

}