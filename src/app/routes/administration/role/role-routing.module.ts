import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleListComponent } from './role-list/role-list.component';
import { RoleDetailsComponent } from './role-details/role-details.component';
import { AuthGuard } from 'app/guards/auth.guard';


const routes: Routes = [
  { path: '', redirectTo: 'list' , canActivate: [AuthGuard] },
  { path: 'details', component: RoleDetailsComponent , canActivate: [AuthGuard] },
  { path: 'list', component: RoleListComponent  , canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RoleRoutingModule { }
