import { Component, OnInit, NgZone, Pipe, PipeTransform } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Location } from '@angular/common';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { Http } from '@angular/http';

import { Role } from 'app/model/role';
import { Permission } from 'app/model/permission';
import { RoleService } from 'app/services/role.service';
import { CommanConfig } from 'app/routes/CommanConfig';


@Component({
  selector: 'app-role-details',
  templateUrl: './role-details.component.html',
  styleUrls: ['./role-details.component.scss']
})
export class RoleDetailsComponent extends CommanConfig implements OnInit {    

  private valForm: FormGroup;
  private role: Role;
  private roleList;
  private submitted: boolean = false;
  private toUpdate: boolean = false;
  private item: any;
  private showSpinner: number = 1;
  private errorMessage;
  private permissionList: Permission[] = [];
  private tempPermissionList;
  private assignPermissionList: Permission[] = [];

  constructor(private zone: NgZone, 
    private fb: FormBuilder, 
    private route: ActivatedRoute,
    private router: Router,
    private roleService: RoleService,
    private _location: Location, 
    public toasterService: ToasterService,
    private http: Http) {
    
    super(toasterService);  
    this.initRole();

    /*this.permissionList = [
      {"value": 1, "name": "CREATE EDC Server"},
      {"value": 2, "name": "DELETE EDC Server"},
      {"value": 3, "name": "VIEW EDC Server"},
      {"value": 4, "name": "CREATE Trial"},
      {"value": 5, "name": "DELETE Trial"},
      {"value": 6, "name": "VIEW Trial"},
      {"value": 7, "name": "CREATE Database Server"},
      {"value": 8, "name": "DELETE Database Server"},
      {"value": 9, "name": "VIEW Database Server"},
      {"value": 10, "name": "CREATE Database Schema"},
      {"value": 11, "name": "DELETE Database Schema"},
      {"value": 12, "name": "VIEW Database Schema"}
    ];*/

  }

  private initRole() {
    this.role = new Role();
    this.valForm = this.fb.group({
        'roleIdPk': [this.role.roleIdPk],
        'roleName': [this.role.roleName, Validators.required],         
        'roleDescription': [this.role.roleDescription, null],
        'active': [this.role.active, Validators.required],
        'permissions': [this.assignPermissionList, null]
    });
  }

  ngOnInit() {
    this.showSpinner = 1;

    this.item = this.route.snapshot.params;
    if (this.item.roleName) {

      this.roleService.getFilteredPermissionByRoleName(this.item.roleName).subscribe((data) => {
          this.permissionList = data.filteredData;
          this.assignPermissionList = data.assignedData;
      },(error) => {
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });

      this.roleService.getRoleByName(this.item.roleName).subscribe((data) => {
        this.zone.run(() => {
          this.role = data;
          //this.assignPermissionList = this.role.permissions;
          //this.filterPermissionList();
          this.toUpdate = true;
          this.valForm.patchValue(this.role);
          this.showSpinner = 0;
        });
      }, (error) => {            
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });
    }
    else {
      this.roleService.getAllPermission().subscribe((data) => {
        this.zone.run(() => {
          this.tempPermissionList = data;
          this.filterPermissionList();
          this.showSpinner = 0;
        });
      }, (error) => {
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });
    }
  }

  private submitForm($ev, value: any) {
    $ev.preventDefault();    
    if (this.valForm.valid) {
      this.submitted = true;
      this.valForm.patchValue({'permissions' : this.assignPermissionList});
      // console.log(this.valForm.value);
      if (this.toUpdate) {
          this.showSpinner = 1;
          this.roleService.update(this.item.roleName, this.valForm.value)
            .subscribe(
            entry => this.handleSubmitSuccess(entry),
            error => this.handleSubmitError(error)
            );
        } else {
          this.showSpinner = 1;
          this.roleService.create(this.valForm.value)
            .subscribe(
            entry => this.handleSubmitSuccess(entry),
            error => this.handleSubmitError(error)
            );
        }
      }
      else{
        this.toasterService.pop(this.required_validation_toaster.type, this.required_validation_toaster.title, this.required_validation_toaster.text);
      }
  }

  protected handleSubmitSuccess(entry) {      
      this.showSpinner = 0;
      this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);      
      setTimeout(()=>{
        this.initRole();        
        this.backClicked();  
      },1500);
  }
  protected handleSubmitError(error: any) {    
    this.showSpinner = 0;
    this.submitted = false;
    //console.log(error);
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, error);    
  }

  private backClicked() {    
    this.router.navigate(['/administration/role/list']);
  }

  private filterPermissionList() {
    for (var i = 0; i < this.assignPermissionList.length; i++) {
      for (var j = 0; j < this.tempPermissionList.length; j++) {
        if (this.tempPermissionList[j].permissionIdPk === this.assignPermissionList[i].permissionIdPk) {              
          this.tempPermissionList.splice(j, 1); 
          break;
        }
      }
    }
    this.permissionList = this.tempPermissionList;
  }

  private changePermissionList(selectElement) {
        for (var i = 0; i < selectElement.options.length; i++) {
            var optionElement = selectElement.options[i];
            var optionModel = this.permissionList[i];

            if (optionElement.selected == true) {             
              this.assignPermissionList.push(optionModel);
              this.permissionList.splice(i, 1); 
            }            
        }
  }

  private changeAssignPermissionList(selectElement) {
        for (var i = 0; i < selectElement.options.length; i++) {
            var optionElement = selectElement.options[i];
            var optionModel = this.assignPermissionList[i];

            if (optionElement.selected == true) { 
              this.permissionList.push(optionModel);
              this.assignPermissionList.splice(i, 1);  
            }
        }
  }
  
  private selectAllPermission() {
    this.permissionList.forEach((item) => {this.assignPermissionList.push(item);});    
    this.permissionList = [];
  }

  private deSelectAllPermission() {
    this.assignPermissionList.forEach((item) => {this.permissionList.push(item);});
    this.assignPermissionList = []; 
  }

}
