import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from 'app/shared/shared.module';
import { RoleRoutingModule } from './role-routing.module';
import { RoleListComponent } from './role-list/role-list.component';
import { RoleDetailsComponent } from './role-details/role-details.component';
import { RoleService } from 'app/services/role.service';
//import { HasaccessDirective } from 'app/shared/directives/hasaccess/hasaccess.directive';
import { PrivilageService } from 'app/services/privilage.service';


@NgModule({
  imports: [
    CommonModule,
    RoleRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    SharedModule
  ],
  declarations: [RoleListComponent, RoleDetailsComponent],
  providers: [RoleService,  PrivilageService]
})
export class RoleModule { }
