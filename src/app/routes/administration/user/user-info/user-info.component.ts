import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StudyService } from 'app/services/study.service';
import { Study } from 'app/model/study';

import { ToasterService } from 'angular2-toaster/src/toaster.service';
import { CommanConfig } from 'app/routes/CommanConfig';
import { User } from 'app/model/user';
import { AppUser } from 'app/model/appuser';
import { Deliverable } from 'app/model/deliverable';
import { Userprivileges } from 'app/model/userprivileges';
import { PrivilageService } from 'app/services/privilage.service';
import { AppUserService} from 'app/services/appUser.service';
import { UserService } from 'app/services/user.service';

import { Group } from 'app/model/group';
import { Role } from 'app/model/role';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent extends CommanConfig implements OnInit {

  private item: any;
  private study : Study;
  private appUser :AppUser ;
  private user : User ;
  private showSpinner: number = 1;
  private errorMessage;
  private users : Array<User>;
  private deliverables : Array<Deliverable>;
  private groups : Array<Group>;
  private studies : Array<Study>;
  private roles : Array<Role>;


  private studyPrivilege : Userprivileges;
  private groupPrivilege : Userprivileges;
  

  constructor(
    private route: ActivatedRoute,
    private studyService : StudyService,
    private zone: NgZone,
    private router: Router,
    public toasterService: ToasterService,
    private privilage : PrivilageService, 
    private appUserService : AppUserService, 
    private userService : UserService


  ) {
    super(toasterService);
    this.init();  
   }

   init(){
    this.appUser = new AppUser(); 
    this.user = new User(); 

    this.studyPrivilege = new Userprivileges();
    this.groupPrivilege  = new Userprivileges();

   }


  ngOnInit() {

     this.privilage.CheckStudyAccess().subscribe((data : Userprivileges) => {
      this.zone.run(() => {
        this.assignPrivileges(data, this.studyPrivilege);
      });
    });

    this.privilage.CheckGroupAccess().subscribe((data : Userprivileges) => {
      this.zone.run(() => {
        this.assignPrivileges(data, this.groupPrivilege);
      });
    });
  
    this.item = this.route.snapshot.params;
    this.showSpinner = 1;
    this.appUserService.getUserDetailsById(this.item.Id).subscribe((data) => {
      this.zone.run(() => {
        this.appUser = data;
        this.showSpinner = 0;        
        this.groups = this.appUser.groups ;
        this.studies = this.appUser.studies;
    
      });
    },(error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

    this. userService.getUserById(this.item.Id).subscribe((data) => {
      this.zone.run(() => {
        this.user = data;
        this.showSpinner = 0;
        this.roles = this.user.roles;
      });
    },(error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });
 }

  private backClicked() {    
    this.router.navigate(['/administration/user/list']);
  }

initializePrivileges(privilege : Userprivileges){
    
  privilege.create = false;
  privilege.view = false;
  privilege.edit = false;
  privilege.delete = false;
  privilege.bridgeStatus = false;
  privilege.dataViewer = false;
}

assignPrivileges(responseData : Userprivileges, privilege : Userprivileges){
  privilege.view = responseData.view;
  privilege.create = responseData.create;
  privilege.edit = responseData.edit;
  privilege.delete = responseData.delete;
  privilege.bridgeStatus = responseData.bridgeStatus;
  privilege.dataViewer = responseData.delete;
}



}
