import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Location } from '@angular/common';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { Http } from '@angular/http';

import { UserService } from 'app/services/user.service';
import { RoleService } from 'app/services/role.service';
import { User } from 'app/model/user';
import { Role } from 'app/model/role';
import { CommanConfig } from 'app/routes/CommanConfig';


@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent extends CommanConfig implements OnInit {

  private settingActive = -99;
  private updateUserForm: FormGroup;
  private createUserForm: FormGroup;
  private passwordForm: FormGroup;
  private userRoleForm: FormGroup;
  private activeUserForm: FormGroup;
  private user: User;
  private submitted: boolean = false;
  private toUpdate;
  private item: any;
  private showSpinner: number = 1;
  private errorMessage;
  private availableRoleList: Role[] = [];
  private tempRoleList;
  private assignedRoleList: Role[] = [];
    
  constructor(private zone: NgZone, 
    private fb: FormBuilder, 
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private roleService: RoleService,
    private _location: Location, 
    public toasterService: ToasterService,
    private http: Http) {

    super(toasterService);  
    this.initUser();

  }

  private initUser() {
    this.user = new User();
    
    this.updateUserForm = this.fb.group({
        'userIdPk': [this.user.userIdPk, Validators.required],
        'userName': [this.user.userName, Validators.required],        
        'firstName': [this.user.firstName, Validators.required],
        'lastName': [this.user.lastName, Validators.required],     
        'email': [this.user.email, Validators.compose([Validators.required, CustomValidators.email])],
        'telephone': [this.user.telephone],
        'roles': [this.assignedRoleList, null]        
    });

    let updatePassword = new FormControl('', Validators.required);
    let confirmPassword = new FormControl('', CustomValidators.equalTo(updatePassword));

    this.passwordForm = this.fb.group({      
        'updatePassword': updatePassword,
        'confirmPassword': confirmPassword
    });

    this.userRoleForm = this.fb.group({      
    'roles': [this.assignedRoleList, null] ,
    'telephone': [this.user.telephone]
    });
    

    this.activeUserForm = this.fb.group({
      'active': [this.user.active, Validators.required]
    });

    let password = new FormControl('', Validators.required);
    let certainPassword = new FormControl('', CustomValidators.equalTo(password));

    this.createUserForm = this.fb.group({        
        'userName': [this.user.userName, Validators.required],        
        'firstName': [this.user.firstName, Validators.required],
        'lastName': [this.user.lastName, Validators.required],     
        'email': [this.user.email, Validators.compose([Validators.required, CustomValidators.email])],
        'telephone': [this.user.telephone],
        'active': [this.user.active, Validators.required],
        'passwordGroup': this.fb.group({
          password: password,
          confirmPassword: certainPassword
        }),
        'roles': [this.assignedRoleList, null]
    });

  }

  ngOnInit() {

    this.showSpinner = 1;    
    this.item = this.route.snapshot.params;
    if (this.item.Id) {      

      this.settingActive = 1;
      this.userService.getUserById(this.item.Id).subscribe((data) => {
        this.zone.run(() => {
          this.user = data;
          this.toUpdate = true;
          // console.log(this.user);
          //this.assignedRoleList = this.user.roles;
          this.updateUserForm.patchValue(this.user); 
          this.activeUserForm.get('active').patchValue(this.user.active);
          this.updateUserForm.get('userName').disable();

          /*this.roleService.getAll().subscribe((roleData) => {
            this.tempRoleList = roleData;
            this.filterRoleList();
          }, (error) => {
            // console.log(error);
            this.errorMessage = error;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
          }); This code gets all roles*/
          this.showSpinner = 0;
        });
      },(error) => {
            // console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });

      this.userService.getRolesByUserId(this.item.Id).subscribe((data) => {
        this.zone.run(() => {
          this.availableRoleList = data.filteredData;
          this.assignedRoleList = data.assignedData;
          this.showSpinner = 0;
        });
      },(error) => {
            // console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });

    }
    else {
      this.roleService.getAll().subscribe((roleData) => {
        this.zone.run(() => {
            this.tempRoleList = roleData;
            this.filterRoleList();
        });
      }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });
      this.showSpinner = 0;
      this.settingActive = 0;
      this.createUserForm.get('active').patchValue('0');
      this.toUpdate = false;
    }
  }

  private submitUpdateUserForm($ev, value: any) {
    $ev.preventDefault();    
    if (this.updateUserForm.valid) {
      this.submitted = true;
      if (this.toUpdate) {        
        this.showSpinner = 1;
        this.userService.updateProfile(this.item.Id, this.updateUserForm.value)
          .subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );
      } 
    }
    else{
      this.toasterService.pop(this.required_validation_toaster.type, this.required_validation_toaster.title, this.required_validation_toaster.text);
    }
  }

  private submitCreateUserForm($ev, value: any) {
    $ev.preventDefault();    

    if (this.createUserForm.valid) {
      this.submitted = true;
      if (this.toUpdate == false) {        
        this.showSpinner = 1;  
        this.user.userName = this.createUserForm.get('userName').value;
        this.user.firstName = this.createUserForm.get('firstName').value;
        this.user.lastName = this.createUserForm.get('lastName').value;
        this.user.email = this.createUserForm.get('email').value;
        this.user.telephone = this.createUserForm.get('telephone').value;
        this.user.password = this.createUserForm.get('passwordGroup').get('password').value;
        this.user.active = +this.createUserForm.get('active').value;        
        this.user.roles = this.createUserForm.get('roles').value;        

        this.userService.create(this.user)
          .subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );
      } 
    } 
    else{
      this.toasterService.pop(this.required_validation_toaster.type, this.required_validation_toaster.title, this.required_validation_toaster.text);
    }
  }

  private submitActiveUserForm($ev, value: any) {
    $ev.preventDefault();    

    if (this.activeUserForm.valid) {
      this.submitted = true;
      if (this.toUpdate == true) {
        this.showSpinner = 1;
        this.user.active = +this.activeUserForm.get('active').value;        
        this.userService.updateActive(this.user.userName, this.user.active)
          .subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );         
      } 
    } 
  }

   private submitDeleteUser() {
        this.userService.deleteUser(this.user.userIdPk, this.user)
          .subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );
        this.showSpinner = 0;    
        this.backClicked();      
   }
   
  private submitPasswordResetForm($ev, value: any) {
    $ev.preventDefault();
    if (this.passwordForm.valid) {
      this.submitted = true;
      if (this.toUpdate == true) {        
        this.showSpinner = 1;
        this.user.password = this.passwordForm.get('updatePassword').value;     
        // call resetPassword rest api
        this.userService.updatePassword(this.user.userIdPk  , this.user)
          .subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );
        this.showSpinner = 0;     
      } 
    } 
    else{
      this.toasterService.pop(this.required_validation_toaster.type, this.required_validation_toaster.title, this.required_validation_toaster.text);
    }
  }

  private submitRoleForm($ev, value: any) {
    $ev.preventDefault();
    console.log("in role submit");
    if (this.userRoleForm.valid) {
      this.submitted = true;
      if (this.toUpdate == true) {        
        this.showSpinner = 1;
        this.user.roles = this.assignedRoleList;     
        
        this.userService.updateRole(this.user.userIdPk  , this.user)
          .subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );
        
        this.showSpinner = 0;    
        this.backClicked();  
      } 
    }
  }

  protected handleSubmitSuccess(entry) {  
    if (this.toUpdate) {
      this.showSpinner = 0;
      this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);      
      setTimeout(()=>{
        this.backClicked();  
      },1500);
    }
    else {
      this.showSpinner = 0;
      this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);        
      setTimeout(()=>{
        this.initUser();
        this.backClicked();        
      },1500);
    }
  }
  protected handleSubmitError(error: any) {
    this.showSpinner = 0;
    this.submitted = false;
    //console.log(error);
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, error);      
  }

  private backClicked() {    
    this.router.navigate(['/administration/user/list']);
  }

  private filterRoleList() {
        for (var i = 0; i < this.assignedRoleList.length; i++) {
          for (var j = 0; j < this.tempRoleList.length; j++) {
            if (this.tempRoleList[j].roleIdPk === this.assignedRoleList[i].roleIdPk) {              
              this.tempRoleList.splice(j, 1);
              break; 
            }
          }
        }
        this.availableRoleList = this.tempRoleList;
  }

  private changeRoleList(selectElement) {
        for (var i = 0; i < selectElement.options.length; i++) {
            var optionElement = selectElement.options[i];
            var optionModel = this.availableRoleList[i];

            if (optionElement.selected == true) {             
              this.assignedRoleList.push(optionModel);
              this.availableRoleList.splice(i, 1); 
            }            
        }
  }

  private changeAssignedRoleList(selectElement) {
        for (var i = 0; i < selectElement.options.length; i++) {
            var optionElement = selectElement.options[i];
            var optionModel = this.assignedRoleList[i];

            if (optionElement.selected == true) { 
              this.availableRoleList.push(optionModel);
              this.assignedRoleList.splice(i, 1);  
            }
        }
  }
  
  private selectAllRoles() {
    this.availableRoleList.forEach((item) => {this.assignedRoleList.push(item);});    
    this.availableRoleList = [];
  }

  private deSelectAllRoles() {
    this.assignedRoleList.forEach((item) => {this.availableRoleList.push(item);});
    this.assignedRoleList = []; 
  }

}
