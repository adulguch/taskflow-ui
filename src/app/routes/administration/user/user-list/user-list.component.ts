import { Component, OnInit, NgZone, ViewChild, Renderer } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { ToasterService, ToasterConfig } from 'angular2-toaster';

import { User } from 'app/model/user';
import { UserService } from 'app/services/user.service';
import { PrivilageService } from 'app/services/privilage.service';
import { Userprivileges } from 'app/model/userprivileges';
import { UserStat } from 'app/model/user-stat';
import * as $ from 'jquery' ;


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

    private writeAccess : Boolean = false ;
    private deleteAccess : Boolean = false ;
    private createAccess : Boolean = false ;
    private singleData;
    private originalData;
    private selectedRow: number;
    private errorMessage: string;        
    private fail_toaster: any;
    private success_toaster: any;
    private showSpinner: number = 1;
    private totalRecords: number = 0;
    private userStat : UserStat[];
    start: number;
    pressed: boolean;
    startX: number;
    startWidth: number;

    toasterconfig: ToasterConfig = new ToasterConfig({
        positionClass: 'toast-bottom-right',
        showCloseButton: true
    });        
    
    constructor(private zone: NgZone, 
    private http: Http, 
    private router: Router, 
    private userService: UserService, 
    private toasterService: ToasterService,
    private privilage: PrivilageService,
    public renderer: Renderer
    , ) {

        this.fail_toaster = {
            type: 'error',
            title: 'Failed',
            text: 'Data could not be loaded.'
        };

        this.success_toaster = {
            type: 'success',
            title: 'Successful',
            text: 'The record has been deleted successfully.'
        }; 
        this.userStat = Array<UserStat>();       
    }

    public ngOnInit(): void {        
            
        this.userService.getAll().subscribe((data) => {
            this.zone.run(() => {
                this.singleData = data;
                this.originalData = this.singleData;
                this.showSpinner = 0;
                this.totalRecords = this.singleData.length;                
            });
        }, (error) => {
            //console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });

        this.privilage.CheckUserAccess().subscribe((data: Userprivileges) => {
            this.zone.run(() => {
                if(data.edit === true ){
                    this.writeAccess = true ;                
                }if(data.delete === true){
                    this.deleteAccess = true ;                
                }
                if(data.create === true){
                    this.createAccess = true ;                
                }
            });
        }, (error) => {
            //console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });

        this.userService.getUserStat().subscribe((data) => {
            this.zone.run(() => {
                this.userStat = data; 
                //console.log(this.userStat);                  
            });
        }, (error) => {
            //console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });
    }

    public setClickedRow(index: number) {
        this.selectedRow = index;
    }
    public editRow(item) {
        const id = { 'Id': item.userIdPk }
        // this.router.navigate(['/administration/user/details', id]);
        this.router.navigate(['/administration/user/details',id], { queryParams: id, skipLocationChange: true});
    }
    public addNew() {
        this.router.navigate(['/administration/user/details']);
    }

    public info(item) {
        const id = { 'Id': item.userIdPk }
        this.router.navigate(['/administration/user/info',id], { queryParams: id, skipLocationChange: true});
    }

    public refresh() {
        this.showSpinner = 1;
        this.userService.getAll().subscribe((data) => {
            this.zone.run(() => {
                this.singleData = data;
                this.originalData = this.singleData;
                this.showSpinner = 0;
                this.totalRecords = this.singleData.length;
            });
        }, (error) => {
            //console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });
    }
    private onQuickFilterChanged($event) {
        //console.log($event.key);
        //console.log($event.target.value);         
        if ($event.target.value) {
            this.singleData = this.originalData.filter(item => {
                return Object.keys(item).some(keyName => {                     
                     if (item[keyName] !== null) {
                        return item[keyName].toString().match(new RegExp($event.target.value, 'i'));
                     }
                });
            });
        } else {
            this.singleData = this.originalData;
        }
    }    

    onMouseDown(event) {
        this.start = event.target;
        this.pressed = true;
        this.startX = event.x;
        this.startWidth = $(this.start).parent().width();
        this.initResizableColumns();
      }
    
      private initResizableColumns() {
        this.renderer.listenGlobal('body', 'mousemove', (event) => {
           if (this.pressed) {
             const width = this.startWidth + (event.x - this.startX);
             $(this.start).parent().css({ 'min-width': width, 'max-   width': width });
             const index = $(this.start).parent().index() + 1;
             $('.glowTableBody tr td:nth-child(' + index + ')').css({ 'min-width': width, 'max-width': width });
           }
        });
    
        this.renderer.listenGlobal('body', 'mouseup', (event) => {
          if (this.pressed) {
            this.pressed = false;
          }
        });
      }

}
