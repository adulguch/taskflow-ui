import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { AuthGuard } from 'app/guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'list' , canActivate: [AuthGuard] },
  { path: 'details', component: UserDetailsComponent, canActivate: [AuthGuard] },
  { path: 'list', component: UserListComponent, canActivate: [AuthGuard] }, 
  { path: 'info', component: UserInfoComponent, canActivate: [AuthGuard] }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
