  import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from 'app/shared/shared.module';
import { UserRoutingModule } from './user-routing.module';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserService } from 'app/services/user.service';
import { RoleService } from 'app/services/role.service';
//import { HasaccessDirective } from 'app/shared/directives/hasaccess/hasaccess.directive';
import { PrivilageService } from 'app/services/privilage.service';
import { UserInfoComponent } from './user-info/user-info.component';
import { StudyService } from 'app/services/study.service';
import { AppUserService } from 'app/services/appUser.service';




@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    UserRoutingModule,
    SharedModule
  ],
  declarations: [UserListComponent, UserDetailsComponent, UserInfoComponent ],
  providers: [UserService, RoleService, PrivilageService, StudyService, AppUserService]
})
export class UserModule { }
