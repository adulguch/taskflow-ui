import { Component, OnInit, NgZone, ViewChild, Renderer } from '@angular/core';
import { ToasterConfig, ToasterService } from 'angular2-toaster';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { SelectData } from 'app/model/selected-data';


import { PrivilageService } from 'app/services/privilage.service';
import { Userprivileges } from 'app/model/userprivileges';
import { Group } from 'app/model/group';
import { UserService } from '../../../../services/user.service';
import { GroupService } from '../../../../services/group.service';
import { User } from 'app/model/user';
import * as $ from 'jquery' ;

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.scss']
})
export class GroupListComponent implements OnInit {

  private writeAccess : Boolean = false ;
    private deleteAccess : Boolean = false ;
    private createAccess : Boolean = false ;
    private singleData;
    private originalData;
    private selectedRow: number;
    private errorMessage: string;        
    private fail_toaster: any;
    private success_toaster: any;
    private showSpinner: number = 1;
    private totalRecords: number = 0;
    private delGroup: Group;
    private availableUserList : User[];
    private assignedUsertList : User[];
    start: number;
    pressed: boolean;
    startX: number;
    startWidth: number;

    toasterconfig: ToasterConfig = new ToasterConfig({
        positionClass: 'toast-bottom-right',
        showCloseButton: true
    });  
    
    @ViewChild('deleteModal') delModal: any;
   
    constructor(private zone: NgZone, 
      private http: Http, 
      private router: Router, 
      private userService: UserService, 
      private groupService: GroupService, 
      private toasterService: ToasterService,
      private privilage: PrivilageService,
      public renderer: Renderer
      , ) {

        this.fail_toaster = {
          type: 'error',
          title: 'Failed',
          text: 'Data could not be loaded.'
      };

      this.success_toaster = {
          type: 'success',
          title: 'Successful',
          text: 'The record has been deleted successfully.'
      };        
  }
    

  ngOnInit(): void {

    this.groupService.getGroupsByUser().subscribe((data) => {
      this.zone.run(() => {
          this.singleData = data;
          this.originalData = this.singleData;
          this.showSpinner = 0;
          this.totalRecords = this.singleData.length;                
      });
  }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
  });

  this.privilage.CheckGroupAccess().subscribe((data: Userprivileges) => {
      this.zone.run(() => {
          if(data.edit === true ){
              this.writeAccess = true ;                
          }if(data.delete === true){
              this.deleteAccess = true ;                
          }
          if(data.create === true){
              this.createAccess = true ;                
          }
      });
  }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
  });
}

    public setClickedRow(index: number) {
      this.selectedRow = index;
    }
    public editRow(item) {
      const id = { 'Id': item.id }
    //   this.router.navigate(['/administration/group/details', id]);
      this.router.navigate(['/administration/group/details',id], { queryParams: id, skipLocationChange: true});
    }
    public addNew() {
      this.router.navigate(['/administration/group/details']);
    }

    public removeItem(item: Group) {
        this.delGroup = item;
        this.delModal.show();
        
    }

    public delete() {
        this.delModal.hide();
        this.showSpinner = 1;
        this.groupService.delete(this.delGroup.id).subscribe((data) => {
            this.zone.run(() => {            
                    this.showSpinner = 0;
                    this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);
                    this.singleData = _.filter(this.originalData, (elem) => elem != this.delGroup);                                       
                    this.totalRecords = this.singleData.length;
                    this.originalData = this.singleData;
                });
            }, (error) => {
              console.log(error);
              this.errorMessage = error;
              this.showSpinner = 0;
              this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
            }
        );
    }




    public refresh() {
      this.showSpinner = 1;
      this.groupService.getGroupsByUser().subscribe((data) => {
          this.zone.run(() => {
              this.singleData = data;
              this.originalData = this.singleData;
              this.showSpinner = 0;
              this.totalRecords = this.singleData.length;
          });
      }, (error) => {
          //console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });
    }
    private onQuickFilterChanged($event) {
      console.log($event.key);
      console.log($event.target.value);         
      if ($event.target.value) {
          this.singleData = this.originalData.filter(item => {
              return Object.keys(item).some(keyName => {                     
                  if (item[keyName] !== null) {
                      return item[keyName].toString().match(new RegExp($event.target.value, 'i'));
                  }
              });
          });
      } else {
          this.singleData = this.originalData;
      }
    }   
    
    onMouseDown(event) {
        this.start = event.target;
        this.pressed = true;
        this.startX = event.x;
        this.startWidth = $(this.start).parent().width();
        this.initResizableColumns();
      }
    
      private initResizableColumns() {
        this.renderer.listenGlobal('body', 'mousemove', (event) => {
           if (this.pressed) {
             const width = this.startWidth + (event.x - this.startX);
             $(this.start).parent().css({ 'min-width': width, 'max-   width': width });
             const index = $(this.start).parent().index() + 1;
             $('.glowTableBody tr td:nth-child(' + index + ')').css({ 'min-width': width, 'max-width': width });
           }
        });
    
        this.renderer.listenGlobal('body', 'mouseup', (event) => {
          if (this.pressed) {
            this.pressed = false;
          }
        });
      }
}

