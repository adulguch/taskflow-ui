import { Component, OnInit, NgZone } from '@angular/core';
import { CommanConfig } from 'app/routes/CommanConfig';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'app/services/user.service';
import { ToasterService } from 'angular2-toaster';
import { Http } from '@angular/http';
import { Group } from 'app/model/group';
import { User } from 'app/model/user';
import { GroupService } from '../../../../services/group.service';
import { SelectData } from 'app/model/selected-data';
import { AppUserService } from 'app/services/appUser.service';
import { AppUser } from 'app/model/appuser';

@Component({
  selector: 'app-group-details',
  templateUrl: './group-details.component.html',
  styleUrls: ['./group-details.component.scss']
})
export class GroupDetailsComponent extends CommanConfig implements OnInit {

 
  private valForm: FormGroup;
  private group: Group;
  private userList;
  private submitted: boolean = false;
  private toUpdate: boolean = false;
  private item: any;
  private showSpinner: number = 1;
  private errorMessage;
  private availableUserList: AppUser[] = [];
  private tempUserList;
  private assignedUserList: AppUser[] = [];

  constructor(private zone: NgZone ,
    private fb: FormBuilder, 
    private route: ActivatedRoute,
    private router: Router,
    private groupService: GroupService,
    private appUserService : AppUserService,
    public toasterService: ToasterService,
    private http: Http
  ){
      super(toasterService);  
    this.initGroup();
    }



 private initGroup() {
  this.group= new Group();
  this.valForm = this.fb.group({
      'id': [this.group.id],
      'name': [this.group.name, Validators.required],         
      'description': [this.group.description, null],
      'active': [this.group.active, Validators.required],
      'users': [this.assignedUserList, null]
  });
}
    
  ngOnInit() {
      this.showSpinner = 1;
      this.item = this.route.snapshot.params;
      if (this.item.Id) {
        this.appUserService.getFilteredUsersByGroupId(this.item.Id).subscribe((data) => {
          this.availableUserList = data.filteredData;
          this.assignedUserList = data.assignedData;
      },(error) => {
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });
    
      this.groupService.getGroupById(this.item.Id).subscribe((data) => {
        this.zone.run(() => {
          this.group = data;
          // this.assignedUserList = this.group.users;
          // this.filterUserList();
          this.toUpdate = true;
          this.valForm.patchValue(this.group);
          this.showSpinner = 0;
        });
      }, (error) => {            
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });
    }
    
    else {
      this.appUserService.getAll().subscribe((data) => {
        this.zone.run(() => {
          this.tempUserList = data;
          this.availableUserList = this.tempUserList;
          this.showSpinner = 0;
        });
      }, (error) => {
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });
   }
   /* if(!this.item.Id ){
    this.appUserService.getFilteredUsersByStudyId(studyId).subscribe((data) => {
      this.zone.run(() => {
          //this.selectData = data;
          this.showSpinner = 0; 
          
          this.availableUserList = data.filteredData;
          this.assignedUserList = data.assignedData;
          
      });
     }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });
   }
   else{
    this.appUserService.getFilteredUsersByStudyId(studyId).subscribe((data) => {
      this.zone.run(() => {
          //this.selectData = data;
          this.showSpinner = 0; 
          
          this.availableUserList = data.filteredData;
          this.assignedUserList = data.assignedData;
          
      });
     }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    }); 
   }*/   
}

private submitForm($ev, value: any) {
  $ev.preventDefault();    
  if (this.valForm.valid) {
    this.submitted = true;
    this.valForm.patchValue({'users' : this.assignedUserList});
    if (this.toUpdate) {    
      this.showSpinner = 1;
      this.groupService.update(this.item.Id, this.valForm.value)
        .subscribe(
        entry => this.handleSubmitSuccess(entry),
        error => this.handleSubmitError(error)
        );
    } else {        
      this.showSpinner = 1;
      this.groupService.create(this.valForm.value)
        .subscribe(
        entry => this.handleSubmitSuccess(entry),
        error => this.handleSubmitError(error)
        );
    }
  }
  else{
    this.toasterService.pop(this.required_validation_toaster.type, this.required_validation_toaster.title, this.required_validation_toaster.text);
  }
  
}

 protected handleSubmitSuccess(entry) {  
  if (this.toUpdate) {
    this.showSpinner = 0;
    this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);      
    setTimeout(()=>{
      this.backClicked();  
    },1500);
  }
  else {
    // setTimeout(()=>{this.router.navigate(['/administration/group/list']);
      this.showSpinner = 0;
      this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);  
      setTimeout(()=>{      
      this.initGroup();
      this.backClicked();        
    },1500);
  }
 }

protected handleSubmitError(error: any) {
  this.showSpinner = 0;
  this.submitted = false;
  //console.log(error);
  this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, error);      
}

 private backClicked() {    
  this.router.navigate(['/administration/group/list']);
}

// private filterUserList() {
//   for (var i = 0; i < this.assignedUserList.length; i++) {
//     for (var j = 0; j < this.tempUserList.length; j++) {
//       if (this.tempUserList[j].userIdPk === this.assignedUserList[i].userIdPk) {              
//         this.tempUserList.splice(j, 1); 
//         break;
//       }
//     }
//   }
//   this.availableUserList = this.tempUserList;
// }

private changeUserList(selectElement) {
  for (var i = 0; i < selectElement.options.length; i++) {
      var optionElement = selectElement.options[i];
      var optionModel = this.availableUserList[i];

      if (optionElement.selected == true) {             
        this.assignedUserList.push(optionModel);
        this.availableUserList.splice(i, 1); 
      }            
  }
}

private changeAssignedUserList(selectElement) {
  for (var i = 0; i < selectElement.options.length; i++) {
      var optionElement = selectElement.options[i];
      var optionModel = this.assignedUserList[i];

      if (optionElement.selected == true) { 
        this.availableUserList.push(optionModel);
        this.assignedUserList.splice(i, 1);  
      }
  }
}

private selectAllUsers() {
  this.availableUserList.forEach((item) => {this.assignedUserList.push(item);});    
  this.availableUserList = [];
}

private deSelectAllUsers() {
  this.assignedUserList.forEach((item) => {this.availableUserList.push(item);});
  this.assignedUserList = []; 
 }
}
