import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTableModule } from 'angular2-datatable';

import { GroupDetailsComponent } from './group-details/group-details.component';
import { GroupListComponent } from './group-list/group-list.component';
import { SharedModule } from '../../../shared/shared.module';
import { GroupRoutingModule } from './group-routing.module';
import { PrivilageService } from 'app/services/privilage.service';
import { UserService } from 'app/services/user.service';
import { GroupService } from '../../../services/group.service';
import { AppUserService } from 'app/services/appUser.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GroupRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,

  ],
  declarations: [GroupDetailsComponent, GroupListComponent],
  providers: [UserService, GroupService, PrivilageService, AppUserService]
})
export class GroupModule { }
