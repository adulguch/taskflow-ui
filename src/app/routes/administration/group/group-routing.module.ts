import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/guards/auth.guard';
import { GroupDetailsComponent } from './group-details/group-details.component';
import { GroupListComponent } from './group-list/group-list.component';


const routes: Routes = [
  { path: '', redirectTo: 'list' , canActivate: [AuthGuard] },
  { path: 'details', component: GroupDetailsComponent, canActivate: [AuthGuard] },
  { path: 'list', component: GroupListComponent, canActivate: [AuthGuard] }
           
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupRoutingModule { }
