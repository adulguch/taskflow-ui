import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { AuthGuard } from 'app/guards/auth.guard';



const routes: Routes = [
  { path: 'user', loadChildren: './user/user.module#UserModule' , canActivate: [AuthGuard] },
  { path: 'role', loadChildren: './role/role.module#RoleModule' , canActivate: [AuthGuard]}, 
  { path: 'audit', loadChildren: './action-audit/action-audit.module#ActionAuditModule' , canActivate: [AuthGuard] },
  { path: 'group', loadChildren: './group/group.module#GroupModule' , canActivate: [AuthGuard] }
  
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class AdministrationModule {
     constructor() { 
     console.log('AdministrationModule loaded......');
  }

 }
