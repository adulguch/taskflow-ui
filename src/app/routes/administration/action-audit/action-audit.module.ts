import { ActionAuditRoutingModule } from './action-audit-routing.module';
import { AuditListComponent } from './audit-list/audit-list.component';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'angular2-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'app/shared/shared.module';
import { ActionauditService } from 'app/services/actionaudit.service';



@NgModule({
  imports: [
    CommonModule,
    ActionAuditRoutingModule, 
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    SharedModule
  ],
  declarations: [AuditListComponent],
  providers: [ActionauditService]
})
export class ActionAuditModule { }
