
import { Component, OnInit, NgZone, ViewChild, Renderer } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { ActionauditService } from 'app/services/actionaudit.service';
import { PaginationMetadata } from 'app/model/paginationmetadata';
import * as $ from 'jquery' ;




@Component({
  selector: 'app-audit-list',
  templateUrl: './audit-list.component.html',
  styleUrls: ['./audit-list.component.scss']
})
export class AuditListComponent implements OnInit {

    private metadata : PaginationMetadata;
    private singleData;
    private originalData;
    private selectedRow: number;
    private errorMessage: string;        
    private fail_toaster: any;
    private success_toaster: any;
    private showSpinner: number = 1;
    private totalRecords: number = 0;
    private recordStart : number;
    private recordEnd : number;
    private nextData;
    private prevData;
    start: number;
    pressed: boolean;
    startX: number;
    startWidth: number;

    public limits : number[] = [1000, 2000, 5000];
    public selectedLimit : number;
    public nextDisabled = false;
    public previousDisabled = true;
    public lastDisabled = false;
    
    toasterconfig: ToasterConfig = new ToasterConfig({
        positionClass: 'toast-bottom-right',
        showCloseButton: true
    }); 
    
    constructor(private zone: NgZone, 
    private http: Http, 
    private router: Router, 
    private toasterService: ToasterService,
    private actionauditService: ActionauditService ,
    public renderer: Renderer   
    ) {

        this.fail_toaster = {
            type: 'error',
            title: 'Failed',
            text: 'Data could not be loaded.'
        };

        this.success_toaster = {
            type: 'success',
            title: 'Successful',
            text: 'The record has been deleted successfully.'
        }; 
        
        
    }

    public ngOnInit(): void {        
        this.selectedLimit = this.limits[0];

        this.actionauditService.getAll(this.selectedLimit, -1, true).subscribe((data) => {
            this.zone.run(() => {
                this.metadata = data;
                this.singleData = this.metadata.currentData;
                this.originalData = this.singleData; 
                this.nextData = this.metadata.nextData;
                this.prevData = this.metadata.previousData;
                this.showSpinner = 0; 
                this.initData(data); 
            });
        }, (error) => {
            //console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });

        this.recordEnd = this.selectedLimit;
        this.recordStart = 1;
    }


    public refresh(limit, start, greater, type) {
        //Empty the originalData for every call
        if(this.originalData && this.originalData.length > 0)
            this.originalData.splice(0, this.originalData.length);

        this.showSpinner = 1;
        this.actionauditService.getAll(limit, start, greater).subscribe((data) => {
            this.zone.run(() => {
                this.metadata = data;
                this.singleData = this.metadata.currentData;
                this.originalData = this.singleData; 
                this.nextData = this.metadata.nextData;
                this.prevData = this.metadata.previousData;
                this.showSpinner = 0;
                this.setRecordsData(this.metadata, type, limit);     
                //if(this.previousDisabled && this.nextDisabled && this.lastDisabled)
                // console.log(data);
                this.initData(data); 

            });
        }, (error) => {
            //console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });
    }

    private onQuickFilterChanged($event) {
        console.log($event.key);
        console.log($event.target.value);         
        if ($event.target.value) {
            this.singleData = this.originalData.filter(item => {
                return Object.keys(item).some(keyName => {                     
                     if (item[keyName] !== null) {
                        return item[keyName].toString().match(new RegExp($event.target.value, 'i'));
                     }
                });
            });
        } else {
            this.singleData = this.originalData;
        }
    }    

    getNextRecords(){
        this.nextDisabled = true;
        //settimeout hass been added to prevent the client click the button multiple times that will break the code 
        /*setTimeout(() => {
            if(this.nextData.length != 0)
                this.nextDisabled = false;
        }, 400);*/
        let start = this.originalData[this.originalData.length - 1].auditId;
        this.refresh(this.selectedLimit, start, false, "next");
        
        
    }

    getPreviousRecords(){
        //settimeout hass been added to prevent the client click the button multiple times that will break the code 
        this.previousDisabled = true;
        /*setTimeout(() => {
            if(this.prevData.length != 0)
                this.previousDisabled = false;
        }, 400);*/
        let start = this.originalData[0].auditId;
        this.refresh(this.selectedLimit, start, true, "prev");
        
        /*if( this.prevData.length < this.selectedLimit){
            this.getFirstRecords();
        }*/
        
    }

    getFirstRecords(){
        this.refresh(this.selectedLimit, -1, true, "first");
        
        /*this.recordStart = 1;
        if(!this.previousDisabled && !this.nextDisabled && !this.lastDisabled){
            this.recordEnd = this.selectedLimit;
            this.nextDisabled = false;
            this.lastDisabled = false;
        }
        if(this.recordStart < 1)
            this.recordStart = 1;
        this.previousDisabled = true;*/
    }

    getLastRecords(){
        this.refresh(this.selectedLimit, -1, false, "last");
        
    }

    onLimitChange(event){
        this.selectedLimit = parseInt(event.target.value);
        if(this.prevData.length > 0){
            this.refresh(this.selectedLimit, this.prevData[this.prevData.length - 1].auditId, false, "limit");         
        }else
            this.refresh(this.selectedLimit, -1, true, "limit");

        
    }

    setRecordsData(data : PaginationMetadata, type, limit){
        //console.log(data);
        if(type === "prev"){
            this.previousDisabled = false;
            this.nextDisabled = false;
            this.lastDisabled = false;
       
            if( data.currentData.length >= this.selectedLimit){
                //console.log(limit + " " + (data.nextData.length - 1));
                this.recordStart -= limit;
                this.recordEnd = this.recordStart + limit - 1;
            }
    
            if(data.previousData.length == 0){
                this.previousDisabled = true;
                this.getFirstRecords();
            }
            
            /*if( data.previousData.length < this.selectedLimit){
                this.getFirstRecords();
            }*/
        }else if(type === "next"){
            this.nextDisabled = false;

            if(data.nextData.length == 0){
                this.nextDisabled = true;
                this.lastDisabled = true;
            }
            this.previousDisabled = false;
            this.recordStart += this.selectedLimit;
            this.recordEnd += this.selectedLimit;
            if(this.recordEnd > this.metadata.totalRecords)
                this.recordEnd = this.metadata.totalRecords;

        }else if(type === "first"){
            this.recordStart = 1;
            this.recordEnd = this.selectedLimit;
            this.nextDisabled = false;
            this.lastDisabled = false;
            this.previousDisabled = true;
        }else if(type === "last"){
            this.nextDisabled = true;
            this.lastDisabled = true;
            this.previousDisabled = false;
            this.recordEnd = data.totalRecords;
            this.recordStart = data.totalRecords - this.selectedLimit + 1;
        
        }else if(type === "limit"){
            if(data.nextData.length > 0){
                this.nextDisabled = false;
                this.lastDisabled = false;
            }else if(data.nextData.length == 0){
                this.nextDisabled = true;
                this.lastDisabled = true;
                this.previousDisabled = false;
            }

            this.recordEnd = this.recordStart + this.selectedLimit - 1;
            if(this.recordEnd > this.metadata.totalRecords)
                this.recordEnd = this.metadata.totalRecords;
        }
    }

    //This is executed when first time the screen is loaded and the total audit records are less than the seleted limit 
    initData(data){

        if(data.totalRecords <= this.selectedLimit){
            
            this.previousDisabled = true;
            this.nextDisabled = true;
            this.lastDisabled = true;
            this.recordStart = 1;
            this.recordEnd = data.totalRecords;
        } 
    }

    onMouseDown(event) {
        this.start = event.target;
        this.pressed = true;
        this.startX = event.x;
        this.startWidth = $(this.start).parent().width();
        this.initResizableColumns();
      }
    
      private initResizableColumns() {
        this.renderer.listenGlobal('body', 'mousemove', (event) => {
           if (this.pressed) {
             const width = this.startWidth + (event.x - this.startX);
             $(this.start).parent().css({ 'min-width': width, 'max-   width': width });
             const index = $(this.start).parent().index() + 1;
             $('.glowTableBody tr td:nth-child(' + index + ')').css({ 'min-width': width, 'max-width': width });
           }
        });
    
        this.renderer.listenGlobal('body', 'mouseup', (event) => {
          if (this.pressed) {
            this.pressed = false;
          }
        });
      }
}
