import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuditListComponent } from './audit-list/audit-list.component';
import { AuthGuard } from 'app/guards/auth.guard';



const routes: Routes = [
  { path: '', redirectTo: 'list' , canActivate: [AuthGuard] },
  { path: 'list', component: AuditListComponent , canActivate: [AuthGuard] }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ActionAuditRoutingModule { }
