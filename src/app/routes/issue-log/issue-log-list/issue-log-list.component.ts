import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { CommanConfig } from 'app/routes/CommanConfig';
import { PrivilageService } from 'app/services/privilage.service';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { IssueLogService } from 'app/services/issue-log.service';
import { Task } from 'app/model/task';
import { TaskSearch} from 'app/model/task-search';
import { Userprivileges } from 'app/model/userprivileges';


@Component({
  selector: 'app-issue-log-list',
  templateUrl: './issue-log-list.component.html',
  styleUrls: ['./issue-log-list.component.scss']
})
export class IssueLogListComponent extends CommanConfig implements OnInit {

  private writeAccess : Boolean = false ;
  private deleteAccess : Boolean = false ;
  private createAccess : Boolean = false ;  

  public success_toaster: any;
  private showSpinner: number = 1;
  private totalRecords: number = 0;

  private searchForm: FormGroup;  
  private taskSearch: TaskSearch;
  private errorMessage;

    constructor(
    private zone: NgZone, 
    private fb: FormBuilder, 
    private route: ActivatedRoute,
    private http: Http, 
    private router: Router, 
    public toasterService: ToasterService,
    private privilage: PrivilageService

   ) {
     super(toasterService);
     this.initTaskSearch();
 }

  private initTaskSearch() {
    this.taskSearch = new TaskSearch();
    
    // this.searchForm = this.fb.group({
    //     'studyId': [this.taskSearch.studyId],
    //     'deliverableId': [this.taskSearch.deliverableId],
    //     'developmentId': [this.taskSearch.developmentId],     
    //     'qualityTestId': [this.taskSearch.qualityTestId],
    //     'startDate': [this.taskSearch.startDate],
    //     'endDate': [this.taskSearch.endDate]       
    // });
  }

  ngOnInit() {
    this.showSpinner = 0;    
    this.privilage.CheckIssueAccess().subscribe((data: Userprivileges) => {

    this.zone.run(() => {
        if(data.edit === true ){
            this.writeAccess = true ;                
        }if(data.delete === true){
            this.deleteAccess = true ;
        }if(data.create === true ){
            this.createAccess = true ;                
        }

      });
    }, (error) => {
    //console.log(error);
    this.errorMessage = error;
    this.showSpinner = 0;
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
});


  }

}
