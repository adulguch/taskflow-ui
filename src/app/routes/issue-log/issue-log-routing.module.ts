import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/guards/auth.guard';
import { IssueLogDetailsComponent } from './issue-log-details/issue-log-details.component';
import { IssueLogListComponent } from './issue-log-list/issue-log-list.component';


const routes: Routes = [
 { path: '', redirectTo: 'list',  canActivate: [AuthGuard] }, 
 { path: 'details', component: IssueLogDetailsComponent,   canActivate: [AuthGuard] },   
 { path: 'list', component: IssueLogListComponent,   canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IssueLogRoutingModule { }
