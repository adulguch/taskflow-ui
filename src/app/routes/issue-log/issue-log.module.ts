import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'angular2-datatable';
import { SharedModule } from 'app/shared/shared.module';

import { IssueLogRoutingModule } from './issue-log-routing.module';
import { IssueLogListComponent } from './issue-log-list/issue-log-list.component';
import { IssueLogDetailsComponent } from './issue-log-details/issue-log-details.component';
import { PrivilageService } from 'app/services/privilage.service';
import { IssueLogService } from 'app/services/issue-log.service';


@NgModule({
  imports: [
    CommonModule,
    IssueLogRoutingModule,
	DataTableModule,
    SharedModule

  ],
  declarations: [IssueLogListComponent, IssueLogDetailsComponent],
	providers: [IssueLogService , PrivilageService]

})
export class IssueLogModule { }
