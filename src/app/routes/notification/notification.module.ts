import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationRoutingModule } from './notification-routing.module';


import { SharedModule } from 'app/shared/shared.module';
import { NotificationListComponent } from './notification-list/notification-list.component';
import { NotificationService } from 'app/services/notification.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NotificationRoutingModule
  ],
  declarations: [NotificationListComponent],
  providers : [NotificationService]
})
export class NotificationModule { }
