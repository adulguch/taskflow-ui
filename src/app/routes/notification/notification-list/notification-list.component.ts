import { Component, OnInit, NgZone } from '@angular/core';
import { NotificationService } from 'app/services/notification.service';
import { Notification } from 'app/model/notification';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { PaginationMetadata } from 'app/model/paginationmetadata';


@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.scss']
})
export class NotificationListComponent implements OnInit {

  private showSpinner: number = 1;
  private errorMessage;
  private success_toaster: any;
  private fail_toaster: any; 
  private notifications : Notification[];
  public limits : number[] = [10, 20, 50];
  private selectedLimit : number;
  private recordStart : number;
  private recordEnd : number;
  private totalRecords: number = 0;
  public nextDisabled = false;
  public previousDisabled = true;
  public lastDisabled = false;
  private metadata : PaginationMetadata;
  private nextData;
  private prevData;
  private notificationArchivalList : Notification[];
  private archivalRecords : number = 0;
  private currentDataRecords;
    
   toasterconfig: ToasterConfig = new ToasterConfig({
        positionClass: 'toast-bottom-right',
        showCloseButton: true
    }); 
    
  constructor(private notificationService : NotificationService, 
              private zone: NgZone, private toasterService: ToasterService) { 
    this.success_toaster = {
      type: 'success',
      title: 'Successful',
      text: 'The record has been updated successfully.'
    };
    this.fail_toaster = {
      type: 'error',
      title: 'Failed',
      text: 'The record was not updated.'
    };

    this.notificationArchivalList = [];

  }

  ngOnInit() {
    this.selectedLimit = this.limits[0];

    this.notificationService.getLimitedNotificationsByUser(this.selectedLimit, -1, true).subscribe((data) => {
      this.zone.run(() => {
          this.metadata = data;
          this.notifications = this.metadata.currentData;
          this.totalRecords = this.metadata.totalRecords;   
          this.nextData = this.metadata.nextData;
          this.prevData = this.metadata.previousData;
          this.showSpinner = 0; 
          this.initData(data);               
      });
    }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

    this.recordEnd = this.selectedLimit;
    this.recordStart = 1;
  }

  public refresh(limit, start, greater, type) {
    //Empty the originalData for every call
    if(this.notifications && this.notifications.length > 0)
        this.notifications.splice(0, this.notifications.length);
    this.notificationArchivalList = [];
    this.showSpinner = 1;
    this.notificationService.getLimitedNotificationsByUser(limit, start, greater).subscribe((data) => {
        this.zone.run(() => {
            this.metadata = data;
            this.notifications = this.metadata.currentData;
            this.totalRecords = this.metadata.totalRecords; 
            this.nextData = this.metadata.nextData;
            this.prevData = this.metadata.previousData;
            this.showSpinner = 0;
            this.setRecordsData(this.metadata, type, limit);
            this.initData(data);
        });
    }, (error) => {
        //console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });
}

  hasNotification(){
    if(this.totalRecords > 0)
      return true;
    else 
      return false;
  }

  getNextRecords(){
    this.nextDisabled = true;
    let start = this.notifications[this.notifications.length - 1].id;
    this.refresh(this.selectedLimit, start, false, "next");
  }

  getPreviousRecords(){
    this.previousDisabled = true;
    let start = this.notifications[0].id;
    this.refresh(this.selectedLimit, start, true, "prev");
  }

  getFirstRecords(){
    this.refresh(this.selectedLimit, -1, true, "first");
  }

  getLastRecords(){
    this.refresh(this.selectedLimit, -1, false, "last");
  }


  onLimitChange(event){
    this.selectedLimit = parseInt(event.target.value);
    if(this.prevData.length > 0){
        this.refresh(this.selectedLimit, this.prevData[this.prevData.length - 1].id, false, "limit");         
    }else
        this.refresh(this.selectedLimit, -1, true, "limit");

    this.recordEnd = this.recordStart + this.selectedLimit - 1;
    if(this.recordEnd > this.metadata.totalRecords)
        this.recordEnd = this.metadata.totalRecords;
  }

  setRecordsData(data : PaginationMetadata, type, limit){
    if(type === "prev"){
      this.previousDisabled = false;
      this.nextDisabled = false;
      this.lastDisabled = false;

      if( data.currentData.length >= this.selectedLimit){
        this.recordStart -= limit;
        this.recordEnd = this.recordStart + limit - 1;    
      }

      if(data.previousData.length == 0){
        this.previousDisabled = true;
        this.getFirstRecords();
      }
    }else if(type === "next"){
      
      this.nextDisabled = false;
      if(data.nextData.length == 0){
        this.nextDisabled = true;
        this.lastDisabled = true;
      }

      this.previousDisabled = false;
      this.recordStart += this.selectedLimit;
      this.recordEnd += this.selectedLimit;
      if(this.recordEnd > this.metadata.totalRecords)
        this.recordEnd = this.metadata.totalRecords;
    }else if(type === "first"){
      this.recordStart = 1;
      this.recordEnd = this.selectedLimit;
      this.nextDisabled = false;
      this.lastDisabled = false;
      this.previousDisabled = true;
    }else if(type === "last"){
      this.nextDisabled = true;
      this.lastDisabled = true;
      this.previousDisabled = false;
      this.recordEnd = data.totalRecords;
      this.recordStart = data.totalRecords - this.selectedLimit + 1;
    }else if(type === "limit"){
      if(data.nextData.length > 0){
        this.nextDisabled = false;
        this.lastDisabled = false;
      }else if(data.nextData.length == 0){
        this.nextDisabled = true;
        this.lastDisabled = true;
        this.previousDisabled = false;
    }
    }else if(type === "archive"){
      if(this.archivalRecords === this.currentDataRecords && this.nextData.length != 0){
        this.recordStart -= this.selectedLimit;
        this.recordEnd -= this.selectedLimit;
      }else if(this.archivalRecords === this.currentDataRecords && this.nextData.length == 0){
        //this.recordStart -= this.selectedLimit;
        this.recordEnd = ( this.recordEnd - this.archivalRecords) + this.metadata.currentData.length ;
        //for safety though below code is not required
        if(this.recordEnd > this.totalRecords)
          this.recordEnd = this.totalRecords 
      }else{
        console.log(this.recordStart);
        if(this.recordEnd > this.totalRecords)
          this.recordEnd = this.totalRecords;
      }
    }
}

  initData(data){
    if(data.totalRecords <= this.selectedLimit){
        this.previousDisabled = true;
        this.nextDisabled = true;
        this.lastDisabled = true;
        if(data.totalRecords == 0)
          this.recordStart = 0;
        else
          this.recordStart = 1;
        this.recordEnd = data.totalRecords;
    } 
  }

  setArchival(event, notification){
    
    if(event.target.checked){
      this.notificationArchivalList.push(notification);
    }else if(!event.target.checked){
      for(let i = 0; i < this.notificationArchivalList.length; i++) {
        let n = this.notificationArchivalList[i];
        if(n.id === notification.id){
          this.notificationArchivalList.splice(i, 1);
          break;
        }
      }
    }
  }

  sendForArchival(){
    this.archivalRecords = this.notificationArchivalList.length;
    this.currentDataRecords = this.metadata.currentData.length;
    this.notificationService.archiveNotification(this.notificationArchivalList)
    .subscribe(
      entry => {
        this.refreshAfterArchival();
        this.handleSubmitSuccess(entry)
      },
      error => this.handleSubmitError(error)
      );
  }

  protected handleSubmitSuccess(entry) {  
      this.showSpinner = 0;
      this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);        
      setTimeout(()=>{
        
      },1500);
  }

  protected handleSubmitError(error: any) {
    this.showSpinner = 0;
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, error);      
  }

  private refreshAfterArchival(){
    let start = 0;
    if(this.prevData.length === 0){
      this.getFirstRecords();
      return;
    }
    else if(this.archivalRecords === this.metadata.currentData.length && this.nextData.length == 0){
      this.getLastRecords();
      return;
      
    }
    else{
      start = this.prevData[this.prevData.length - 1].id;
    }
    this.refresh(this.selectedLimit, start, false, "archive");
    this.notificationArchivalList = [];
  }

}
