import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudyListComponent } from './study-list/study-list.component';
import { StudyDetailsComponent } from './study-details/study-details.component';
import { AuthGuard } from 'app/guards/auth.guard';
import { StudyManagementComponent } from 'app/routes/study/study-management/study-management.component';
import { StudyInfoComponent } from 'app/routes/study/study-info/study-info.component';
import { StudyTaskComponent } from './study-task/study-task.component';


const routes: Routes = [
	{ path: '', redirectTo: 'list' , canActivate: [AuthGuard] },
  	{ path: 'details', component: StudyDetailsComponent, canActivate: [AuthGuard] },
	{ path: 'list', component: StudyListComponent, canActivate: [AuthGuard] },
	{ path: 'management', component: StudyManagementComponent, canActivate: [AuthGuard] },
	{ path: 'info', component: StudyInfoComponent, canActivate: [AuthGuard] },
	{ path: 'task', component: StudyTaskComponent, canActivate: [AuthGuard] }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudyRoutingModule { }
