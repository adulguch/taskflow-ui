import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { PrivilageService } from 'app/services/privilage.service';
import { Http } from '../../../../../node_modules/@angular/http';
import { CommanConfig } from 'app/routes/CommanConfig';
import { Router, ActivatedRoute } from '../../../../../node_modules/@angular/router';
import { StudyService } from 'app/services/study.service';
import { ToasterService } from '../../../../../node_modules/angular2-toaster';
import { Study } from 'app/model/study';
import * as _ from 'lodash';

@Component({
  selector: 'app-study-task',
  templateUrl: './study-task.component.html',
  styleUrls: ['./study-task.component.scss']
})
export class StudyTaskComponent extends CommanConfig implements OnInit {

    private item: any ;
    private writeAccess : Boolean = false ;
    private deleteAccess : Boolean = false ;
    private createAccess : Boolean = false ;
    private singleData;
    private originalData;
    private selectedRow: number;
    private errorMessage: string;        
    public success_toaster: any;
    private showSpinner: number = 1;
    private totalRecords: number = 0;
    private delStudy : Study ; 
    private delTaskList ;  
  

 
  @ViewChild('deleteModal') deleteModal: any;

    constructor(private zone: NgZone, 
    private http: Http, 
    private router: Router, 
    private studyService: StudyService, 
    public toasterService: ToasterService,
    private privilage: PrivilageService,
    private route: ActivatedRoute , 
  ) {
  
  super(toasterService);  

    }

  ngOnInit() {
    this.item = this.route.snapshot.params;
    this.studyService.getTasksByStudyId(this.item.Id).subscribe((data) => {
          this.zone.run(() => {
              this.delTaskList = data;
              this.showSpinner = 0;
              this.totalRecords = this.delTaskList.length;                
          });
      }, (error) => {
          //console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });
  }


  private submitDeleteStudy() {
        this.deleteModal.show();
    }  

    private delete() {
      // This method deletes the selected record.
      this.deleteModal.hide();
      this.showSpinner = 1;
      this.studyService.delete(this.item.Id).subscribe((data) => {
          this.zone.run(() => {            
                  this.showSpinner = 0;
                  this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);
                  this.singleData = _.filter(this.originalData, (elem) => elem != this.delStudy);                                       
                  this.totalRecords = this.singleData.length;
                  this.originalData = this.singleData;
              });
          }, (error) => {
            // console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
          }
      );
      setTimeout(()=>{
        this.backClicked();        
      },1500);    
  }   
  
  private backClicked() {    
    this.router.navigate(['/study/list']);
  }

}
