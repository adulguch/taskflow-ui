import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyTaskComponent } from './study-task.component';

describe('StudyTaskComponent', () => {
  let component: StudyTaskComponent;
  let fixture: ComponentFixture<StudyTaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyTaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyTaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
