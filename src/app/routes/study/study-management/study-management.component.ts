import { Component, NgZone, OnInit } from '@angular/core';
import { StudyService } from 'app/services/study.service';
import { Study } from 'app/model/study';
import { CommanConfig } from 'app/routes/CommanConfig';
import { ToasterService } from 'angular2-toaster/src/toaster.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectData } from 'app/model/selected-data';
import { Router } from '@angular/router';
import { AppUserService } from 'app/services/appUser.service';
import { DeliverableService } from 'app/services/deliverable.service';
import { Userprivileges } from 'app/model/userprivileges';
import { PrivilageService } from 'app/services/privilage.service';
import { GroupService } from 'app/services/group.service';

@Component({
  selector: 'app-study-management',
  templateUrl: './study-management.component.html',
  styleUrls: ['./study-management.component.scss']
})
export class StudyManagementComponent  extends CommanConfig implements OnInit {

  private studyList;
  private studyList2;
  private originalData;
  private showSpinner: number = 1;
  private errorMessage: string;
  private valForm: FormGroup;
  private id;
  private selectedStudy : Study;
  private selectData : SelectData;
  private usersList;
  private assignUsersList;

  private deliverablesList;
  private assigndeliverablesList;

  private globalDeliverablesList ;
  private assignGlobalDeliverablesList;
  private localDeliverablesList ;
  private assignLocalDeliverablesList;

 
  
  private groupsList;
  private assignGroupsList;
  
  
  private submitted : boolean;

  appUserPrivilege : Userprivileges;
  deliverablePrivilege : Userprivileges;
  groupPrivilege : Userprivileges;
  
  
  constructor(private studyService : StudyService,
              private zone: NgZone,
              public toasterService: ToasterService,
              private fb: FormBuilder,
              private router: Router,
              private appUserService : AppUserService,
              private deliverableService : DeliverableService,
              private privilage : PrivilageService,
              private groupService : GroupService) {
                super(toasterService);
                this.init();
              }


  init(){
    this.valForm = this.fb.group({        
      'studyId': [this.id, Validators.required],
      'users':[this.assignUsersList, null],
      'deliverables' : [this.assigndeliverablesList, null],
      'deliverables1' : [this.assignGlobalDeliverablesList, null],
      'deliverables2' : [this.assignLocalDeliverablesList, null],
      'groups' : [this.assignGroupsList, null],
    });

    this.usersList = [];
    this.assignUsersList = [];
    this.deliverablesList = [];
    this.assigndeliverablesList = [];
    this.globalDeliverablesList = [];
    this.assignGlobalDeliverablesList = [];
     this.localDeliverablesList = [] ;
     this.assignLocalDeliverablesList = [];
    this.groupsList = [];
    this.assignGroupsList = [];
    this.appUserPrivilege = this.studyService.appUserPrivilege;
    this.deliverablePrivilege  = this.studyService.deliverablePrivilege;
    this.groupPrivilege  = this.studyService.groupPrivilege;
  }

  ngOnInit() {
    this.studyService.getAll().subscribe((data) => {
      this.zone.run(() => {
          this.studyList = data;
          this.showSpinner = 0;            
      });
     }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

  }

  onStudyChange(studyId){
    /* for(let i = 0; i < this.studyList.length; i++){
      if(studyId == this.studyList[i].studyId){
        this.selectedStudy = this.studyList[i];
        break;
      }
    } */
    this.id = studyId;
    this.showSpinner = 1;
    this.appUserService.getFilteredUsersByStudyId(studyId).subscribe((data) => {
      this.zone.run(() => {
          this.selectData = data;
          this.showSpinner = 0; 
          
          this.usersList = this.selectData.filteredData;
          this.assignUsersList = this.selectData.assignedData;
          
      });
     }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

    this.deliverableService.getFilteredDeliverablesByStudyId(studyId).subscribe((data) => {
      this.zone.run(() => {
          this.selectData = data;
          this.showSpinner = 0; 

          this.globalDeliverablesList = this.selectData.filteredData;
          this.assignGlobalDeliverablesList = this.selectData.assignedData;

          this.localDeliverablesList = this.selectData.filteredData2;
          this.assignLocalDeliverablesList = this.selectData.assignedData2;
          
      });
     }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });


    this.groupService.getFilteredGroupsByStudyId(studyId).subscribe((data) => {
      this.zone.run(() => {
          this.selectData = data;
          this.showSpinner = 0; 
          
          this.groupsList = this.selectData.filteredData;
          this.assignGroupsList = this.selectData.assignedData;
          
      });
     }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

  }

  private changeUsersList(selectElement) {
    for (var i = 0; i < selectElement.options.length; i++) {
        var optionElement = selectElement.options[i];
        var optionModel = this.usersList[i];

        if (optionElement.selected == true) {             
          this.assignUsersList.push(optionModel);
          this.usersList.splice(i, 1); 
        }            
    }
  }

  private changeAssigUsersList(selectElement) {
      for (var i = 0; i < selectElement.options.length; i++) {
          var optionElement = selectElement.options[i];
          var optionModel = this.assignUsersList[i];

          if (optionElement.selected == true) { 
            this.usersList.push(optionModel);
            this.assignUsersList.splice(i, 1);  
          }
      }
  }

  private changeGloibalDeliverablesList(selectElement) {
    for (var i = 0; i < selectElement.options.length; i++) {
        var optionElement = selectElement.options[i];
        var optionModel = this.globalDeliverablesList[i];

        if (optionElement.selected == true) {             
          this.assignGlobalDeliverablesList.push(optionModel);
          this.globalDeliverablesList.splice(i, 1); 
        }            
    }
  }

  private changeAssignedGloibalDeliverablesList(selectElement) {
      for (var i = 0; i < selectElement.options.length; i++) {
          var optionElement = selectElement.options[i];
          var optionModel = this.assignGlobalDeliverablesList[i];

          if (optionElement.selected == true) { 
            this.globalDeliverablesList.push(optionModel);
            this.assignGlobalDeliverablesList.splice(i, 1);  
          }
      }
  }

  private changeDeliverablesList(selectElement) {
    for (var i = 0; i < selectElement.options.length; i++) {
        var optionElement = selectElement.options[i];
        var optionModel = this.localDeliverablesList[i];

        if (optionElement.selected == true) {             
          this.assignLocalDeliverablesList.push(optionModel);
          this.localDeliverablesList.splice(i, 1); 
        }            
    }
  }

  private changeAssignedDeliverablesList(selectElement) {
      for (var i = 0; i < selectElement.options.length; i++) {
          var optionElement = selectElement.options[i];
          var optionModel = this.assignLocalDeliverablesList[i];

          if (optionElement.selected == true) { 
            this.localDeliverablesList.push(optionModel);
            this.assignLocalDeliverablesList.splice(i, 1);  
          }
      }
  }

  private changeGroupsList(selectElement) {
    for (var i = 0; i < selectElement.options.length; i++) {
        var optionElement = selectElement.options[i];
        var optionModel = this.groupsList[i];

        if (optionElement.selected == true) {             
          this.assignGroupsList.push(optionModel);
          this.groupsList.splice(i, 1); 
        }            
    }
  }

  private changeAssigGroupsList(selectElement) {
      for (var i = 0; i < selectElement.options.length; i++) {
          var optionElement = selectElement.options[i];
          var optionModel = this.assignGroupsList[i];

          if (optionElement.selected == true) { 
            this.groupsList.push(optionModel);
            this.assignGroupsList.splice(i, 1);  
          }
      }
  }

  selectAllUsers(){
    this.usersList.forEach(user => {this.assignUsersList.push(user)});
    this.usersList = [];
  }

  deselectAllUsers(){
    this.assignUsersList.forEach(user => {this.usersList.push(user)});
    this.assignUsersList = [];
  }

  selectAllDeliverables(){
    this.localDeliverablesList.forEach(deliverable => {this.assignLocalDeliverablesList.push(deliverable)});
    this.localDeliverablesList = [];
  }

  deselectAllDeliverables(){
    this.assignLocalDeliverablesList.forEach(deliverable => {this.localDeliverablesList.push(deliverable)});
    this.assignLocalDeliverablesList = [];
  }

  selectAllGlobalDeliverables(){
    this.globalDeliverablesList.forEach(deliverable => {this.assignGlobalDeliverablesList.push(deliverable)});
    this.globalDeliverablesList = [];
  }

  deselectAllGlobalDeliverables(){
    this.assignGlobalDeliverablesList.forEach(deliverable => {this.globalDeliverablesList.push(deliverable)});
    this.assignGlobalDeliverablesList = [];
  }

  selectAllGroups(){
    this.groupsList.forEach(group => {this.assignGroupsList.push(group)});
    this.assignGroupsList = [];
  }

  deselectAllGroups(){
    this.assignGroupsList.forEach(group => {this.groupsList.push(group)});
    this.assignGroupsList = [];
  }

  private submitForm($ev, value: any) {
    $ev.preventDefault();    
    if (this.valForm.valid) {
      this.submitted = true;
      if(this.appUserPrivilege.view)
        this.valForm.patchValue({'users' : this.assignUsersList});
      if(this.deliverablePrivilege.view)  
    
       this.valForm.patchValue({'deliverables1' : this.assignGlobalDeliverablesList});
       this.valForm.patchValue({'deliverables2' : this.assignLocalDeliverablesList});
       this.valForm.patchValue({'deliverables' : this.assignGlobalDeliverablesList.concat(this.assignLocalDeliverablesList)} );
      if(this.groupPrivilege.view)  
        this.valForm.patchValue({'groups' : this.assignGroupsList});
      
          this.studyService.updateUsers(this.id, this.valForm.value)
            .subscribe(
            entry => this.handleSubmitSuccess(entry),
            error => this.handleSubmitError(error)
            );
            console.log(this.valForm.value);
        
      }
      else{
        this.toasterService.pop(this.required_validation_toaster.type, this.required_validation_toaster.title, this.required_validation_toaster.text);
      }
  }

  protected handleSubmitSuccess(entry) {      
    this.showSpinner = 0;
    this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);      
    setTimeout(()=>{
      this.init(); 
      this.backClicked(); 
    },1500);
  }
  protected handleSubmitError(error: any) {    
    this.showSpinner = 0;
    this.submitted = false;
    //console.log(error);
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, error);    
  }

  private backClicked() {    
    this.router.navigate(['/study/list']);
  }


}
