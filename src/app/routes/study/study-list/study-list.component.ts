import { Component, OnInit, NgZone, ViewChild, Renderer } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { CommanConfig } from 'app/routes/CommanConfig';
import { Study } from 'app/model/study';
import { StudyService } from 'app/services/study.service';
import { PrivilageService } from 'app/services/privilage.service';
import { Userprivileges } from 'app/model/userprivileges';
import * as _ from 'lodash';
import * as $ from 'jquery' ;


@Component({
  selector: 'app-study-list',
  templateUrl: './study-list.component.html',
  styleUrls: ['./study-list.component.scss']
})
export class StudyListComponent extends CommanConfig implements OnInit {

    private writeAccess : Boolean = false ;
    private deleteAccess : Boolean = false ;
    private createAccess : Boolean = false ;
    private singleData;
    private originalData;
    private selectedRow: number;
    private errorMessage: string;        
    public success_toaster: any;
    private showSpinner: number = 1;
    private totalRecords: number = 0;
    private delStudy : Study ; 
    private delTaskList ; 
    
    start: number;
    pressed: boolean;
    startX: number;
    startWidth: number;

    @ViewChild('deleteModal') deleteModal: any;

    constructor(private zone: NgZone, 
    private http: Http, 
    private router: Router, 
    private studyService: StudyService, 
    public toasterService: ToasterService,
    private privilage: PrivilageService,
    public renderer: Renderer

    , ) {
   
   super(toasterService);  

    }

    public ngOnInit(): void { 
        this.showSpinner = 1;

        this.studyService.getStudyByUsers().subscribe((data) => {
            this.zone.run(() => {
                this.singleData = data;
                
                this.originalData = this.singleData;
                this.showSpinner = 0;
                this.totalRecords = this.singleData.length;                
            });
        }, (error) => {
            //console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });

        this.privilage.CheckStudyAccess().subscribe((data: Userprivileges) => {
            this.zone.run(() => {
                if(data.edit === true ){
                    this.writeAccess = true ;                
                }if(data.delete === true){
                    this.deleteAccess = true ;                
                }
                if(data.create === true){
                    this.createAccess = true ;                
                }
            });
        }, (error) => {
            //console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });
        


        //Study list shoul be displayed as per user hence below code is commented
        /* this.studyService.getAll().subscribe((data) => {
            this.zone.run(() => {
                this.singleData = data;
                this.originalData = this.singleData;
                this.showSpinner = 0;
                this.totalRecords = this.singleData.length;
            });
        }, (error) => {
            //console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        }); */
    }

    public setClickedRow(index: number) {
        this.selectedRow = index;
    }
    public editRow(item) {    	
        const id = { 'Id': item.studyId }
		this.router.navigate(['/study/details',id], { queryParams: id, skipLocationChange: true});
    }

    public info(item) {    	
        const id = { 'Id': item.studyId }
		this.router.navigate(['/study/info',id], { queryParams: id, skipLocationChange: true});
    }
    public addNew() {
        this.router.navigate(['/study/details']);
    }

    public removeItem(item: Study) { 
        const id = { 'Id': item.studyId }
        this.router.navigate(['/study/task',id], { queryParams: id, skipLocationChange: true});    
    }

    public refresh() {
        this.showSpinner = 1;
        this.studyService.getStudyByUsers().subscribe((data) => {
            this.zone.run(() => {
                this.singleData = data;
                this.originalData = this.singleData;
                this.showSpinner = 0;
                this.totalRecords = this.singleData.length;                
            });
        }, (error) => {
            //console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });
    }
    private onQuickFilterChanged($event) {
        console.log($event.key);
        console.log($event.target.value);         
        if ($event.target.value) {
            this.singleData = this.originalData.filter(item => {
                return Object.keys(item).some(keyName => {                     
                     if (item[keyName] !== null) {
                        return item[keyName].toString().match(new RegExp($event.target.value, 'i'));
                     }
                });
            });
        } else {
            this.singleData = this.originalData;
        }
    }

    onMouseDown(event) {
        this.start = event.target;
        this.pressed = true;
        this.startX = event.x;
        this.startWidth = $(this.start).parent().width();
        this.initResizableColumns();
      }
    
      private initResizableColumns() {
        this.renderer.listenGlobal('body', 'mousemove', (event) => {
           if (this.pressed) {
             const width = this.startWidth + (event.x - this.startX);
             $(this.start).parent().css({ 'min-width': width, 'max-   width': width });
             const index = $(this.start).parent().index() + 1;
             $('.glowTableBody tr td:nth-child(' + index + ')').css({ 'min-width': width, 'max-width': width });
           }
        });
    
        this.renderer.listenGlobal('body', 'mouseup', (event) => {
          if (this.pressed) {
            this.pressed = false;
          }
        });
      }
}
