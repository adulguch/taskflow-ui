import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'angular2-datatable';


import { StudyRoutingModule } from './study-routing.module';
import { StudyListComponent } from './study-list/study-list.component';
import { StudyDetailsComponent } from './study-details/study-details.component';
import { SharedModule } from 'app/shared/shared.module';
import { StudyService } from 'app/services/study.service';
import { PrivilageService } from 'app/services/privilage.service';
import { StudyManagementComponent } from './study-management/study-management.component';
import { AppUserService } from 'app/services/appUser.service';
import { DeliverableService } from 'app/services/deliverable.service';
import { GroupService } from 'app/services/group.service';
import { StudyInfoComponent } from './study-info/study-info.component';
import { StudyTaskComponent } from './study-task/study-task.component';


@NgModule({
  imports: [
    CommonModule,
    StudyRoutingModule,
    DataTableModule,
    SharedModule
  ],
  declarations: [StudyListComponent, StudyDetailsComponent, StudyManagementComponent, StudyInfoComponent, StudyTaskComponent],
  providers: [StudyService , PrivilageService, AppUserService, DeliverableService, GroupService]

})
export class StudyModule { }
