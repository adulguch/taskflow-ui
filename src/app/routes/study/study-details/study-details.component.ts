
import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Location } from '@angular/common';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { Http } from '@angular/http';

import { StudyService } from 'app/services/study.service';
import { Study } from 'app/model/study';
import { CommanConfig } from 'app/routes/CommanConfig';


@Component({
  selector: 'app-study-details',
  templateUrl: './study-details.component.html',
  styleUrls: ['./study-details.component.scss']
})
export class StudyDetailsComponent  extends CommanConfig implements OnInit {

  private settingActive = -99;
  private updateStudyForm: FormGroup;
  private valForm: FormGroup;
  private passwordForm: FormGroup;
  private activeStudyForm: FormGroup;
  private study: Study;
  private submitted: boolean = false;
  private toUpdate;
  private item: any;
  private showSpinner: number = 1;
  private errorMessage;
    
  constructor(private zone: NgZone, 
    private fb: FormBuilder, 
    private route: ActivatedRoute,
    private router: Router,
    private studyService: StudyService,    
    private _location: Location, 
    public toasterService: ToasterService,
    private http: Http) {

    super(toasterService);  
    this.initStudy();

  }

  private initStudy() {
    this.study = new Study();
    
    // this.updateStudyForm = this.fb.group({
    //     'studyId': [this.study.studyId],
    //     'studyName': [this.study.studyName, Validators.required],   
    //     'studyTitle': [this.study.studyTitle, Validators.required],     
    //     'studyDescription': [this.study.studyDescription, Validators.required],
    //     'clinicalPhase': [this.study.clinicalPhase, Validators.required],     
    //     'therapeuticArea': [this.study.therapeuticArea, Validators.required]
    // });


    this.valForm = this.fb.group({        
        'studyId': [this.study.studyId],
        'studyName': [this.study.studyName, Validators.required],  
        'studyCode': [this.study.studyCode, Validators.required], 
        'studyTitle': [this.study.studyTitle, Validators.required],      
        'studyDescription': [this.study.studyDescription, Validators.required],
        'clinicalPhase': [this.study.clinicalPhase, Validators.required],     
        'therapeuticArea': [this.study.therapeuticArea, Validators.required]
        });
  }

  ngOnInit() {
    
    this.showSpinner = 1;    
    this.item = this.route.snapshot.params;
    if (this.item.Id) {      
      this.settingActive = 1;
      this.studyService.getStudyById(this.item.Id).subscribe((data) => {
        this.zone.run(() => {
          this.study = data;
          this.toUpdate = true;
          this.valForm.patchValue(this.study);
          this.showSpinner = 0;
        });
      },(error) => {
            // console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });

    }
    this.showSpinner = 0;

  }

  // private submitUpdateStudyForm($ev, value: any) {
  //   $ev.preventDefault();    
  //   if (this.updateStudyForm.valid) {
  //     this.submitted = true;
  //     if (this.toUpdate) {        
  //       this.showSpinner = 1;
  //       this.studyService.update(this.item.Id, this.updateStudyForm.value)
  //         .subscribe(
  //         entry => this.handleSubmitSuccess(entry),
  //         error => this.handleSubmitError(error)
  //         );
  //     } 
  //   }
  //   else{
  //     this.toasterService.pop(this.required_validation_toaster.type, this.required_validation_toaster.title, this.required_validation_toaster.text);
  //   }
  // }

  private submitCreateStudyForm($ev, value: any) {
    $ev.preventDefault();    

    if (this.valForm.valid) {
      this.submitted = true;
      if (this.toUpdate) {       
        this.showSpinner = 1;  
        this.studyService.update(this.item.Id, this.valForm.value)
        .subscribe(
        entry => this.handleSubmitSuccess(entry),
        error => this.handleSubmitError(error)
        )
      }else{     
        this.studyService.create(this.valForm.value)
          .subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );
        }
      }
      
    else{
      this.toasterService.pop(this.required_validation_toaster.type, this.required_validation_toaster.title, this.required_validation_toaster.text);
    }
  }

  
   private submitDeleteStudy() {
        this.studyService.deleteStudy(this.study.studyId, this.study)
          .subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );
        this.showSpinner = 0;    
        this.backClicked();      
   }
   

  protected handleSubmitSuccess(entry) {  
    if (this.toUpdate) {
      this.showSpinner = 0;
      this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);      
      setTimeout(()=>{
        this.backClicked();  
      },1500);
    }
    else {
      this.showSpinner = 0;
      this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);        
      setTimeout(()=>{
        this.initStudy();
        this.backClicked();        
      },1500);
    }
  }
  protected handleSubmitError(error: any) {
    this.showSpinner = 0;
    this.submitted = false;
    //console.log(error);
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, error);      
  }

  private backClicked() {    
    this.router.navigate(['/study/list']);
  }

}
