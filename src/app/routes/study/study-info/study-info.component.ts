import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StudyService } from 'app/services/study.service';
import { Study } from 'app/model/study';

import { ToasterService } from 'angular2-toaster/src/toaster.service';
import { CommanConfig } from 'app/routes/CommanConfig';
import { User } from 'app/model/user';
import { Deliverable } from 'app/model/deliverable';
import { Userprivileges } from 'app/model/userprivileges';
import { PrivilageService } from 'app/services/privilage.service';
import { Group } from 'app/model/group';
import { Router } from '@angular/router';

@Component({
  selector: 'app-study-info',
  templateUrl: './study-info.component.html',
  styleUrls: ['./study-info.component.scss']
})
export class StudyInfoComponent extends CommanConfig implements OnInit {

  private item: any;
  private study : Study;
  private showSpinner: number = 1;
  private errorMessage;
  private users : Array<User>;
  private deliverables : Array<Deliverable>;
  private groups : Array<Group>;
  private appUserPrivilege : Userprivileges;
  private deliverablePrivilege : Userprivileges;
  private groupPrivilege : Userprivileges;
  

  constructor(
    private route: ActivatedRoute,
    private studyService : StudyService,
    private zone: NgZone,
    private router: Router,
    public toasterService: ToasterService,
    private privilage : PrivilageService
  ) {
    super(toasterService);
    this.init();  
   }

   init(){
    this.study = new Study();
    this.appUserPrivilege = this.studyService.appUserPrivilege;
    this.deliverablePrivilege  = this.studyService.deliverablePrivilege;
    this.groupPrivilege  = this.studyService.groupPrivilege;
   }

  ngOnInit() {
    this.item = this.route.snapshot.params;
    this.showSpinner = 1;
    this.studyService.getStudyById(this.item.Id).subscribe((data) => {
      this.zone.run(() => {
        this.study = data;
        this.showSpinner = 0;
        this.users = this.study.users;
        this.groups = this.study.groups;
        this.deliverables = this.study.deliverables;
      });
    },(error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });
  }

  private backClicked() {    
    this.router.navigate(['/study/list']);
  }

}
