
const Dashboard = {
    text: 'Dashhboard',
    link: '/dashboard',
    icon: 'icon-home',
    submenu: []
};

const configurations = {
    text: ' Configurations',
    link: '/configurations',
    icon: 'icon-settings',
    submenu: [
        {
            text: 'EDC Server',
            link: '/configurations/source/list'            
        },
        {
            text: 'Trial Names',
            link: '/configurations/trial/list'            
        },
        {
            text: 'Database Server',
            link: '/configurations/target/list'            
        },
        {
            text: 'Database Names',
            link: '/configurations/db/list'            
        }
    ]
};
const administration = {
    text: 'Administration',
    link: '/administration',
    icon: 'icon-lock',
    submenu: [
        {
            text: 'User',
            link: '/administration/user/list'
        },
        {
            text: 'Role',
            link: '/administration/role/list'
        }
    ]
};

const reports = {
    text: 'Reports',
    link: '/reports',
    icon: 'fa fa-bar-chart',
    submenu: [
        {
            text: 'Data Quality Checks',
            link: '/reports/datachecks/list'
        }
    ]
};

const bridges ={
    text: 'Bridges',
    link: '/bridges',
    icon: 'fa fa-server',
    submenu: [
        {
            text: 'Bridge List',
            link: '/bridges/list'
        },
        {
            text: 'Bridge Status',
            link: '/bridges/status'
        }
    ]
};

const databaseViewer = {
    text: 'Data Viewer',
    link: '/data-viewer',
    icon: 'fa fa-database',
    submenu: []
};
const xmlViewer = {
    text: 'XML Viewer',
    link: '/dbviewer',
    icon: 'fa fa-xing',
};


const headingMain = {
    text: 'Main Navigation',
    heading: true
};

const headingComponents = {
    text: 'Components',
    heading: true
};

const headingMore = {
    text: 'More',
    heading: true
};

export const menu = [
    Dashboard,
    bridges,
    databaseViewer,
    reports,
    configurations,
    administration
];
