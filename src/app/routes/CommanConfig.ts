import { ToasterService, ToasterConfig  , BodyOutputType } from 'angular2-toaster';

export class CommanConfig {
  
  public success_toaster: any;
  public fail_toaster: any;  
  public wait_toaster: any;

  public success_submitted_toaster: any;   
  public required_validation_toaster: any;  

  public toasterconfig: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-bottom-right',
    showCloseButton: true,
    tapToDismiss: true, 
    timeout: 0,
    bodyOutputType: BodyOutputType.TrustedHtml 
  });
  
  constructor(
    public toasterService: ToasterService
  ) {
  
    this.success_toaster = {
      type: 'success',
      title: 'Successful',
      text: 'The record has been updated successfully.'
    };
    this.fail_toaster = {
      type: 'error',
      title: 'Failed',
      text: 'The record was not updated.'
    };    
    this.wait_toaster = {
      type: 'wait',
      title: 'Processing',
      text: 'Please wait while transaction is being processed.'
    };
    this.success_submitted_toaster = {
      type: 'success',
      title: 'Successful',
      text: 'Request has been submitted successfully.'
    };

    this.required_validation_toaster = {
      type: 'error',
      title: 'Required',
      text: 'All the required fields must be entered.'
    };
  }
}