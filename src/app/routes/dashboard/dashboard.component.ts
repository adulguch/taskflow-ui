import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import { DashboardService } from 'app/services/dashboard.service';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { BridgeDashboard } from 'app/model/BridgeDashboard';
import { BridgeTransState } from 'app/model/BridgeTransState';
import { PrivilageService } from 'app/services/privilage.service';
import { Userprivileges } from 'app/model/userprivileges';
import { CommanConfig } from 'app/routes/CommanConfig';
import { DashboardCntByUser } from 'app/model/dashboardCntByUser';
import { DashboardCntByUserNType } from 'app/model/dashboardCntByUserNType';
import { DTaskCntByType } from 'app/model/d-TaskCntByType';
import { DTaskCntByStatus } from 'app/model/d-TaskCntByStatus';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends CommanConfig implements OnInit {


  //private gDashboardCntByUser : Array<GDashboardCntByType> ;
  //private gDashboardCntByType : Array<GDashboardCntByType> ;
  //private gDashboardCntByUser : Array<GDashboardCntByUser> ;
  
  private dashboardCntByUser : Array<DashboardCntByUser> ;
  private dashboardCntByUserNType : Array<DashboardCntByUserNType> ;
  
  private dTaskCntByType : Array<DTaskCntByType> ;
  private dTaskCntByStatus : Array<DTaskCntByStatus> ;


  private totalActiveBridges: number;
  private totalBridgesRanLastHr: number;
  private failedLastHour: number;    
  private transactionData ; 
  private totalTransactionCount: number;
  private transactionProcessed: number;
  private transactionRemaining: number;
  private transactionXMLCount: number;
  private transactionXMLRemaining: number;
  private viewAccess : boolean;
  private dashboardData : BridgeDashboard;
  // to display pops
  private errorMessage: string;
  private showSpinner: number = 1;  
  public success_toaster: any;
  

  //Privileges for configurations
  edcPrivilege : Userprivileges;
  trialPrivilege : Userprivileges;
  dbServerPrivilege : Userprivileges;
  dbNamePrivilege : Userprivileges;
  bridgePrivilege : Userprivileges;

  
  constructor(private http: Http,
              private zone: NgZone,
              private dashboardService: DashboardService,  
              private router: Router,
              public toasterService: ToasterService, 
              private privilage: PrivilageService ) {

  super(toasterService);   
  this.viewAccess = false;

 this.dashboardData = new BridgeDashboard();
 this.edcPrivilege = new Userprivileges();
 this.trialPrivilege = new Userprivileges();
 this.dbServerPrivilege = new Userprivileges();
 this.dbNamePrivilege = new Userprivileges();
 this.bridgePrivilege = new Userprivileges();
 
  }

  ngOnInit() {
    // read the dashboard transaction json file. This should be replaced by webService call     
    //this.http.get('assets/server/dashboard_transactions.json').subscribe((data) => {
    this.showSpinner = 1;        
    this.dashboardService.getDashboard().subscribe((data) => {
            this.zone.run(() => {              
                

                //this.gDashboardCntByType = data.gdashboardCntByType ;
                //this.gDashboardCntByUser = data.gdashboardCntByUser
                
                this.dashboardCntByUser = data.dashboardCntByUser;
                this.dashboardCntByUserNType =  data.dashboardCntByUserNType;
                this.dTaskCntByType = data.dtaskCntByType;
                this.dTaskCntByStatus = data.dtaskCntByStatus;  

                 
                /*this.totalActiveBridges = data.totalActiveBridge;                
                this.totalBridgesRanLastHr = data.executedLastHour;
                  this.failedLastHour = data.failedLastHour;
                this.dashboardData.edcServersCount = data.edcServersCount;
                this.dashboardData.trialNamesCount = data.trialNamesCount;
                this.dashboardData.databaseServerCount = data.databaseServerCount;
                this.dashboardData.databaseNameCount = data.databaseNameCount;
                */

                //this.transactionData = data.bridgeTransStatisticsList;
                this.showSpinner = 0;
                //console.log(this.gDashboardCntByUser);
                //console.log(this.gDashboardCntByType);


              });
            }, (error) => {
            //console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);

        });

  //   this.initializePrivileges(this.edcPrivilege);
  //   this.initializePrivileges(this.trialPrivilege);
  //   this.initializePrivileges(this.dbServerPrivilege);
  //   this.initializePrivileges(this.dbNamePrivilege);

  //   this.privilage.CheckBridgeSchedulerAccess().subscribe((data: Userprivileges) => {
  //     this.zone.run(() => {
  //         if(data.view === true ){
  //             this.viewAccess = true ;                
  //         }
  //     });
  //   });

  //   this.privilage.CheckBridgeAccess().subscribe((data : Userprivileges) => {
  //     this.zone.run(() => {
  //       this.assignPrivileges(data, this.bridgePrivilege);
  //     });
  //   });
    
  //   this.privilage.CheckEdcSourceAccess().subscribe((data : Userprivileges) => {
  //     this.zone.run(() => {
  //       this.assignPrivileges(data, this.edcPrivilege);
  //     });
  //   });
    
  //   this.privilage.CheckDbNameAccess().subscribe((data : Userprivileges) => {
  //     this.zone.run(() => {
  //       this.assignPrivileges(data, this.dbNamePrivilege);
  //     })
  //   });

  //   this.privilage.CheckDbTargetAccess().subscribe((data : Userprivileges) => {
  //     this.zone.run(() => {
  //       this.assignPrivileges(data, this.dbServerPrivilege);
  //     });
  //   });

  //   this.privilage.CheckTrialNameAccess().subscribe((data : Userprivileges) => {
  //     this.zone.run(() => {
  //       this.assignPrivileges(data, this.trialPrivilege);
  //     });
  //   });
  // }


  // initializePrivileges(privilege : Userprivileges){
    
  //   privilege.create = false;
  //   privilege.view = false;
  //   privilege.edit = false;
  //   privilege.delete = false;
  //   privilege.bridgeStatus = false;
  //   privilege.dataViewer = false;
  // }

  // assignPrivileges(responseData : Userprivileges, privilege : Userprivileges){
  //   privilege.view = responseData.view;
  //   privilege.create = responseData.create;
  //   privilege.edit = responseData.edit;
  //   privilege.delete = responseData.delete;
  //   privilege.bridgeStatus = responseData.bridgeStatus;
  //   privilege.dataViewer = responseData.delete;
  }

}
