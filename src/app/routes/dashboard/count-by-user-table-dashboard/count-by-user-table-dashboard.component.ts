import { Component, OnInit, Input,  NgZone, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { GDashboardCntByType } from 'app/model/g-dashboardCntByType';
//import { GDashboardCntByUser } from 'app/model/g-dashboardCntByUser';

import { DashboardCntByUser } from 'app/model/dashboardCntByUser';
import { DashboardCntByUserNType } from 'app/model/dashboardCntByUserNType';


@Component({
  selector: 'app-count-by-user-table-dashboard',
  templateUrl: './count-by-user-table-dashboard.component.html',
  styleUrls: ['./count-by-user-table-dashboard.component.scss']
})

export class CountByUserTableDashboardComponent implements OnChanges , OnInit {
private singleData;
private totalRecords: number = 0;

//@Input() gDashboardCntByUser: Array<GDashboardCntByUser> ;

@Input() dashboardCntByUser: Array<DashboardCntByUser> ;
@Input() dashboardCntByUserNType : Array<DashboardCntByUserNType> ;


 visible : boolean = true ;

  constructor() { }
  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {
  	if(!changes['dashboardCntByUser'].firstChange) 
  	{
  		this.singleData = changes['dashboardCntByUser'].currentValue ; 
  		this.totalRecords =  this.singleData.length; 
  	}
  }	

    removePanel(){
    this.visible = !this.visible;
  }
}
