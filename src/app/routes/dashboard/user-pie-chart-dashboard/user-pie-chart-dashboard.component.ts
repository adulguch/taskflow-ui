import { ColorsService } from 'app/shared/colors/colors.service';
import { Component, OnInit, Input,  NgZone, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { GDashboardCntByUser } from 'app/model/g-dashboardCntByUser';


@Component({
  selector: 'app-user-pie-chart-dashboard',
  templateUrl: './user-pie-chart-dashboard.component.html',
  styleUrls: ['./user-pie-chart-dashboard.component.scss']
})
export class UserPieChartDashboardComponent  implements OnChanges , OnInit {

  visiblePieChart : boolean = true ;
  
  @Input() gDashboardCntByUser: Array<GDashboardCntByUser> ;
  private pieData ;
  private pieOptions;
  	  
  private labels1:  Array<any> =[];
  private datasets1: Array<number>= []; 


  constructor(
  	private zone: NgZone, 
    private http: Http, 
    private router: Router, 
    private colors:ColorsService,
) { 
  

  this.pieData = {
    labels: [
        'BUG',
        'DEV',
        'TEST',
        'REVIEW'
    ],
    datasets: [{
        data: [0,0,0,0]        
    }],
};

this.pieOptions = {
    responsive: true
};

}

  ngOnInit() {

  }
  
  ngOnChanges(changes: SimpleChanges) {

  	if(!changes['gDashboardCntByUser'].firstChange) 
  	{
		Array.from(this.gDashboardCntByUser).forEach(a=> this.labels1.push(a.userName));
		Array.from(this.gDashboardCntByUser).forEach(a=> this.datasets1.push(a.taskCount));

		this.pieData.datasets = this.datasets1; 
		this.pieData.labels = this.labels1 ; 
  	
  	}


}

}
