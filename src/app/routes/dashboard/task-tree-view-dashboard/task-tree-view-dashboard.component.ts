

import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { CommanConfig } from 'app/routes/CommanConfig';
import { TreeModule , TreeNode } from 'angular-tree-component';
import { DashboardService } from 'app/services/dashboard.service';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { Task } from 'app/model/task';



@Component({
  selector: 'app-task-tree-view-dashboard',
  //template: '<tree-root [nodes]="nodes" [options]="options"></tree-root>'
  templateUrl: './task-tree-view-dashboard.component.html',
  //styleUrls: ['./task-tree-view-dashboard.component.scss']
})
export class TaskTreeViewDashboardComponent extends CommanConfig implements  OnInit {

private nodes; 
private nodes1; 
private childNodes ; 
private options; 	
private errorMessage: string;
private tasklist: any ;  
 


constructor(private http: Http,
              private zone: NgZone,
              public toasterService: ToasterService,
              private dashboardService: DashboardService,  
              private router: Router ) {

super(toasterService);
//this.defaultValues();
this.initTree()

this.nodes1 = [
  {
    name: 'DEV1',
    id : 5,
    hasChildren: true
  },
  {
    name: 'DEV2',
    id : 6,
    children: [
      {
        name: 'leaf',
        hasChildren: false,
        id : 7
      }
    ]
  }
] ;


}

ngOnInit() {

    this.dashboardService.getAllParentTask().subscribe((data) => {
        this.zone.run(() => {              
        console.log(data);
        this.nodes =  this.getTreeDate(data);

          });
        }, (error) => {
        //console.log(error);
        this.errorMessage = error;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);

    });

	this.options = {
	  getChildren: (node:TreeNode) => {  
      console.log("" + node.id);
      return  this.getAllChildData(node.id);
      //return this.nodes1 ;      
	}
	}

}

extractNode (task : Task){
return Object.assign({} , { name : task.name,
								 id: task.taskId, 
								 hasChildren: true } );

}


private getAllChildData(taskId : number ) {
    
     this.dashboardService.getAllChildTask(taskId).subscribe((data) => {
      //console.log("?????????????????????") ; 
      //console.log(data) ;
        this.zone.run( () => { 
        this.nodes1  =  this.getTreeDate(data);
        }      
        );
        }, (error) => {
        //console.log(error);
        //return null; 
        this.errorMessage = error;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage); 
    }); 
    
    // this.nodes1 = this.dashboardService.getAllChildTask(taskId).subscribe() ; 

    // console.log("return Child");
    // console.log(this.nodes1 ); 
    //return this.convertTree(this.nodes1) ;
    return this.nodes1 ; 
}

private getTreeDate(data : any) {
	return this.convertTree(data) ;  
}

private convertTree(data: Array<Task> ){
  console.log("-----------------");
  console.log(data);
	return Array.from(data).map( (a) => 
			Object.assign({} , { name : a.name,
								 id: a.taskId, 
								 hasChildren: true }
						  ));
}


private defaultValues() {	
this.nodes = [
  {
    name: 'DEV1',
    id : 5,
    hasChildren: true
  },
  {
    name: 'DEV2',
    id : 6,
    children: [
      {
        name: 'leaf',
        hasChildren: false,
        id : 7
      }
    ]
  }
] ;

}

initTree(){
	this.nodes = []
}

}
