import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'angular2-datatable';

import { SharedModule } from 'app/shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { DashboardService  } from 'app/services/dashboard.service';
import { PrivilageService } from 'app/services/privilage.service';
import { UserPieChartDashboardComponent } from './user-pie-chart-dashboard/user-pie-chart-dashboard.component';
import { CountByUserNTypeTableDashboardComponent } from './count-by-user-ntype-table-dashboard/count-by-user-ntype-table-dashboard.component';
import { CountByUserTableDashboardComponent } from './count-by-user-table-dashboard/count-by-user-table-dashboard.component';
import { ParaPieChartDashboardComponent } from './para-pie-chart-dashboard/para-pie-chart-dashboard.component';
import { StudyService } from 'app/services/study.service';
import { DeliverableService } from 'app/services/deliverable.service';
import { ParaStatusPieChartDashboardComponent } from './para-status-pie-chart-dashboard/para-status-pie-chart-dashboard.component';
import { TaskTreeViewDashboardComponent } from './task-tree-view-dashboard/task-tree-view-dashboard.component';
import { TreeModule } from 'angular-tree-component';
import { TaskService } from 'app/services/task.service';



@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    SharedModule,    
    DashboardRoutingModule,
    TreeModule
  ],
  declarations: [DashboardComponent, UserPieChartDashboardComponent, CountByUserNTypeTableDashboardComponent, 
  CountByUserTableDashboardComponent, ParaPieChartDashboardComponent, ParaStatusPieChartDashboardComponent, TaskTreeViewDashboardComponent ], 
   providers: [DashboardService, PrivilageService ,StudyService , DeliverableService, TaskService]
})
export class DashboardModule { 
		constructor() {
     	console.log('DashboardModule loaded......');
	   }
   }
