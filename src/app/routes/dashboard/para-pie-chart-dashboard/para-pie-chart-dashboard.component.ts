import {  ViewChild, Component, OnInit, Input,  NgZone, OnChanges, SimpleChanges, SimpleChange  } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { StudyService } from 'app/services/study.service';
import { DeliverableService } from 'app/services/deliverable.service';
import { CommanConfig } from 'app/routes/CommanConfig';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { DashboardService } from 'app/services/dashboard.service';
import { DTaskCntByType } from 'app/model/d-TaskCntByType';



@Component({
  selector: 'app-para-pie-chart-dashboard',
  templateUrl: './para-pie-chart-dashboard.component.html',
  styleUrls: ['./para-pie-chart-dashboard.component.scss']
})
export class ParaPieChartDashboardComponent extends CommanConfig  implements OnChanges , OnInit {
 
  @Input() dTaskCntByType: Array<DTaskCntByType> ;

  visiblePieChart : boolean = true ;
  dTaskCntByType1: boolean = true ;
  private pieData ;
  private pieOptions;
  private studyForm: FormGroup;
  private delvForm: FormGroup;
  private studyId;
  private deliverableId;
  private studyList ;
  private tabledata2 ;
  private deliverableList ;
  private errorMessage;
  private showSpinner: number = 1;
  private valTaskForm: FormGroup;  
  private selectedStudy;  
  private selectedDeliverable;
  private chartDate : Array<DTaskCntByType> ; 
  private taskCountByTypelist  ;
  private tableData = []; 
  private totalDataCount = 0;
  
  private labels:  Array<any> =[];
  private datasets: Array<number>= []; 


  constructor(
  	
  	private fb: FormBuilder, 
   	private zone: NgZone, 
  	public toasterService: ToasterService,
  	private studyService: StudyService, 
  	private  deliverableService: DeliverableService,
  	private dashboardService: DashboardService

	) {
    
    super(toasterService);

    this.studyForm = this.fb.group({
    	'studyId': [this.studyId, Validators.required]
    });

    this.delvForm = this.fb.group({
    	'deliverableId': [this.deliverableId, Validators.required]
    });
    
    this.pieData = {
    labels: [
        'DEV',
        'TEST',
        'BUG',
        'REVIEW'
      
    ],
    datasets: [{
        data: [0,0,0,0]        
    }],
	};

 this.pieOptions = {
    responsive: true
	};
}

  ngOnInit() {
  	/// Gettig Active study assigned to user
  	this.studyForm.get('studyId').patchValue(-1);
  	this.selectedStudy = -1 ;
  	this.delvForm.get('deliverableId').patchValue(-1);	
  	this.selectedDeliverable = -1 ;

	this.studyService.getAll().subscribe((data) => {
	this.zone.run(() => {
        this.studyList = data;

	});
	}, (error) => {
	    this.errorMessage = error;
	    this.showSpinner = 0;
	    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

    this.deliverableService.getAll().subscribe((data) => {
	this.zone.run(() => {
	    this.deliverableList = data;
	});
	}, (error) => {
	    this.errorMessage = error;
	    this.showSpinner = 0;
	    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });  
    
   
  }
 
  ngOnChanges(changes: SimpleChanges) {

  	if(!changes['dTaskCntByType'].firstChange) 
  	{
		this.totalDataCount = 0;

    Array.from(this.dTaskCntByType).forEach(a=> this.labels.push(a.taskType));    
    Array.from(this.dTaskCntByType).forEach( a=> {

      //this.labels.push(a.taskType)}
      if(a.taskType == 'DEV') {
        this.datasets[0] = a.taskCount ;
        this.totalDataCount = this.totalDataCount + a.taskCount ; 
      }
      if(a.taskType == 'TEST') {
       this.datasets[1] = a.taskCount ;
      this.totalDataCount = this.totalDataCount + a.taskCount ; 
  
      }
      if(a.taskType == 'BUG') {
        this.datasets[2] = a.taskCount ; 
        this.totalDataCount = this.totalDataCount + a.taskCount ; 

      }
      if(a.taskType == 'REVIEW') {
        this.datasets[3] = a.taskCount ; 
        this.totalDataCount = this.totalDataCount + a.taskCount ; 
      }

    });
		
  	this.pieData.datasets = this.datasets; 
    this.pieData.labels = this.labels ; 
  
  	}
}

 private onStudyChange(selectedStudy: any) {     
 	this.selectedStudy = selectedStudy;

	this.getchartData(this.selectedStudy , this.selectedDeliverable);
	 if(selectedStudy == -1 ){
	 	this.getDeliverables();
 	}else {
 		this.getDeliverablesByStudy(selectedStudy);
 	}
 }

private onDeliverableChange(selectedDeliverable : any) {
     /// Gettig active deliverable for given stiudy
     this.getchartData(this.selectedStudy , selectedDeliverable);
}

 
 private getDeliverablesByStudy(selectedStudy){
     /// Gettig active deliverable for given stiudy
      this.deliverableService.getDeliverablesByStudyId(selectedStudy).subscribe((data) => {
      this.zone.run(() => {
          this.deliverableList = data;
          console.log('response ....');
          console.log(this.deliverableList);

      });
    
      }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });

 }

 private getDeliverables(){
     /// Gettig active deliverable for given stiudy
      this.deliverableService.getAll().subscribe((data) => {
      this.zone.run(() => {
          this.deliverableList = data;
      });
    
      }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });
 }

private getchartData(selectedStudy, selectedDeliverable){
		
      this.dashboardService.getTaskCntByType( this.selectedStudy , selectedDeliverable).subscribe((data) => {
      this.zone.run(() => {
          this.taskCountByTypelist = data;

          if(this.taskCountByTypelist.length > 0){
          	this.getchart(this.taskCountByTypelist);	
          }else {
  			this.resetChart();
          }
          
      });
      }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });	
	
	}

private getchart(data : Array<DTaskCntByType>){
		

    this.resetChart();
		
     this.dTaskCntByType = data ;  
    //Array.from(data).forEach(a=> this.labels.push(a.taskType));
    //Array.from(data).forEach(a=> this.datasets.push(a.taskCount));  
    this.totalDataCount  =  0 ;

    Array.from(data).forEach(a=> this.labels.push(a.taskType));
    Array.from(data).forEach( a=> {

      //this.labels.push(a.taskType)}
      if(a.taskType == 'DEV') {
        this.datasets[0] = a.taskCount ; 
        this.totalDataCount = this.totalDataCount + a.taskCount ; 

      }
      if(a.taskType == 'TEST') {
       this.datasets[1] = a.taskCount ;  
       this.totalDataCount = this.totalDataCount + a.taskCount ; 
     
      }
      if(a.taskType == 'BUG') {
        this.datasets[2] = a.taskCount ; 
        this.totalDataCount = this.totalDataCount + a.taskCount ; 
      
      }
      if(a.taskType == 'REVIEW') {
        this.datasets[3] = a.taskCount ;
        this.totalDataCount = this.totalDataCount + a.taskCount ; 
      }

    });

		this.pieData.datasets = this.datasets; 
    this.pieData.labels = this.labels ; 

	}

private resetChart(){
		//this.pieData.datasets =  [];
		//this.pieData.labels = [];
    
    this.dTaskCntByType = [] ; 

		this.datasets = [0,0,0,0];
		this.labels = [];

    this.pieData = {
    labels: [ 
        'DEV',
        'TEST',
        'BUG',
        'REVIEW'
    ],
    datasets: [{
        data: [0,0,0,0]        
    }],
	};
 }

  removePanel(){
    this.visiblePieChart = !this.visiblePieChart;
  }

}
