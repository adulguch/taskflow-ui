import { PagesModule } from './pages/pages.module';
import { NgModule, Component, OnInit, NgZone } from '@angular/core';
import { RouterModule , PreloadAllModules } from '@angular/router';
import { TranslatorService } from '../core/translator/translator.service';
import { MenuService } from '../core/menu/menu.service';
import { SharedModule } from '../shared/shared.module';
import { Http } from '@angular/http';
import { AuthenticationService } from 'app/services/authentication.service';


///import { menu } from './menu';
import { routes } from './routes';

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forRoot(
            routes 
            ,
            {  preloadingStrategy: PreloadAllModules}
            //,
            //{ enableTracing: true } // <-- debugging purposes only
            ),
        PagesModule
    ],
    declarations: [],
    exports: [
        RouterModule
    ]
})

export class RoutesModule {

    menu: { text: string,heading?: boolean
                    ,link?: string
                    ,elink?: string
                    ,target?: string
                    ,icon?: string
                    ,alert?: string
                    ,submenu?: Array<any>} [];                     

    constructor(public menuService: MenuService
                ,tr: TranslatorService
                ,private zone: NgZone
                ,private http: Http
                , private  authenticationService: AuthenticationService) {
       
       //this.menuService.addMenu(menu); 

        if(this.authenticationService.loggedIn()) 
        {
            menuService.constructmenu().subscribe(
            result => {
            menuService.addMenu(result);
             });

             this.authenticationService.getAuthUserDetails().subscribe((data) => {});
        }
    }    

}
