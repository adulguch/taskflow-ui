import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { CommanConfig } from 'app/routes/CommanConfig';
import { PrivilageService } from 'app/services/privilage.service';
import { TaskService } from 'app/services/task.service';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { Task } from 'app/model/task';
import { TaskSearch} from 'app/model/task-search';
import { Userprivileges } from 'app/model/userprivileges';
import { StudyService } from '../../../services/study.service';
import { TaskProperty } from 'app/model/taskProperty';
import { TaskType } from 'app/model/TaskType';
import { AppUserService } from 'app/services/appUser.service';
import { SelectData } from 'app/model/selected-data';
import { DeliverableService } from 'app/services/deliverable.service';
import { UserService } from '../../../services/user.service';


@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent extends CommanConfig implements OnInit {

  private writeAccess : Boolean = false ;
  private deleteAccess : Boolean = false ;
  private createAccess : Boolean = false ;  

  public success_toaster: any;
  private showSpinner: number = 1;
  private showSpinnerReset: number = 0;
  private totalRecords: number = 0;
  private selectedRow: number;

  private searchForm: FormGroup;  
  private taskSearch: TaskSearch;
  private errorMessage;

  private singleData;
//   private dilevrableList;
  private originalData;
  private studyList;
  private id;
  private selectData : SelectData;
  private usersList;
  private allUsersList;
  private assignUsersList;
  private deliverablesList;
  private allDeliverablesList;
  private linkedDeliverablesList;
  private assigndeliverablesList;
  private taskProperty: TaskProperty ;
  private taskTypeList: Array<TaskType> ; 
  private taskStatusList: Array<String> ; 
  private taskPriorityList: Array<String> ;

    
  @ViewChild('cantCreateSubTaskModal')  cantCreateSubTaskModal: any;
   
  constructor(
  	private zone: NgZone, 
    private fb: FormBuilder, 
    private route: ActivatedRoute,
    private http: Http, 
    private router: Router, 
  	public toasterService: ToasterService,
  	private privilage: PrivilageService,
    private taskService: TaskService,
    private deliverableService : DeliverableService,
    private appUserService : AppUserService,
    private studyService: StudyService,
    private userService : UserService

   ) {
     super(toasterService);
     this.initTaskSearch();
 }


  private initTaskSearch() {
    this.taskSearch = new TaskSearch();
    
    this.searchForm = this.fb.group({
        
        'studyId': [this.taskSearch.studyId],
        'deliverableId': [this.taskSearch.deliverableId],
        'priority': [this.taskSearch.priority],     
        'type': [this.taskSearch.type],
        'status': [this.taskSearch.status],
        'assigneeId': [this.taskSearch.assigneeId],
        'linkedDeliverableId': [this.taskSearch.linkedDeliverableId]
    });
  }

  ngOnInit() {
    this.showSpinner = 0;
    this.privilage.CheckTaskAccess().subscribe((data: Userprivileges) => {

    this.zone.run(() => {
        if(data.edit === true ){
            this.writeAccess = true ;                
        }if(data.delete === true){
            this.deleteAccess = true ;
        }if(data.create === true ){
            this.createAccess = true ;                
        }

      });
    }, (error) => {
    //console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

    this.showSpinner = 1;
      this.taskService.getAll().subscribe((data) => {
          this.zone.run(() => {
             this.singleData = data;
              
              //console.log(this.singleData);
              
              this.originalData = this.singleData;
              this.totalRecords = this.singleData.length;
              this.showSpinner = 0;

          });
      }, (error) => {
          console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });

      this.studyService.getStudyByUsers().subscribe((data) => {
        this.zone.run(() => {
            this.studyList = data;
        });
        }, (error) => {
            // console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });

        this.taskService.getTaskProperties().subscribe((data) => {
            this.zone.run(() => {
                this.taskProperty = data;
                this.taskTypeList = this.taskProperty.taskTypeList ;
                  this.taskStatusList= this.taskProperty.taskStatusList ;
                  this.taskPriorityList = this.taskProperty.taskPriorityList;
            });
            }, (error) => {
                // console.log(error);
                this.errorMessage = error;
                this.showSpinner = 0;
                this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
            });     
            this.onLoad();
 }


 onStudyChange(studyId){
    this.id = studyId;
    this.showSpinnerReset = 1;
    this.appUserService.getFilteredUsersByStudyId(studyId).subscribe((data) => {
      this.zone.run(() => {
          this.allUsersList = [];
          this.selectData = data;
          this.showSpinnerReset = 0; 
          this.usersList = this.selectData.filteredData;  
          this.assignUsersList = this.selectData.assignedData; 
      });
     }, (error) => {
      //console.log(error);
      this.errorMessage = error;
      this.showSpinnerReset = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

    this.deliverableService.getFilteredDeliverablesByStudyId(studyId).subscribe((data) => {
        this.zone.run(() => {
            this.allDeliverablesList = [];
            this.selectData = data;
            this.showSpinner = 0; 
            
            this.deliverablesList = this.selectData.filteredData;
            this.assigndeliverablesList = this.selectData.assignedData.concat(this.selectData.assignedData2);  
        });
       }, (error) => {
        //console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });
}

  public addNew() {
      //this.router.navigate(['/task/details']);
      this.router.navigate(['/task/details-edit']);
  }

  public editRow(item) {
      const id = { 'taskId': item.taskId }
      //this.router.navigate(['/task/details-read/', id]);
      this.router.navigate(['/task/details-read/' + id.taskId ]);
      //this.router.navigate(['/task/details',id], { queryParams: id, skipLocationChange: true});
      //this.router.navigate(['/task/details-read',id], { queryParams: id, skipLocationChange: true});

  }

  public setClickedRow(index: number) {
    this.selectedRow = index;
}
private onQuickFilterChanged($event) {
    console.log($event.html.keyName);
    //console.log($event.target.value);         
    if ($event.target.value) {
        this.singleData = this.originalData.filter(item => {
            return Object.keys(item).some(keyName => {                     
                 if (item[keyName] !== null) {
                    return item[keyName].toString().match(new RegExp($event.target.value, 'i'));
                 }
            });
        });
    } else {
        this.singleData = this.originalData;
    }
}  


public refresh() {
    this.showSpinner = 1;
    this.taskService.getAll().subscribe((data) => {
        this.zone.run(() => {
            this.singleData = data;  
            this.originalData = this.singleData;
            this.totalRecords = this.singleData.length;
            this.showSpinner = 0;
        });
    }, (error) => {
        //console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });
}

public reset() {  
 this.searchForm = this.fb.group({
        
    'studyId': [null],
    'deliverableId': [null],
    'priority': [null],     
    'type': [null],
    'status': [null],
    'assigneeId': [null],
    'linkedDeliverableId' : [null]
});
           
  this.assigndeliverablesList = [];
  this.assignUsersList = [] ;
  this.onLoad();
}

public search(){
    this.showSpinner = 1;
    this.taskService.getFilteredTasks(this.searchForm.value).subscribe((data) => {
        this.zone.run(() => {
            this.singleData = data;
            this.originalData = this.singleData;
            this.totalRecords = this.singleData.length;
            this.showSpinner = 0;

        });
    }, (error) => {
        console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });
}

public TaskByAssignee(){
    this.showSpinner = 1;
    this.taskService.getTasksByAssignee().subscribe((data) => {
        this.zone.run(() => {
            this.singleData = data;
            this.originalData = this.singleData;
            this.totalRecords = this.singleData.length;
            this.showSpinner = 0;
        });
    }, (error) => {
        console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });
  }


  public onLoad(){
    this.deliverableService.getAll().subscribe((data) => {
        this.zone.run(() => {
            this.allDeliverablesList = data;
            this.linkedDeliverablesList = data;          
        });
    }, (error) => {
        //console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });
    this.userService.getAll().subscribe((data) => {
        this.zone.run(() => {
            this.allUsersList = data;              
        });
    }, (error) => {
        //console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

  }
}
