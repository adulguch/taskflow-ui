import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/guards/auth.guard';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskDetailsReadComponent } from './task-details-read/task-details-read.component';
import { TaskDetailsEditComponent } from './task-details-edit/task-details-edit.component';
import { TaskSubtaskComponent } from './task-subtask/task-subtask.component';


const routes: Routes = [
 { path: '', redirectTo: 'list',  canActivate: [AuthGuard] }, 
 { path: 'details', component: TaskDetailsComponent,   canActivate: [AuthGuard] }, 

 { path: 'details-read/:id', component: TaskDetailsReadComponent, canActivate: [AuthGuard] }, 
 
 { path: 'details-read', component: TaskDetailsReadComponent,   canActivate: [AuthGuard] }, 
 { path: 'details-edit', component: TaskDetailsEditComponent,   canActivate: [AuthGuard] },    
 
 { path: 'detail-subtask', component: TaskSubtaskComponent,   canActivate: [AuthGuard] },    
 
 { path: 'list', component: TaskListComponent,   canActivate: [AuthGuard] },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskRoutingModule { }
