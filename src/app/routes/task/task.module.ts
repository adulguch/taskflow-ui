import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableModule } from 'angular2-datatable';
import { SharedModule } from 'app/shared/shared.module';
import { TaskRoutingModule } from './task-routing.module';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { PrivilageService } from 'app/services/privilage.service';
import { TaskService } from 'app/services/task.service';
import { StudyService } from 'app/services/study.service';
import { DeliverableService } from 'app/services/deliverable.service';
import { AppUserService } from 'app/services/appUser.service';
import { TaskDetailsReadComponent } from './task-details-read/task-details-read.component';
import { TaskDetailsEditComponent } from './task-details-edit/task-details-edit.component';
import { TaskSubtaskComponent } from './task-subtask/task-subtask.component';
import { FileSelectDirective } from 'ng2-file-upload';
import { FileUploadsConfig } from 'app/routes/FileUploadsConfig' ; 
import { UserService } from '../../services/user.service';


@NgModule({
  imports: [
    CommonModule,
    TaskRoutingModule,
    DataTableModule,
    SharedModule

  ],
  declarations: [TaskListComponent, TaskDetailsComponent, TaskDetailsReadComponent, TaskDetailsEditComponent, TaskSubtaskComponent, FileSelectDirective ],
   providers: [TaskService , PrivilageService, StudyService, DeliverableService, AppUserService ,UserService, FileUploadsConfig]

})
export class TaskModule { }
