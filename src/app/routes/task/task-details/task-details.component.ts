import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Location } from '@angular/common';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { Http } from '@angular/http';
import { TaskService } from 'app/services/task.service';
import { AppUserService } from 'app/services/appUser.service';
import { StudyService } from 'app/services/study.service';
import { DeliverableService } from 'app/services/deliverable.service';
import { Task } from 'app/model/task';
import { AppUser } from 'app/model/appuser'  ; 
import { CommanConfig } from 'app/routes/CommanConfig';
import { TaskProperty } from 'app/model/taskProperty';
import { TaskType } from 'app/model/TaskType';
import { TaskComment } from 'app/model/taskComment';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.scss']
})
export class TaskDetailsComponent extends CommanConfig implements OnInit {

  private writeAccess : Boolean = false ;
  private deleteAccess : Boolean = false ;
  private createAccess : Boolean = false ;  

  public success_toaster: any;
  private showSpinner: number = 1;
  private totalRecords: number = 0;

  private valTaskForm: FormGroup;  
  private task: Task;
  private submitted: boolean = false;
  private toUpdate;
  private toSubTask;
  private editOrSubTask: boolean = false;  
  private errorMessage;
  private item: any ;
  private studyList ;
  private deliverableList ;
  private appUserList ;
  private parentTaskList ;
  private parentTaskType = null  ;  	  
  private levelOneSubTaskList  ;      
  private taskProperty: TaskProperty ;
  private taskTypeList: Array<TaskType> ; 
  private taskStatusList: Array<String> ; 
  private taskPriorityList: Array<String> ;

  private availableSubtaskTypeList: Array<TaskType> ;

  private buttonCreateDev: boolean =  false;
  private buttonCreateTest: boolean =  false;
  private buttonCreateBug: boolean =  false;
  private buttonCreateReview: boolean =  false;
  private isButtonVisible = true;

  private createAvailableSubtaskButtonflag: boolean=false
  
  private taskCommentList : TaskComment[] = [];
  private toShowForm :boolean =  false; 
  private toEdit :boolean =  false; 
  private allParentTaskList ;
  private editButtonFlag : boolean = true ;


  @ViewChild('cantCreateSubTaskModal')  cantCreateSubTaskModal: any;
  @ViewChild('cantCreateTaskModal')  cantCreateTaskModal: any;

  constructor(
  	private zone: NgZone, 
    private fb: FormBuilder, 
    private route: ActivatedRoute,
    private http: Http, 
	  private taskService: TaskService,
    private router: Router, 
  	public toasterService: ToasterService,
  	private studyService: StudyService, 
  	public deliverableService: DeliverableService,  		
  	private appUserService: AppUserService  

   ) {
     super(toasterService);
     //this.initTaskSearch(); 
     this.task = new Task();
     this.levelOneSubTaskList = [];

    this.valTaskForm = this.fb.group({
        'taskId': [this.task.taskId],
        'name': [this.task.name, Validators.required],             
	    	'type': [this.task.type], // dev, QC        
  		  'level': [this.task.level],
		    'parentId': [this.task.parentId],		
		    'description': [this.task.description],				
        'studyId': [this.task.studyId, Validators.required],        
        'deliverableId': [this.task.deliverableId, Validators.required],
        'priority': [this.task.priority, Validators.required],                
        'status': [this.task.status],
        //'user': [this.task.user],
        'assigneeId': [this.task.assigneeId],        
        'assigneeName': [this.task.assigneeName],
        'startDate': [this.task.startDate],
        'endDate': [this.task.endDate],        
        'comment': [this.task.comment],
        'comments': [this.task.comments]
    });
 }

  ngOnInit() {
    this.isButtonVisible = false;
	// Get all Source EDC Server Names
	this.taskService.getTaskProperties().subscribe((data) => {
	this.zone.run(() => {
	    this.taskProperty = data;
	    this.taskTypeList = this.taskProperty.taskTypeList ;
		  this.taskStatusList= this.taskProperty.taskStatusList ;
	  	this.taskPriorityList = this.taskProperty.taskPriorityList;
	    
	});
	}, (error) => {
	    // console.log(error);
	    this.errorMessage = error;
	    this.showSpinner = 0;
	    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
	});


	/// Gettig Active study assigned to user
	this.studyService.getStudyByUsers().subscribe((data) => {
	this.zone.run(() => {
	    this.studyList = data;
	});
	}, (error) => {
	    // console.log(error);
	    this.errorMessage = error;
	    this.showSpinner = 0;
	    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

	this.showSpinner = 1;    
    this.item = this.route.snapshot.params;
    if (this.item.taskId) {     
        this.isButtonVisible = true;
      	this.allParentTaskList = [] ; 
      // getting all parent of given task id for breadcrumb display   
          this.taskService.getAllParentTask(this.item.taskId).subscribe((data) => {        
          this.zone.run(() => {
          this.allParentTaskList = data;
          this.showSpinner = 0;
        });

      }, (error) => {
            // console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });


      // Get the details of the requested BridgeId
        	this.taskService.getTaskById(this.item.taskId).subscribe((data) => {        
          this.zone.run(() => {
          this.task = data;
          this.taskCommentList =  this.task.comments;
          this.toUpdate = true;
          this.parentTaskType = this.task.type;
          this.toShowForm = true;
		      this.valTaskForm.patchValue(this.task);          
		      this.showSpinner = 0;
          this.getUserListByStudyId(this.task.studyId);  		  
		      this.getDeliverablesByStudy(this.task.studyId);
          this.showDetailsDisableElement();
      
        });

    	}, (error) => {
            // console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });

    //create subtask Available Links
    //console.log( this.task);
    this.createAvailableSubtaskList(this.item.taskId);
    this.editOrSubTask = true;

    // getting subtask of current task to display 
    this.getOneLevelSubtask(this.item.taskId);
    //console.log(this.levelOneSubTaskList);

	}else {
		this.toUpdate = false;
		this.showSpinner = 0;  
		}
	}

 submitForm($ev, value: any) {
    $ev.preventDefault(); 	
    if (this.valTaskForm.valid) {
      this.submitted = true;
           
      if (this.toUpdate && ! this.toSubTask) {    	
     	
     	  if(this.toShowForm){
          
       		  this.showDetailsEnableElement();
       		  this.toShowForm = false;

            this.subTaskEnableElements();
            this.showSpinner = 1;
            this.taskService.update(this.item.taskId, this.valTaskForm.value)
            .subscribe(
            entry => this.handlesave(entry),
            error => this.handleSubmitError(error)
            );
            // add comments to exiting comment list
            this.addComment(this.task , this.valTaskForm.get('comment').value);
            this.valTaskForm.get('comment').patchValue('');
            this.showDetailsDisableElement();
            this.submitted = false;
            this.toShowForm = true; 

     	  }
		    else if(this.toEdit){

            this.toEdit = false;
       		  this.editEnableElement();	

            this.subTaskEnableElements();
            this.showSpinner = 1;
            this.taskService.update(this.item.taskId, this.valTaskForm.value)
            .subscribe(
            entry => this.handleSubmitSuccess(entry),
            error => this.handleSubmitError(error)
            );
    	  }

/*     	this.subTaskEnableElements();
          this.showSpinner = 1;
          this.taskService.update(this.item.taskId, this.valTaskForm.value)
          .subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );
*/
       }        
      else {
        this.showSpinner = 1;
        //console.log('In create form');
    		//console.log(this.valTaskForm.value);		
		    this.parentTaskList =  null;		

		if(this.toSubTask)
		{
			this.subTaskEnableElements();
			this.toSubTask = false;
			
			this.taskService.create(this.valTaskForm.value)
          	.subscribe(
          	entry => this.handleSubmitSuccess(entry),
          	error => this.handleSubmitError(error)
          	);
          	this.showSpinner = 0;
		}else {				
			if(this.valTaskForm.get('type').value != 'DEV') {				
        
        if(this.valTaskForm.get('parentId').value == null ){
					this.submitted = false; 
					this.showSpinner = 0;
					this.cantCreateTaskModal.show();
					
				}else {
					this.taskService.create(this.valTaskForm.value)
          			.subscribe(
          			entry => this.handleSubmitSuccess(entry),
			        error => this.handleSubmitError(error)
        	  			);
          			this.showSpinner = 0;
				}

			}else{
					this.taskService.create(this.valTaskForm.value)
	      			.subscribe(
	      			entry => this.handleSubmitSuccess(entry),
			        error => this.handleSubmitError(error)
	    	  			);
	      			this.showSpinner = 0;
			}
		}
		/*this.taskService.create(this.valTaskForm.value)
          .subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );
          this.showSpinner = 0;*/ 
      }
    }else{
      this.toasterService.pop(this.required_validation_toaster.type, this.required_validation_toaster.title, this.required_validation_toaster.text);
    }
 }  

 private createAvailableSubtaskList(taskId){
   
   this.showSpinner = 1;       
   //need to get available  task type to create subtask , if level 2 tasktype is null and we can not create subtask.   
   this.taskService.getAvailableTaskPropertiesForTaskById(taskId).subscribe((data) => {
     this.zone.run(() => {
     this.taskProperty = data;        
     //this.taskTypeList = this.taskProperty.taskTypeList ;
     this.taskStatusList= this.taskProperty.taskStatusList ;
     this.taskPriorityList = this.taskProperty.taskPriorityList;
     this.availableSubtaskTypeList = this.taskProperty.taskTypeList;
      
     if(this.availableSubtaskTypeList.length > 0) {
       this.createAvailableSubtaskButtonflag = true;
     }

    });
  }, (error) => {
      // console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
  })
 }

 private createSubTask(valTaskForm : FormGroup , taskType: any ){
   
    this.createAvailableSubtaskButtonflag = false;
    this.editButtonFlag = false

    //just need to set taskType as per type button click and set task fotm into new form. 
    this.valTaskForm.get('parentId').patchValue(this.valTaskForm.get('taskId').value); 
    this.valTaskForm.get('type').patchValue(taskType); 
    
    var taskList: Task[] = [];
    taskList.push(this.task);
    this.parentTaskList = taskList; 
    this.valTaskForm.get('taskId').patchValue('');  
    
    //this.valTaskForm.get('level').patchValue(2);  
    this.valTaskForm.get('name').patchValue('') ;
    this.valTaskForm.get('description').patchValue('') ;
    this.valTaskForm.get('comment').patchValue('') ;
    this.taskCommentList= [];  
    this.toSubTask = true;

    this.subTaskDisableElements();
 }

 private editTask(){
 	this.editDisableElement();
 	 this.toShowForm = false ; 
   this.toEdit= true;
   this.editButtonFlag = false;
   this.createAvailableSubtaskButtonflag = false;

 }

 private backClicked() {
  	this.router.navigate(['/task/list']);
  }

private taskById(taskId) {
    
    this.ngOnInit_Edit(taskId) ;
    this.createAvailableSubtaskButtonflag = true ; 
    //console.log(this.createAvailableSubtaskButtonflag +' ' + this.editButtonFlag ); 
    
    this.editButtonFlag = true ; 
    //this.createAvailableSubtaskList(taskId);

    this.getOneLevelSubtask(taskId); 
}


 protected handleSubmitSuccess(entry) {
    this.showSpinner = 0;
    this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);
    setTimeout(() => {
      this.backClicked();
    }, 1500);
  }
 
protected handlesave(entry) {
    this.showSpinner = 0;
    this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);
/*    setTimeout(() => {
      this.backClicked();
    }, 1500);*/
  }
 

 protected handleSubmitError(error: any) {
    this.showSpinner = 0;
    this.submitted = false;
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, error);
  }

 private subTaskEnableElements(){
	  this.valTaskForm.get('type').enable();
    this.valTaskForm.get('deliverableId').enable();
    this.valTaskForm.get('studyId').enable();
 }

 private subTaskDisableElements(){
	  this.valTaskForm.get('type').disable();
    this.valTaskForm.get('deliverableId').disable();
    this.valTaskForm.get('studyId').disable();

    //this.valTaskForm.get('type').enable();
    this.valTaskForm.get('name').enable();
	this.valTaskForm.get('taskId').enable();
    this.valTaskForm.get('description').enable();
    this.valTaskForm.get('priority').enable();    
	this.valTaskForm.get('status').enable();
    this.valTaskForm.get('assigneeId').enable();
    this.valTaskForm.get('parentId').enable();


 }

 private editEnableElement(){

	this.valTaskForm.get('type').enable();
	this.valTaskForm.get('deliverableId').enable();
	this.valTaskForm.get('studyId').enable();    
	this.valTaskForm.get('taskId').enable();
	this.valTaskForm.get('name').enable();
    
 }


 private editDisableElement(){

	this.valTaskForm.get('type').disable();
    this.valTaskForm.get('deliverableId').disable();
    this.valTaskForm.get('studyId').disable();    
    this.valTaskForm.get('taskId').disable();
    this.valTaskForm.get('name').disable();

    this.valTaskForm.get('taskId').enable();
    this.valTaskForm.get('description').enable();
    this.valTaskForm.get('priority').enable();    
	this.valTaskForm.get('status').enable();
    this.valTaskForm.get('assigneeId').enable();

    
   }



 private showDetailsEnableElement(){

    this.valTaskForm.get('type').enable();
    this.valTaskForm.get('deliverableId').enable();
    this.valTaskForm.get('studyId').enable();        
    this.valTaskForm.get('taskId').enable();
    this.valTaskForm.get('name').enable();
    this.valTaskForm.get('description').enable();
    this.valTaskForm.get('priority').enable();    
	this.valTaskForm.get('status').enable();
    this.valTaskForm.get('assigneeId').enable();
 
 }

 private showDetailsDisableElement(){
	
	this.valTaskForm.get('type').disable();
    this.valTaskForm.get('deliverableId').disable();
    this.valTaskForm.get('studyId').disable();    
    this.valTaskForm.get('taskId').disable();
    this.valTaskForm.get('name').disable();
    this.valTaskForm.get('description').disable();
    this.valTaskForm.get('priority').disable();    
	this.valTaskForm.get('status').disable();
    this.valTaskForm.get('assigneeId').disable();
 
 }

 private onTaskTypeChange(selectedSourceFk: any) {
  /*  // console.log(selectedSourceFk);
    let tmp_selectedSourceFk = +selectedSourceFk;
    this.trialListFiltered = this.trialList.filter((x) => x.sourceFk === tmp_selectedSourceFk);
    // console.log(this.trialListFiltered);
  */
}

private onStudyChange(selectedStudy: any) { 
   this.getDeliverablesByStudy(selectedStudy); 
   this.getUserListByStudyId(selectedStudy);
   this.OnSelectStudy(selectedStudy);
}

private onDeliverableChange(selectedDeliverable : any) {
   this.OnSelectDeliverable(selectedDeliverable);
}


private OnSelectStudy(selectedStudyId: any) {
    if(this.valTaskForm.get('deliverableId').value 
        && this.valTaskForm.get('type').value ){
        
        this.getSubtaskByStudyandDeliverable(selectedStudyId
            , this.valTaskForm.get('deliverableId').value
            , this.valTaskForm.get('type').value );
    }
  } 

private OnSelectDeliverable(selectedDeliverableId: any) {
    if(this.valTaskForm.get('studyId').value 
        && this.valTaskForm.get('type').value ){

        this.getSubtaskByStudyandDeliverable(this.valTaskForm.get('studyId').value
            , selectedDeliverableId
            , this.valTaskForm.get('type').value );
    }
  } 

 
 private OnSelectTaskType(selectedtaskType: any) {

 	//console.log('in  OnSelectTaskType');
 	//console.log(this.valTaskForm.value)	;

  this.getSubtaskByStudyandDeliverable(this.valTaskForm.get('studyId').value
          , this.valTaskForm.get('deliverableId').value
          , selectedtaskType );
 }

private getSubtaskByStudyandDeliverable(selectdStudyId, selectedDeliverableId , selectedtaskType){
    this.showSpinner = 1;
    
    this.taskService.getParentIdByStudyNDeliverable(selectdStudyId
          , selectedDeliverableId
          , selectedtaskType )
  .subscribe((data) => {
    this.zone.run(() => {
        this.parentTaskList = data;
        //console.log(this.parentTaskList);      
      this.showSpinner = 0;
  });
  }, (error) => {
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
  });

}

private getUserListByStudyId(selectedStudy: any) {
 /// Gettig active users 
  this.appUserService.appUsersByStudyId(selectedStudy).subscribe((data) => {
  this.zone.run(() => {
      this.appUserList = data;
  });
  }, (error) => {
      // console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
  });  
 }

private getDeliverablesByStudy(selectedStudy){
     /// Gettig active deliverable for given stiudy
      this.deliverableService.getDeliverablesByStudyId(selectedStudy).subscribe((data) => {
      this.zone.run(() => {
          this.deliverableList = data;
          //console.log(this.deliverableService);
      });
      }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });  

 }


 private onTaskPriorityChange(selectedSourceFk: any) {
 }

 private onTaskStatusChange(selectedSourceFk: any) {
 }

 private onTaskAssigneeChange(selectedSourceFk: any) {
 }

 private addComment(task: Task , comment: string ) {
   var newComment = new TaskComment();
   newComment.comment = comment;
   newComment.createdBy = task.createdBy;
   this.taskCommentList.push(newComment);
 }


private getOneLevelSubtask(taskId){
     /// Gettig active deliverable for given stiudy
      this.taskService.getOneLevelsubTask(taskId).subscribe((data) => {
      this.zone.run(() => {
          this.levelOneSubTaskList = data;
          //console.log(this.levelOneSubTaskList);
      });
      }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });  

 }



 ngOnInit_Edit(taskId) {
   
    // Get all Source EDC Server Names
    this.taskService.getTaskProperties().subscribe((data) => {
    this.zone.run(() => {
        this.taskProperty = data;
        //this.taskTypeList = this.taskProperty.taskTypeList ;
        this.taskStatusList= this.taskProperty.taskStatusList ;
        this.taskPriorityList = this.taskProperty.taskPriorityList;
        
    });
    }, (error) => {
        // console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

    /// Gettig Active study assigned to user
    this.studyService.getStudyByUsers().subscribe((data) => {
    this.zone.run(() => {
        this.studyList = data;
    });
    }, (error) => {
        // console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });

       
    this.isButtonVisible = true;
    //console.log(' getting By Id...' + this.item.taskId);
    this.allParentTaskList = [] ; 
    // getting all parent of given task id for breadcrumb display   
    this.taskService.getAllParentTask(taskId).subscribe((data) => {        
      this.zone.run(() => {
      this.allParentTaskList = data;
      this.showSpinner = 0;
      //console.log(this.allParentTaskList);          
    });
    }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });

    // Get the details of the requested BridgeId
      this.taskService.getTaskById(taskId).subscribe((data) => {        
      this.zone.run(() => {
        this.task = data;
        this.taskCommentList =  this.task.comments;
        this.toUpdate = true;
        this.parentTaskType = this.task.type;
        this.toShowForm = true;
        this.valTaskForm.patchValue(this.task);          
        this.showSpinner = 0;
        this.getUserListByStudyId(this.task.studyId);        
        this.getDeliverablesByStudy(this.task.studyId);
        this.showDetailsDisableElement();        
    });
    }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });

          //create subtask Available Links
          this.createAvailableSubtaskList(taskId);
          this.editOrSubTask = true;
    }



}