import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Location } from '@angular/common';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { Http } from '@angular/http';
import { TaskService } from 'app/services/task.service';
import { AppUserService } from 'app/services/appUser.service';
import { StudyService } from 'app/services/study.service';
import { DeliverableService } from 'app/services/deliverable.service';
import { Task } from 'app/model/task';
import { AppUser } from 'app/model/appuser'  ; 
import { CommanConfig } from 'app/routes/CommanConfig';
import { TaskProperty } from 'app/model/taskProperty';
import { TaskType } from 'app/model/TaskType';
import { TaskComment } from 'app/model/taskComment';
import { FileUploader } from 'ng2-file-upload';
import { FileUploadsConfig } from 'app/routes/FileUploadsConfig' ; 
import { forEach } from '@angular/router/src/utils/collection';
import { Deliverable } from '../../../model/deliverable';


//const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';
// const URL = 'api/task/upload' ; 

@Component({
  selector: 'app-task-details-edit',
  templateUrl: './task-details-edit.component.html',
  styleUrls: ['./task-details-edit.component.scss']
})
export class TaskDetailsEditComponent extends CommanConfig implements OnInit {

/*  public uploader: FileUploader = new FileUploader({ url: URL 
    //, authTokenHeader: "Authorization"
    //, authToken:  this.getToken() 
   });
*/
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;
    
  public success_toaster: any;
  private showSpinner: number = 1;
  private totalRecords: number = 0;

  private valTaskForm: FormGroup;  
  private task: Task;
  private submitted: boolean = false;
  private toUpdate;
  private toSubTask;
  private showParent: boolean = true ;  
  private errorMessage;
  private item: any ;
  private studyList ;
  private deliverableList ;
  private allLinkedDeliverableList ;
  
  private appUserList ;
  private parentTaskList ;
  private parentTaskType = null  ;  	  
  private levelOneSubTaskList  ;      
  private taskProperty: TaskProperty ;
  private taskTypeList: Array<TaskType> ; 
  private taskStatusList: Array<String> ; 
  private taskPriorityList: Array<String> ;
  private linkedDeliverablesList ;

  private deliverableNames = [] ;
  private assignedDeliverableNames = [] ;
  
  private items: Array<String> ;  
  private assignedItem : Array<String> ;  
  private value: any = {};
  private linkedDeliverableList = [] ;

  private availableSubtaskTypeList: Array<TaskType> ;

  private buttonCreateDev: boolean =  false;
  private buttonCreateTest: boolean =  false;
  private buttonCreateBug: boolean =  false;
  private buttonCreateReview: boolean =  false;
  private isButtonVisible = true;
  private showCommnet = true;
  private isUploadButtonVisible = true;
  private docId: number;

  private createAvailableSubtaskButtonflag: boolean=false
  
  private taskCommentList : TaskComment[] = [];
  //private toShowForm :boolean =  false; 
  private toEdit :boolean =  false; 
  private allParentTaskList ;
  private document ;
  private editButtonFlag : boolean = true ;


  @ViewChild('cantCreateSubTaskModal')  cantCreateSubTaskModal: any;
  @ViewChild('cantCreateTaskModal')  cantCreateTaskModal: any;

  constructor(
  	private zone: NgZone, 
    private fb: FormBuilder, 
    private route: ActivatedRoute,
    private http: Http, 
    private taskService: TaskService,
    private router: Router, 
    public toasterService: ToasterService,
    private studyService: StudyService, 
    public deliverableService: DeliverableService,
    private appUserService: AppUserService ,
    private fileUpload: FileUploadsConfig
   ) {
     super(toasterService);
     //this.initTaskSearch(); 
     this.task = new Task();
     this.levelOneSubTaskList = [];

    this.valTaskForm = this.fb.group({
        'taskId': [this.task.taskId],
        'name': [this.task.name, Validators.required],
        'type': [this.task.type, Validators.required], // dev, QC
        'level': [this.task.level],
        'parentId': [this.task.parentId],
        'description': [this.task.description],
        'studyId': [this.task.studyId, Validators.required],
        'deliverableId': [this.task.deliverableId, Validators.required],
        'priority': [this.task.priority, Validators.required],
        'status': [this.task.status,Validators.required],
        'assigneeId': [this.task.assigneeId,Validators.required],
        'assigneeName': [this.task.assigneeName],
        'startDate': [this.task.startDate, Validators.required],
        'endDate': [this.task.endDate,Validators.required],
        'comment': [this.task.comment],
        'comments': [this.task.comments],
        'linkedDeliverables': [this.task.linkedDeliverables],
        'documents': [this.task.documents],
        'estimatedTime': [this.task.estimatedTime],
        'actualTime': [this.task.actualTime]
    });
 }

 public uploader = this.fileUpload.setup('/api/task/upload');

  ngOnInit() {
   
  this.isButtonVisible = false;
	// Get all Source EDC Server Names
	this.taskService.getTaskProperties().subscribe((data) => {
	this.zone.run(() => {
	    this.taskProperty = data;
	    this.taskTypeList = this.taskProperty.taskTypeList ;
		this.taskStatusList= this.taskProperty.taskStatusList ;
        this.taskPriorityList = this.taskProperty.taskPriorityList;
          
	});
	}, (error) => {
	    // console.log(error);
	    this.errorMessage = error;
	    this.showSpinner = 0;
	    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
	});

	this.studyService.getStudyByUsers().subscribe((data) => {
	this.zone.run(() => {
	    this.studyList = data;
	});
	}, (error) => {
	    // console.log(error);
	    this.errorMessage = error;
	    this.showSpinner = 0;
	    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

	this.showSpinner = 1;    
  this.item = this.route.snapshot.params;
  if (this.item.taskId) {     
        this.isButtonVisible = true;
      	this.allParentTaskList = [] ; 
      
      // getting all parent of given task id for breadcrumb display   
        this.taskService.getAllParentTask(this.item.taskId).subscribe((data) => {        
          this.zone.run(() => {
          this.allParentTaskList = data;
          this.showSpinner = 0;
        });

      }, (error) => {
            // console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });

       // check parameter has type define then it is subtask creation    
      
      if(this.item.type){  // if type is there then make prepare subtask
         this.toUpdate = false ;         
         this.taskService.getTaskById(this.item.taskId).subscribe((data) => {   
         this.showCommnet = false;      
          this.zone.run(() => {
          this.task = data;
          this.showSpinner = 0;
          this.getUserListByStudyId(this.task.studyId);
          this.getDeliverablesByStudy(this.task.studyId);

          this.valTaskForm.get('studyId').patchValue(this.task.studyId);
          this.valTaskForm.get('deliverableId').patchValue(this.task.deliverableId);
          this.valTaskForm.get('type').patchValue(this.item.type);
          this.valTaskForm.get('parentId').patchValue(this.item.taskId);
          this.valTaskForm.get('status').patchValue("NEW");
          this.task.linkedDeliverables = this.valTaskForm.get('linkedDeliverables').value;
          //just need to set taskType as per type button click and set task fotm into new form. 
             
            var taskList: Task[] = [];
            taskList.push(this.task);
            this.parentTaskList = taskList; 
            this.parentTaskType = this.task.type;
            

          this.valTaskForm.get('studyId').disable();
          this.valTaskForm.get('deliverableId').disable();
          this.valTaskForm.get('type').disable();
        //   this.valTaskForm.get('parentId').disable();
          this.valTaskForm.get('status').disable();


          /*this.valTaskForm.get('status').disable();
          this.valTaskForm.get('status').patchValue("NEW");
          this.task.startDate = this.getSringDate(this.task.startDate);
          this.task.endDate = this.getSringDate(this.task.endDate);
          this.taskCommentList =  this.task.comments;
          //this.toUpdate = true;
          this.parentTaskType = this.task.type;
          //this.toShowForm = true;
          this.task.linkedDeliverables = [];
          this.valTaskForm.patchValue(this.task);          
          this.showSpinner = 0;
          this.getUserListByStudyId(this.task.studyId);        
          this.getDeliverablesByStudy(this.task.studyId);
        
          //this.showDetailsDisableElement();          
          this.prepareSubTask(this.valTaskForm, this.item.type) ;*/
       
        });

      }, (error) => {
            // console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });

      }else {  // at edit of existing task.
          this.taskService.getTaskById(this.item.taskId).subscribe((data) => {   
            this.valTaskForm.get('status').enable();
            this.showParent = false ;
          this.showCommnet = true;    
          this.zone.run(() => {
          this.task = data;
          this.task.startDate = this.getSringDate(this.task.startDate);
          this.task.endDate = this.getSringDate(this.task.endDate);
          this.taskCommentList =  this.task.comments;
          this.toUpdate = true;                                                    
          //this.toShowForm = true;
          this.valTaskForm.patchValue(this.task);       
          this.showSpinner = 0;

          this.getUserListByStudyId(this.task.studyId);        
          this.getDeliverablesByStudy(this.task.studyId);

          //this.showDetailsDisableElement();
          
          // make assigned item to display
          this.assignedItem = [];
          this.assignedDeliverableNames = this.task.linkedDeliverables ;
          if(this.assignedDeliverableNames.length > 0 ) {
            this.assignedDeliverableNames.forEach( a => {
                  this.assignedItem.push(a.deliverableName); 
            });

        }

          //prepare for edit task   
          this.editTask(); ///added 
            this.uploader.addToQueue(this.task.documents);
            this.uploader.queue.push();
        });
      }, (error) => {
            // console.log(error);
            this.errorMessage = error;
            this.showSpinner = 0;
            this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
        });

      }
    
   // this.showParent = false ;
    //console.log(this.levelOneSubTaskList);

	}else { 
        this.toUpdate = false;
        this.showCommnet = false;
        this.valTaskForm.get('status').patchValue("NEW");
        this.valTaskForm.get('status').disable();
		this.showSpinner = 0;  
		}
	

  this.editButtonFlag = false ;
  this.levelOneSubTaskList = [] ; 
  this.createAvailableSubtaskButtonflag = false ;

  
  this.deliverableService.getAll().subscribe((data) => {
    this.zone.run(() => {

        this.allLinkedDeliverableList = data;   
        
        this.allLinkedDeliverableList.forEach(deliverable => {
            this.deliverableNames.push(deliverable.deliverableName);
        });
         this.items= this.deliverableNames;
        });
    }, (error) => {
        // console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    }); 

    //  this.getDocument(1);

  }


 submitForm($ev, value: any) {
    $ev.preventDefault(); 	
    this.valTaskForm.get('status').enable();
    
    if (this.valTaskForm.valid) { 
      this.submitted = true;
      var actualTime = this.valTaskForm.get('actualTime').value;
      if (this.toUpdate && ! this.toSubTask) {    	

        this.toEdit = false;
        this.editEnableElement();
          this.subTaskEnableElements();
          this.showSpinner = 1;
          this.valTaskForm.get('documents').patchValue(this.task.documents);

          //console.log("On time of submit  Update submit") ;
          
          this.valTaskForm.get('linkedDeliverables').patchValue(this.task.linkedDeliverables);          

          this.taskService.update(this.item.taskId, this.valTaskForm.value).subscribe(
          entry => this.handleSubmitSuccess(entry),
          error => this.handleSubmitError(error)
          );
          //console.log(this.valTaskForm.value);

       }        
      else { //In create 
        
        this.showSpinner = 1;
            this.subTaskEnableElements();
    		if(this.toSubTask)
    		{
    			this.subTaskEnableElements();
    			this.toSubTask = false;
                this.valTaskForm.get('documents').patchValue(this.task.documents);
                this.valTaskForm.get('linkedDeliverables').patchValue(this.task.linkedDeliverables);
    			this.taskService.create(this.valTaskForm.value)
              	.subscribe(
              	entry => this.handleSubmitSuccess(entry),
              	error => this.handleSubmitError(error)
              	);
                this.showSpinner = 0;
                console.log(this.valTaskForm.value);
    		}else {				
    			if(this.valTaskForm.get('type').value != 'DEV') {				
            
                if(this.valTaskForm.get('parentId').value == null ){
      					this.submitted = false; 
      					this.showSpinner = 0;
      					this.cantCreateTaskModal.show();
      					
      				}else {
                        this.valTaskForm.get('documents').patchValue(this.task.documents);
      					this.taskService.create(this.valTaskForm.value)
                			.subscribe(
                			entry => this.handleSubmitSuccess(entry),
      			        error => this.handleSubmitError(error)
              	  			);
                            this.showSpinner = 0;
                            //console.log(this.valTaskForm.value);
      				}

    			}else{  
                    this.valTaskForm.get('documents').patchValue(this.task.documents);
    					this.taskService.create(this.valTaskForm.value)
    	      			.subscribe(
    	      			entry => this.handleSubmitSuccess(entry),
    			        error => this.handleSubmitError(error)
    	    	  			);
                          this.showSpinner = 0;
                          
                  }

      		 }    		
            }    
   }else{
    this.valTaskForm.get('documents').patchValue(this.task.documents);
     this.toasterService.pop(this.required_validation_toaster.type, this.required_validation_toaster.title, this.required_validation_toaster.text);
    }
 }  

 private createAvailableSubtaskList(taskId){
   
   this.showSpinner = 1;       
   //need to get available  task type to create subtask , if level 2 tasktype is null and we can not create subtask.   
   this.taskService.getAvailableTaskPropertiesForTaskById(taskId).subscribe((data) => {
     this.zone.run(() => {
     this.taskProperty = data;        
     //this.taskTypeList = this.taskProperty.taskTypeList ;
     this.taskStatusList= this.taskProperty.taskStatusList ;
     this.taskPriorityList = this.taskProperty.taskPriorityList;
     this.availableSubtaskTypeList = this.taskProperty.taskTypeList;
      
     if(this.availableSubtaskTypeList.length > 0) {
       this.createAvailableSubtaskButtonflag = true;
     }

    });
  }, (error) => {
      // console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
  })
 }

 private prepareSubTask(valTaskForm : FormGroup , taskType: any ){
   
    this.createAvailableSubtaskButtonflag = false;
    this.editButtonFlag = false
    this.toUpdate = false ;   
    this.levelOneSubTaskList = [] ; 

    //just need to set taskType as per type button click and set task fotm into new form. 
    this.valTaskForm.get('parentId').patchValue(this.valTaskForm.get('taskId').value); 
    this.valTaskForm.get('type').patchValue(taskType); 
    
    var taskList: Task[] = [];
    taskList.push(this.task);
    this.parentTaskList = taskList; 
    this.valTaskForm.get('taskId').patchValue('');  
    
    //this.valTaskForm.get('level').patchValue(2);  
    this.valTaskForm.get('name').patchValue('') ;
    this.valTaskForm.get('description').patchValue('') ;
    this.valTaskForm.get('comment').patchValue('') ;
    
    this.valTaskForm.get('linkedDeliverables').patchValue([]) ;
    
    this.taskCommentList= [];  

    this.toSubTask = true;
       
    this.subTaskDisableElements();
 }

 private editTask(){
   this.editDisableElement();
   //this.toShowForm = false ; 
   this.toEdit= true;
   this.editButtonFlag = false;
   this.createAvailableSubtaskButtonflag = true;
//    this.isUploadButtonVisible = false; 
 }

 private backClicked() {
  	this.router.navigate(['/task/list']);
  }

 private taskByIdForEdit(taskId) {
   
    this.ngOnInit_Edit(taskId) ;    
    //console.log(this.createAvailableSubtaskButtonflag +' ' + this.editButtonFlag );     
    this.editButtonFlag = false ; 

    this.createAvailableSubtaskButtonflag = true ;     
    this.createAvailableSubtaskList(taskId);
    this.getOneLevelSubtask(taskId);

}


 protected handleSubmitSuccess(entry) {
    this.showSpinner = 0;
    this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);
    setTimeout(() => {
      this.backClicked();
    }, 1500);
  }
 
protected handlesave(entry) {
    this.showSpinner = 0;
    this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);
/*    setTimeout(() => {
      this.backClicked();
    }, 1500);*/
  }
 

 protected handleSubmitError(error: any) {
    this.showSpinner = 0;
    this.submitted = false;
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, error);
  }

 private subTaskEnableElements(){
	  this.valTaskForm.get('type').enable();
    this.valTaskForm.get('deliverableId').enable();
    this.valTaskForm.get('studyId').enable();
 }

 private subTaskDisableElements(){
	  this.valTaskForm.get('type').disable();
    this.valTaskForm.get('deliverableId').disable();
    this.valTaskForm.get('studyId').disable();

    //this.valTaskForm.get('type').enable();
    this.valTaskForm.get('name').enable();
	this.valTaskForm.get('taskId').enable();
    this.valTaskForm.get('description').enable();
    this.valTaskForm.get('priority').enable();    
	// this.valTaskForm.get('status').enable();
    this.valTaskForm.get('assigneeId').enable();
    this.valTaskForm.get('parentId').enable();


 }

 private editEnableElement(){

	this.valTaskForm.get('type').enable();
	this.valTaskForm.get('deliverableId').enable();
	this.valTaskForm.get('studyId').enable();    
	this.valTaskForm.get('taskId').enable();
	this.valTaskForm.get('name').enable();
    
 }


private editDisableElement(){
	this.valTaskForm.get('type').disable();
    this.valTaskForm.get('deliverableId').disable();
    this.valTaskForm.get('studyId').disable();    
    this.valTaskForm.get('taskId').disable();
    // this.valTaskForm.get('name').disable();

    this.valTaskForm.get('taskId').enable();
    this.valTaskForm.get('description').enable();
    this.valTaskForm.get('priority').enable();    
	this.valTaskForm.get('status').enable();
    this.valTaskForm.get('assigneeId').enable();

    
   }

 private showDetailsEnableElement(){
    this.valTaskForm.get('type').enable();
    this.valTaskForm.get('deliverableId').enable();
    this.valTaskForm.get('studyId').enable();        
    this.valTaskForm.get('taskId').enable();
    this.valTaskForm.get('name').enable();
    this.valTaskForm.get('description').enable();
    this.valTaskForm.get('priority').enable();    
	this.valTaskForm.get('status').enable();
    this.valTaskForm.get('assigneeId').enable();
 
 }

 private showDetailsDisableElement(){
	
	this.valTaskForm.get('type').disable();
    this.valTaskForm.get('deliverableId').disable();
    this.valTaskForm.get('studyId').disable();    
    this.valTaskForm.get('taskId').disable();
    this.valTaskForm.get('name').disable();
    this.valTaskForm.get('description').disable();
    this.valTaskForm.get('priority').disable();    
	this.valTaskForm.get('status').disable();
    this.valTaskForm.get('assigneeId').disable();
 
 }

 private onTaskTypeChange(selectedSourceFk: any) {
  /*  // console.log(selectedSourceFk);
    let tmp_selectedSourceFk = +selectedSourceFk;
    this.trialListFiltered = this.trialList.filter((x) => x.sourceFk === tmp_selectedSourceFk);
    // console.log(this.trialListFiltered);
  */
}

private onStudyChange(selectedStudy: any) { 
   this.getDeliverablesByStudy(selectedStudy); 
   this.getUserListByStudyId(selectedStudy);
   this.OnSelectStudy(selectedStudy);
}

private onDeliverableChange(selectedDeliverable : any) {
   this.OnSelectDeliverable(selectedDeliverable);
}


private OnSelectStudy(selectedStudyId: any) {
    if(this.valTaskForm.get('deliverableId').value 
        && this.valTaskForm.get('type').value ){
        
        this.getSubtaskByStudyandDeliverable(selectedStudyId
            , this.valTaskForm.get('deliverableId').value
            , this.valTaskForm.get('type').value );
    }
  } 

private OnSelectDeliverable(selectedDeliverableId: any) {
    if(this.valTaskForm.get('studyId').value 
        && this.valTaskForm.get('type').value ){

        this.getSubtaskByStudyandDeliverable(this.valTaskForm.get('studyId').value
            , selectedDeliverableId
            , this.valTaskForm.get('type').value );
    }
  } 

 
 private OnSelectTaskType(selectedtaskType: any) {

if(this.valTaskForm.get('studyId').value != null && this.valTaskForm.get('deliverableId').value != null){
  this.getSubtaskByStudyandDeliverable(this.valTaskForm.get('studyId').value
          , this.valTaskForm.get('deliverableId').value
          , selectedtaskType );
   }
 }

private getSubtaskByStudyandDeliverable(selectdStudyId, selectedDeliverableId , selectedtaskType){
    this.showSpinner = 1;
    
    this.taskService.getParentIdByStudyNDeliverable(selectdStudyId
          , selectedDeliverableId
          , selectedtaskType )
  .subscribe((data) => {
    this.zone.run(() => {
        this.parentTaskList = data;
        //console.log(this.parentTaskList);      
      this.showSpinner = 0;
  });
  }, (error) => {
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
  });

}

private getUserListByStudyId(selectedStudy: any) {
 /// Gettig active users 
  this.appUserService.appUsersByStudyId(selectedStudy).subscribe((data) => {
  this.zone.run(() => {
      this.appUserList = data;
  });
  }, (error) => {
      // console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
  });  
 }

private getDeliverablesByStudy(selectedStudy){
     /// Gettig active deliverable for given stiudy
      this.deliverableService.getDeliverablesByStudyId(selectedStudy).subscribe((data) => {
      this.zone.run(() => {
          this.deliverableList = data;
          //console.log(this.deliverableService);
      });
      }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });  

 }

 private onTaskPriorityChange(selectedSourceFk: any) {
 }

 private onTaskStatusChange(selectedSourceFk: any) {
 }

 private onTaskAssigneeChange(selectedSourceFk: any) {
 }

 private addComment(task: Task , comment: string ) {
   var newComment = new TaskComment();
   newComment.comment = comment;
   newComment.createdBy = task.createdBy;
   this.taskCommentList.push(newComment);
 }


private getOneLevelSubtask(taskId){
     /// Gettig active deliverable for given stiudy
      this.taskService.getOneLevelsubTask(taskId).subscribe((data) => {
      this.zone.run(() => {
          this.levelOneSubTaskList = data;
          //console.log(this.levelOneSubTaskList);
      });
      }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });  

 }



 ngOnInit_Edit(taskId) {
   
    //console.log('ngOnInit_Edit...')
    // Get all Source EDC Server Names
    this.taskService.getTaskProperties().subscribe((data) => {
    this.zone.run(() => {
        this.taskProperty = data;
        //this.taskTypeList = this.taskProperty.taskTypeList ;
        this.taskStatusList= this.taskProperty.taskStatusList ;
        this.taskPriorityList = this.taskProperty.taskPriorityList;
        
    });
    }, (error) => {
        // console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

    /// Gettig Active study assigned to user
    this.studyService.getStudyByUsers().subscribe((data) => {
    this.zone.run(() => {
        this.studyList = data;
    });
    }, (error) => {
        // console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });

       
    this.isButtonVisible = true;
    //console.log(' getting By Id...' + this.item.taskId);
    this.allParentTaskList = [] ; 
    // getting all parent of given task id for breadcrumb display   
    this.taskService.getAllParentTask(taskId).subscribe((data) => {        
      this.zone.run(() => {
      this.allParentTaskList = data;
      this.showSpinner = 0;
      //console.log(this.allParentTaskList);          
    });
    }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });

    // Get the details of the requested BridgeId
      this.taskService.getTaskById(taskId).subscribe((data) => {        
      this.zone.run(() => {
        this.task = data;
        this.taskCommentList =  this.task.comments;
        this.toUpdate = true;
        this.parentTaskType = this.task.type;
        //this.toShowForm = true;
        this.valTaskForm.patchValue(this.task);          
        this.showSpinner = 0;
        this.getUserListByStudyId(this.task.studyId);        
        this.getDeliverablesByStudy(this.task.studyId);

        //this.showDetailsDisableElement();
        this.editDisableElement();       
    });
    }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });
      
         //create subtask Available Links
         //this.createAvailableSubtaskList(taskId);
         this.showParent = false;
    }
    
  private getSringDate(date: any){
        date = new Date(date).toLocaleDateString().split("/");
        date[0] = ("0" + date[0]).slice(-2);
        date[1] = ("0" + date[1]).slice(-2);
        var sDate = date[2] + '-' + date[0] + '-' + date[1];
  return sDate;
    } 

     public fileOverBase(e: any): void {
        this.hasBaseDropZoneOver = e;
    }

    public fileOverAnother(e: any): void {
        this.hasAnotherDropZoneOver = e;
    }

   removeFile(item: any ){
      console.log(item); 
      item.remove();

    }

    downloadFile(data){
    console.log('start download:',data);
    var blob = new Blob([data.docContent]);
    var url = window.URL.createObjectURL(blob);
    var a = document.createElement('a');
    document.body.appendChild(a);
    a.setAttribute('style', 'display: none');
    a.href = url;
    a.download = data.name;
    a.click();
      
    }

    public getDocument(docId) {
        console.log(docId);
      this.taskService.getDocumentById(docId).subscribe((data) => {        
            this.zone.run(() => {
            this.document = data;
            console.log("inside get Doc");
            console.log(this.document.docId);
            console.log(this.document);
            
            
            this.downloadFile(this.document);

            this.showSpinner = 0;
          });

        }, (error) => {
              // console.log(error);
              this.errorMessage = error;
              this.showSpinner = 0;
              this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
          });

    }

    public selected(value: any): void {
        
        var tempDeliverable ;
        tempDeliverable =  Array.from(this.allLinkedDeliverableList).find( a => value.text == (<Deliverable> a).deliverableName );
        this.task.linkedDeliverables.push(tempDeliverable) ;  
        
        //console.log('after added ' + value )
        //console.log(tempDeliverable);
        //console.log(this.task.linkedDeliverables) ; 
    
    }

    public removed(value: any): void {        
         
        var tempList  =  Array.from(this.task.linkedDeliverables).filter( a => value.text != (<Deliverable> a).deliverableName ) ; 
        this.task.linkedDeliverables = tempList ; 
        console.log('after removal: ' + value);
        console.log(this.task.linkedDeliverables) ;         
    
    }
    
    
    public typed(value: any): void {
    }

    public refreshValue(value: any): void {
        this.value = value;
    }
 }
