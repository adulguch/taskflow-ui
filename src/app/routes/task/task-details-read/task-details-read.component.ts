import { Component, OnInit, NgZone, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Params,  Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Location } from '@angular/common';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { Http } from '@angular/http';
import { TaskService } from 'app/services/task.service';
import { AppUserService } from 'app/services/appUser.service';
import { StudyService } from 'app/services/study.service';
import { DeliverableService } from 'app/services/deliverable.service';
import { Task } from 'app/model/task';
import { AppUser } from 'app/model/appuser'  ; 
import { CommanConfig } from 'app/routes/CommanConfig';
import { TaskProperty } from 'app/model/taskProperty';
import { TaskType } from 'app/model/TaskType';
import { TaskComment } from 'app/model/taskComment';
import { saveAs } from 'file-saver';
import { Response } from '@angular/http';

import { empty } from 'rxjs/observable/empty';

@Component({
  selector: 'app-task-details-read',
  templateUrl: './task-details-read.component.html',
  styleUrls: ['./task-details-read.component.scss']
})
export class TaskDetailsReadComponent extends CommanConfig implements OnInit {
  fileUrl: string;
 
  public success_toaster: any;
  private showSpinner: number = 1;
  private totalRecords: number = 0;

  private valTaskForm: FormGroup;  
  private task: Task;
  private submitted: boolean = false;
  private toUpdate;
  private errorMessage;
  private item: any ;

  private parentTaskList ;
  private parentTaskType = null  ;  	  
  private levelOneSubTaskList  ;      
  private taskProperty: TaskProperty ;


  private taskStatusList: Array<String> ; 
  private taskPriorityList: Array<String> ;

  private availableSubtaskTypeList: Array<TaskType> ;
  private createAvailableSubtaskButtonflag: boolean=false
  
  private taskCommentList : TaskComment[] = [];

  private allParentTaskList ;
  private editButtonFlag : boolean = true ;

  private isButtonVisible = true;
  private linkedDeliverablesList;
  private selectedFiles: FileList;
  private fileNames: string[];

  @ViewChild('FileUpload') fileUpload: ElementRef;

  constructor(
  	private zone: NgZone, 
    private fb: FormBuilder, 
    private route: ActivatedRoute,
    private http: Http, 
	  private taskService: TaskService,
    private router: Router, 
  	public toasterService: ToasterService,
  	private studyService: StudyService, 
  	public deliverableService: DeliverableService,  		
  	private appUserService: AppUserService  

   ) {
     super(toasterService);
     //this.initTaskSearch(); 
     this.task = new Task();
     this.levelOneSubTaskList = [];

     this.valTaskForm = this.fb.group({
        'taskId': [this.task.taskId],
        'name': [this.task.name, Validators.required],             
	   	  'type': [this.task.type], // dev, QC        
  		  'level': [this.task.level],
		    'parentId': [this.task.parentId],		
		    'description': [this.task.description],				
        'studyId': [this.task.studyId, Validators.required],        
        'deliverableId': [this.task.deliverableId, Validators.required],
        'priority': [this.task.priority, Validators.required],                
        'status': [this.task.status],
        //'user': [this.task.user],
        'assigneeId': [this.task.assigneeId],        
        'assigneeName': [this.task.assigneeName],
        'startDate': [this.task.startDate],
        'endDate': [this.task.endDate],        
        'comment': [this.task.comment,],
        'comments': [this.task.comments],
        'estimatedTime': [this.task.estimatedTime],
        'actualTime': [this.task.actualTime]
    });
 }

  ngOnInit() {

    this.isButtonVisible = false;
	  this.showSpinner = 1;    
    this.item = this.route.snapshot.params;
    
    this.route.params
      .subscribe( (params : Params) =>  {
        //this.item.taskId = params['taskId'] 
        this.item = params;
    
      })

    this.isButtonVisible = true;
  	this.allParentTaskList = [] ; 

	  // getting all parent of given task id for breadcrumb display   
    this.taskService.getAllParentTask(this.item.id).subscribe((data) => {        
      this.zone.run(() => {
      this.allParentTaskList = data;
      this.showSpinner = 0;
    });

  	}, (error) => {
        // console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });


   this.taskService.getTaskById(this.item.id).subscribe((data) => {        
      this.zone.run(() => {
      this.task = data;
      this.taskCommentList =  this.task.comments;
      this.linkedDeliverablesList = this.task.linkedDeliverables;
      this.fileNames = this.task.fileNames;
      this.toUpdate = true;
      this.parentTaskType = this.task.type;
  	  this.valTaskForm.patchValue(this.task);          
	    this.showSpinner = 0;

    });

	}, (error) => {
        // console.log(error);
        this.errorMessage = error;
        this.showSpinner = 0;
        this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

  // getting to generate button for subtask 
  this.createAvailableSubtaskList(this.item.id);

	// getting subtask of current task to display 
	this.getOneLevelSubtask(this.item.id);

	//} 

	}

 submitForm($ev, value: any) {
    $ev.preventDefault(); 	
    if (this.valTaskForm.get('comment').value != null ){
    // add comments to exiting comment list
     this.taskService.update(this.item.id, this.valTaskForm.value).subscribe(
		entry => this.handlesave(entry),
		error => this.handleSubmitError(error)
    );
    this.addComment(this.task , this.valTaskForm.get('comment').value);
    this.valTaskForm.get('comment').reset();
     console.log("In end", this.valTaskForm.get('comment').value);
    //this.showDetailsDisableElement()
     }else{
      console.log("In end esle");
     }
 }  

 private createAvailableSubtaskList(taskId){
   
   this.showSpinner = 1;       
   //need to get available  task type to create subtask , if level 2 tasktype is null and we can not create subtask.   
   this.taskService.getAvailableTaskPropertiesForTaskById(taskId).subscribe((data) => {
     this.zone.run(() => {
     this.taskProperty = data;        
     //this.taskTypeList = this.taskProperty.taskTypeList ;
     this.taskStatusList= this.taskProperty.taskStatusList ;
     this.taskPriorityList = this.taskProperty.taskPriorityList;
     this.availableSubtaskTypeList = this.taskProperty.taskTypeList;
      
     if(this.availableSubtaskTypeList.length > 0) {
       this.createAvailableSubtaskButtonflag = true;
     }

    });
  }, (error) => {
      // console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
  })
 }

 private createSubTask(valTaskForm : FormGroup , taskType: any ){
   
   	const id = { 'taskId': this.task.taskId,
   				 'type' : taskType	
   				}
   	this.router.navigate(['/task/details-edit',id], { queryParams: id, skipLocationChange: true});
 }

 private editTask(){
	const id = { 'taskId': this.task.taskId }      
   	this.router.navigate(['/task/details-edit',id]);
 }

 private backClicked() {
  //const id = { 'taskId': this.task.taskId }      
  //this.router.navigate(['/task/details-read',id], { queryParams: id, skipLocationChange: true});
  this.router.navigate(['/task/list']);
  }

private taskById(taskId) {
    const id = { 'taskId': taskId }

    this.router.navigate(['/task/details-read/' + taskId]);

    this.ngOnInit_Edit(taskId) ;
    this.createAvailableSubtaskButtonflag = true ; 
    //console.log(this.createAvailableSubtaskButtonflag +' ' + this.editButtonFlag ); 
    
    this.editButtonFlag = true ; 
    //this.createAvailableSubtaskList(taskId);

    this.getOneLevelSubtask(taskId); 
}


 protected handleSubmitSuccess(entry) {
    this.showSpinner = 0;
    this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);
    setTimeout(() => {
      this.backClicked();
    }, 1500);
  }
 

 private addComment(task: Task , comment: string ) {
   var newComment = new TaskComment();
   newComment.comment = comment;
   newComment.comment.trim().length;
   console.log(newComment.comment.length);
   newComment.createdBy = task.modifiedBy;
   console.log(newComment.comment.trim().length);
   this.taskCommentList.push(newComment);
 }
 

private getOneLevelSubtask(taskId){
     /// Gettig active deliverable for given stiudy
      this.taskService.getOneLevelsubTask(taskId).subscribe((data) => {
      this.zone.run(() => {
          this.levelOneSubTaskList = data;
          //console.log(this.levelOneSubTaskList);
      });
      }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });  

 }

 ngOnInit_Edit(taskId) {
       
    this.isButtonVisible = true;
    //console.log(' getting By Id...' + this.item.taskId);

    // Get the details of the requested BridgeId
      this.taskService.getTaskById(taskId).subscribe((data) => {        
      this.zone.run(() => {
        this.task = data;
        this.taskCommentList =  this.task.comments;
        this.linkedDeliverablesList = this.task.linkedDeliverables;
        this.fileNames = this.task.fileNames;
        this.toUpdate = true;
        this.parentTaskType = this.task.type;
        this.valTaskForm.patchValue(this.task);          
        this.showSpinner = 0;
    });
    }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });
     
      // get all parent for breadcrumbs links
     this.getAllParent(taskId)
     //create subtask Available Links
     this.createAvailableSubtaskList(taskId);
    }

private getAllParent(taskId) {
    this.allParentTaskList = [] ; 
    // getting all parent of given task id for breadcrumb display   
    this.taskService.getAllParentTask(taskId).subscribe((data) => {        
      this.zone.run(() => {
      this.allParentTaskList = data;
      this.showSpinner = 0;
      //console.log(this.allParentTaskList);          
    });
    }, (error) => {
          // console.log(error);
          this.errorMessage = error;
          this.showSpinner = 0;
          this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
      });
  }

protected handlesave(entry) {
    this.showSpinner = 0;
    this.toasterService.pop(this.success_toaster.type,this.success_toaster.title,this.success_toaster.text);
/*    setTimeout(() => {
      this.backClicked();
    }, 1500);*/
  }
 

 protected handleSubmitError(error: any) {
    this.showSpinner = 0;
    this.submitted = false;
    this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, error);
  }

  public selectFile(event){
    this.selectedFiles = event.target.files;
  }

  public upload(){
    this.taskService.pushFilesToStorage(this.selectedFiles, this.task.taskId).subscribe(
      entry => {
        this.fileUpload.nativeElement.value='';
        this.fileNames = entry;
        this.selectedFiles = null;
      },
      error => this.handleSubmitError(error)
      );
  }

  public download(fileName){
    let tempFileName = fileName.replace('.', '+');
    this.taskService.downloadFile(this.task.taskId, tempFileName).subscribe(
      data => {
        const blob = new Blob([data], { type: 'application/octet-stream' });
        saveAs(blob, fileName);
      },
      error => this.handleSubmitError(error)
    );
  }

  public deleteFile(fileName){
    let tempFileName = fileName.replace('.', '+');
    this.taskService.deleteFile(this.task.taskId, tempFileName).subscribe(
      entry => this.fileNames = entry,
      error => this.handleSubmitError(error)
      );
  }
}