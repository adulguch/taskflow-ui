import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Observable';
import { Injectable, NgZone } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';


@Injectable()
export class MenuService {

    submenu : {text: string,heading?: boolean
                    ,link?: string
                    ,elink?: string
                    ,target?: string
                    ,icon?: string
                    ,alert?: string
                    ,submenu?: Array<any>} [];

    menuItems: { text: string,heading?: boolean
                    ,link?: string
                    ,elink?: string
                    ,target?: string
                    ,icon?: string
                    ,alert?: string
                    ,submenu?: Array<any>} [];    
    
    menu: { text: string,heading?: boolean
                    ,link?: string
                    ,elink?: string
                    ,target?: string
                    ,icon?: string
                    ,alert?: string
                    ,submenu?: Array<any>} [];   
   /* = 
    [{
    
    "text": "1Reports",
    "link": "/reports",
    "icon": "fa fa-bar-chart"
    },
    {
    
    "text": "2Reports",
    "link": "/reports",
    "icon": "fa fa-bar-chart"
    }] ;
    
    */

    constructor(public zone: NgZone
               ,private http: AuthHttp
               ) {
        this.menuItems = [];
    }

    addMenu(items: Array<{
        text: string,
        heading?: boolean,
        link?: string,     // internal route links
        elink?: string,    // used only for external links
        target?: string,   // anchor target="_blank|_self|_parent|_top|framename"
        icon?: string,
        alert?: string,
        submenu?: Array<{
            text: string,
            heading?: boolean,
            link?: string,     // internal route links
            elink?: string,    // used only for external links
            target?: string,   // anchor target="_blank|_self|_parent|_top|framename"
            icon?: string,
            alert?: string,
            submenu?: Array<any>
            }>
    }>) {

        if (this.menuItems.length > 0  ){
                this.menuItems = [];
        }

        items.forEach((item) => {
            //console.log("MenuItem : " + item.text.valueOf());    
            if(item.submenu.length === 0){
                item.submenu = null;
            }else {
               item.submenu.forEach((item)=>{ item.submenu = null ; });               
            }
            this.menuItems.push(item);
        });
    }

    getMenu() {
        return this.menuItems;
    }

    constructmenu(): any {
      //return this.http.get('assets/server/menu1.json')
      return this.http.get('api/menu' )
        .map(this.extractData)
        //.catch(this.handleError)
    }

    private extractData(res: Response) {
    const body = res.json();    
    return body.data  || {};
  }

  private handleError(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    const body = error.json() || '';
    if (body.message) {
      errMsg = body.message ? body.message : error.toString();
    } else {
      errMsg = error.status + ': Error occur on server.';
    }
    errMsg = error.status + ': ' +error.statusText+ '. '+error._body+'.';    
    return Observable.throw(errMsg);
  }





}
