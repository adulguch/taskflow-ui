import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { User } from '../model/user';
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectData } from 'app/model/selected-data';
import { UserStat } from 'app/model/user-stat';


@Injectable()
export class UserService extends handleErrorBaseServices{

private restUrl = 'api/users';  // URL to web API

  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router 
    ) {
   super(authenticationService , router );
  }

  create(formData): Observable<User> {
    if(this.checkAuth()){
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.post(this.restUrl, formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  updateProfile(userId, formData): Observable<User> {
    if(this.checkAuth()){ 
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });
      return this.http.put(this.restUrl + '/' + userId + '/' + 'profile', formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  updatePassword(userId, formData): Observable<User> {
    if(this.checkAuth()){
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });
      return this.http.put(this.restUrl + '/' + userId + '/' + 'password', formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  updateRole(userId, formData): Observable<User> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.http.put(this.restUrl + '/' + userId + '/' + 'role', formData, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  deleteUser(userId, user : User): Observable<User> {
    if(this.checkAuth()){   
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers});
        console.log(this.restUrl + '/' + userId + '/' + 'softDelete');
        return this.http.put(this.restUrl + '/' + userId + '/' + 'softDelete', user, options)
          .map(this.extractData)
          .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  updateActive(userName, active): Observable<User> {
    if(this.checkAuth()){ 
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.put(this.restUrl + '/active/' + userName + '/' + active, options)
      .map(this.extractData)
      .catch(this.handleError);
    }else {
      return this.http.get('').map((response: Response) => response.json().data);
   }
  }

  getUserByName(userName): Observable<User> {
    return this.http.get(this.restUrl + '/' + userName).map(this.extractData)
      .catch(this.handleError);
  }

  getUserById(userId): Observable<User> {
    return this.http.get(this.restUrl + '/' + userId).map(this.extractData)
      .catch(this.handleError);
  }
  
  getRolesByUserId(userId): Observable<SelectData> {
    return this.http.get(this.restUrl + '/filterRoles/' + userId).map(this.extractData)
      .catch(this.handleError);
  }

  getAll(): Observable<User> {    
    if(this.checkAuth()){ 
      return this.http.get(this.restUrl).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  getUserStat(): Observable<Array<UserStat>> {    
    if(this.checkAuth()){ 
      return this.http.get(this.restUrl + '/stat').map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }
}
