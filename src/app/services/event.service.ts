import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import { Event } from 'app/model/event';
import { handleErrorBaseServices } from 'app/services/handleErrorBaseServices.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { SelectData } from 'app/model/selected-data';

@Injectable()
export class EventService extends handleErrorBaseServices{

  private restUrl = 'api';  // URL to web API
  private events;
  constructor(private http : AuthHttp, 
              public authenticationService: AuthenticationService, 
              public router : Router ) {
    super(authenticationService, router);
   }

  getAllEvents() : Observable<Event[]>{
    if(this.checkAuth()){           
      return this.http.get(this.restUrl + '/events').map(this.extractData)
        .catch(this.handleError);
      
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
    } 
  }

  getEventsByUser() : Observable<Event[]>{
    if(this.checkAuth()){
      return this.http.get(this.restUrl + '/userEvents').map(this.extractData)
          .catch(this.handleError);
    }else{
      return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  getFilteredEvents() : Observable<SelectData>{
    if(this.checkAuth()){
      return this.http.get(this.restUrl + '/filterEvents').map(this.extractData)
          .catch(this.handleError);
    }else{
      return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  updateEvents(eventList : Event[]){
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    console.log("")
    return this.http.put(this.restUrl + '/events', eventList, options)
      .map(this.extractData)
      .catch(this.handleError);
  }


}
