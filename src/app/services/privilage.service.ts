import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Userprivileges } from '../model/userprivileges';
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';


@Injectable()
export class PrivilageService extends handleErrorBaseServices {

  private restUrl = 'api/checkWriteAccess';  // URL to web API
  private userModule : String = 'USER' ; 
  private RoleModule : String = 'ROLE' ;
  private BridgeModule : String = 'BRIDGE' ;
  private BridgeSchedulerModule : String = 'BRIDGE_SCHEDULER' ;
  private edcSourceServerModule : String = 'EDC_SOURCE_SERVER' ;
  private trialNameModule : String = 'TRIAL_NAME' ;
  private dbTargetServerModule : String = 'DB_TARGET_SERVER' ;
  private dbNameModule : String = 'DB_NAME' ;
  private kriModule : String = 'KRI' ;
  private groupModule : String = 'GROUP' ;
  private StudyModule: String = 'STUDY' ;
  private DeliverableModule: String = 'DELIVERABLE' ;
  private TaskModule: String = 'TASK' ;
  private IssueLogModule: String = 'ISSUE' ;
  
  
  //private bridgeStatusModule : String = 'BRIDGE_INFORM_STATUS' ;
  //private dataViewerModule : String = 'DATA_VIEWER' ;
  

  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router 
    ) {
   super(authenticationService , router );
  }


    CheckUserAccess(): Observable<Userprivileges> {    
    return this.http.get(this.restUrl + '/' + this.userModule ).map(this.extractData)
      .catch(this.handleError);
  }

   CheckRoleAccess(): Observable<Userprivileges> {    
    return this.http.get(this.restUrl + '/' + this.RoleModule ).map(this.extractData)
      .catch(this.handleError);
  }

   CheckBridgeAccess(): Observable<Userprivileges> {    
    return this.http.get(this.restUrl + '/' + this.BridgeModule ).map(this.extractData)
      .catch(this.handleError);
  }

   CheckEdcSourceAccess(): Observable<Userprivileges> {    
    return this.http.get(this.restUrl + '/' + this.edcSourceServerModule ).map(this.extractData)
      .catch(this.handleError);
  }

 CheckTrialNameAccess(): Observable<Userprivileges> {    
    return this.http.get(this.restUrl + '/' + this.trialNameModule ).map(this.extractData)
      .catch(this.handleError);
  }


  CheckDbTargetAccess(): Observable<Userprivileges> {    
    return this.http.get(this.restUrl + '/' + this.dbTargetServerModule ).map(this.extractData)
      .catch(this.handleError);
  }


  CheckDbNameAccess(): Observable<Userprivileges> {    
    return this.http.get(this.restUrl + '/' + this.dbNameModule ).map(this.extractData)
      .catch(this.handleError);
  }

  CheckKRIAccess(): Observable<Userprivileges> {    
    return this.http.get(this.restUrl + '/' + this.kriModule ).map(this.extractData)
      .catch(this.handleError);
  }

  CheckBridgeSchedulerAccess(): Observable<Userprivileges> {    
    return this.http.get(this.restUrl + '/' + this.BridgeSchedulerModule ).map(this.extractData)
  }
  
  CheckGroupAccess(): Observable<Userprivileges> {    
    return this.http.get(this.restUrl + '/' + this.groupModule ).map(this.extractData)
      .catch(this.handleError);
  }

  CheckStudyAccess(): Observable<Userprivileges> {    
    return this.http.get(this.restUrl + '/' + this.StudyModule ).map(this.extractData)
      .catch(this.handleError);
  }

  CheckDeliverableAccess(): Observable<Userprivileges> {    
    return this.http.get(this.restUrl + '/' + this.DeliverableModule ).map(this.extractData)
      .catch(this.handleError);
  }

  CheckTaskAccess(): Observable<Userprivileges> {    
    return this.http.get(this.restUrl + '/' + this.TaskModule ).map(this.extractData)
      .catch(this.handleError);
  }

  CheckIssueAccess(): Observable<Userprivileges> {    
    return this.http.get(this.restUrl + '/' + this.IssueLogModule ).map(this.extractData)
      .catch(this.handleError);
  }


}
