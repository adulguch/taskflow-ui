import { TestBed, inject } from '@angular/core/testing';

import { BridgeSchedulerService } from './bridge-scheduler.service';

describe('BridgeSchedulerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BridgeSchedulerService]
    });
  });

  it('should ...', inject([BridgeSchedulerService], (service: BridgeSchedulerService) => {
    expect(service).toBeTruthy();
  }));
});
