import { Observable } from 'rxjs/Rx';
import { Response, Headers, RequestOptions } from '@angular/http';
import { AuthenticationService } from 'app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';


export class handleErrorBaseServices {

  private errorLength: number = 0  ;
  constructor(
    public authenticationService: AuthenticationService 
    , public router : Router ) {
   }

  public extractData(res: Response) {
    const token = res.headers.get('Authorization')
    localStorage.setItem('currentUser', JSON.stringify({ token: token }));
    localStorage.setItem('token' , token )
    const body = res.json();
    //console.log('in extractData ') ; 

    return body.data || {};
  }

  public checkAuth() { 
    if(this.authenticationService.loggedIn()) {
      return true;
      } else {        
         this.router.navigateByUrl('/login');
          return false;
       }
    }

  public handleError(error: Response | any) {
     // In a real world app, you might use a remote logging infrastructure
     let errMsg: string;
     const body = error.json() || '';
     if (body.message) {
       
       errMsg = body.message ? body.message : error.toString();
       console.log('body.message ' +body.message );
       if(errMsg == 'Validation failed'){
           this.errorLength = body.fieldErrors.length ; 
           body.fieldErrors.forEach( (p, index) => {
             errMsg += '<br/> ' +( this.errorLength==1 ? '' : (index+1)  + ')' )  +  p.message;
            });
       }
     } else {
       errMsg = error.status + 'Error occur on server. Please contact support team.';
     }
     //errMsg = error.status + ': ' +error.statusText+ '. '+error._body+'.'; 
     return Observable.throw(errMsg.toString());
   }


  public handleError_newWithSupportMSG(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    const body = error.json() || '';
    if (body.message) {      
      errMsg = body.message ? body.message : error.toString();
      console.log('body.message ' +body.message );
      if(errMsg == 'Validation failed'){
          this.errorLength = body.fieldErrors.length ; 
          body.fieldErrors.forEach( (p, index) => {
            errMsg += '<br/> ' +( this.errorLength==1 ? '' : (index+1)  + ')' )  +  p.message;
           });
      } else {
      errMsg =  'Error status : ' + error.status  +  '<br/>' + ' Error occur on server. Please contact support team. ' ;
      }
    }
    //errMsg = error.status + ': ' +error.statusText+ '. '+error._body+'.'; 
    return Observable.throw(errMsg.toString());
  }


}
