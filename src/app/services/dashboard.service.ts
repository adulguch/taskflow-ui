import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { BridgeDashboard } from '../model/BridgeDashboard';
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { TaskDashboard } from '../model/task-dashboard';
import { Task } from '../model/task';


//import { GDashboardCntByType } from 'app/model/g-dashboardCntByType';
//import { GDashboardCntByUser } from 'app/model/g-dashboardCntByUser';

import { DashboardCntByUser } from 'app/model/dashboardCntByUser';
import { DashboardCntByUserNType } from 'app/model/dashboardCntByUserNType';
import { DTaskCntByType } from 'app/model/d-TaskCntByType';
import { DTaskCntByStatus } from 'app/model/d-TaskCntByStatus';


@Injectable()
export class DashboardService extends handleErrorBaseServices {

  private restUrl = 'api/dashboard';  // URL to web API
  

  constructor(private http: AuthHttp) {
  super(null ,null );
  }

  getDashboard(): Observable<TaskDashboard> {
    return this.http.get(this.restUrl).map(this.extractData)
      .catch(this.handleError);
  }

  getTaskCntByType(studyId , delvId): Observable<DTaskCntByType> {
    return this.http.get(this.restUrl + '/taskCountByType' + '/'+ studyId + '/' + delvId ).map(this.extractData)
      .catch(this.handleError);
  }

  getTaskCntByStatus(studyId , delvId): Observable<DTaskCntByStatus> {
    return this.http.get(this.restUrl + '/taskCountByStatus' + '/'+ studyId + '/' + delvId ).map(this.extractData)
      .catch(this.handleError);
  }

  getAllParentTask(): Observable<Task> {
    return this.http.get(this.restUrl + '/allParentTask' ).map(this.extractData)
      .catch(this.handleError);
  }

  getAllChildTask(taskId)  {
    return this.http.get(this.restUrl + '/allChildTask' + '/' +  taskId ).map(this.extractData)
      .catch(this.handleError);
  }

}
