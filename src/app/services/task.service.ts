import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import { Study } from '../model/study';
import { Task } from '../model/task';
import { TaskProperty } from '../model/taskProperty';

import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectData } from 'app/model/selected-data';




@Injectable()
export class TaskService extends handleErrorBaseServices{

private restTaskPropertyUrl = 'api/task/property';  // URL to web API
private restUrl = 'api/task';
private restParentTaskUrl = 'api/task/parenttask';
private restAllParentTaskUrl = 'api/task/allparentTask';
private restOneLevelSubTaskUrl = 'api/task/levelOneSubTask';
private restDownloadDocumentUrl = 'api/task/download';


constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router 
    ) {
   super(authenticationService , router );
}

  create(formData): Observable<Task> {
    if(this.checkAuth()){
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.post(this.restUrl, formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

update(id, formData): Observable<Task> {
    if(this.checkAuth()){   
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.put(this.restUrl + '/' + id, formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }  
}

  deleteStudy(userId, user : Study): Observable<Study> {
    if(this.checkAuth()){   
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers});
        console.log(this.restUrl + '/' + userId + '/' + 'softDelete');
        return this.http.put(this.restUrl + '/' + userId + '/' + 'softDelete', user, options)
          .map(this.extractData)
          .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }


  getStudyById(studyId): Observable<Study> {
    return this.http.get(this.restUrl + '/' + studyId).map(this.extractData)
      .catch(this.handleError);
  }
  
  getTaskProperties(): Observable<TaskProperty> {    
    if(this.checkAuth()){ 
      return this.http.get(this.restTaskPropertyUrl ).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  getAvailableTaskPropertiesForTaskById(taskId): Observable<TaskProperty> {    
    if(this.checkAuth()){ 
      return this.http.get(this.restTaskPropertyUrl + '/'  + taskId).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }


   getAll(): Observable<Task> {    
    if(this.checkAuth()){ 
      return this.http.get(this.restUrl + '/user/studiesTask').map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  getParentIdByStudyNDeliverable(studyId , deliverableId, taskType): Observable<Task> {
    return this.http.get(this.restParentTaskUrl + '/' + studyId + '/' + deliverableId + '/' + taskType ).map(this.extractData)
      .catch(this.handleError);
  }

  getTaskById(taskId): Observable<Task> {
    return this.http.get(this.restUrl + '/' + taskId).map(this.extractData)
      .catch(this.handleError);
  }


   getAllParentTask(taskId): Observable<Task> {    
    if(this.checkAuth()){ 
      return this.http.get(this.restAllParentTaskUrl + '/' +  taskId ).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }
  getOneLevelsubTask(taskId): Observable<Task> {    
    if(this.checkAuth()){ 
      return this.http.get(this.restOneLevelSubTaskUrl + '/' +  taskId ).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  getFilteredTasks(taskSearch): Observable<Task> {    
    if(this.checkAuth()){ 
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.post(this.restUrl + '/filtered', taskSearch, options).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  getTasksByAssignee(): Observable<Task> {    
    if(this.checkAuth()){ 
      return this.http.get(this.restUrl + '/Assignee').map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  getDocumentById(docId) : Observable<Task>   {   
    if(this.checkAuth()){ 
      return this.http.get(this.restDownloadDocumentUrl +  '/' + docId).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  pushFilesToStorage(files: FileList, taskId): Observable<any> {
    console.log(taskId);
    const headers = new Headers();
    //headers.append('Content-Type', 'multipart/form-data');
    //headers.append('Accept', 'application/json');
    const options = new RequestOptions({ headers: headers });
    const formdata: FormData = new FormData();
    for(let i = 0; i < files.length; i++)
      formdata.append('file', files.item(i), files.item(i).name);
    console.log(formdata);
    //return null;
    return this.http.post(this.restUrl + '/upload/' + taskId, formdata, options)
          .map(this.extractData)
          .catch(this.handleError);
        
  }

  downloadFile(id, fileName): Observable<Blob> {
    if(this.checkAuth()){
      let options = new RequestOptions( {responseType: ResponseContentType.Blob });
      return this.http.get(this.restUrl + '/download/' + id + '/' + fileName, options)
          .map(res => res.blob())
          .catch(this.handleError)
    }else {
      return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  deleteFile(id, fileName){
    if(this.checkAuth()){ 
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.post(this.restUrl +  '/delete/' + id + '/' + fileName, options)
        .map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  getFileList(taskId){
    if(this.checkAuth()){ 
      return this.http.get(this.restUrl + '/' + taskId + '/attachment').map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

/* getDocumentById(taskId): Observable<Task> {    
    console.log("in get doc ")    
    
    if(this.checkAuth()){ 
    console.log("in auth..... " + taskId);
      return this.http.get(this.restAllParentTaskUrl + '/' +  taskId ).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }
*/  
}
