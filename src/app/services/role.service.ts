import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { Role } from '../model/role';
import { Permission } from '../model/permission';
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectData } from 'app/model/selected-data';


@Injectable()
export class RoleService extends handleErrorBaseServices {

private restUrl = 'api/roles';  // URL to web API

 constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router 
    ) {
   super(authenticationService , router );
  }

  create(formData): Observable<Role> {
    if(this.checkAuth()){           
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.restUrl, formData, options)
          .map(this.extractData)
          .catch(this.handleError);
     }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  update(roleName, formData): Observable<Role> {
    if(this.checkAuth()){ 
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.put(this.restUrl + '/' + roleName, formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  delete(roleName): Observable<any> {
    if(this.checkAuth()){           
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });
      return this.http.delete(this.restUrl + '/' + roleName, options)
        .map(res => res)
        .catch(this.handleError);

    }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }

  }

  getRoleByName(roleName): Observable<Role> {
    return this.http.get(this.restUrl + '/' + roleName).map(this.extractData)
      .catch(this.handleError);
  }


  getAll(): Observable<Role> {    
    if(this.checkAuth()){           
        return this.http.get(this.restUrl).map(this.extractData)
          .catch(this.handleError);
      }else {
          return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  getAllPermission(): Observable<Permission> {    
    return this.http.get('api/permissions').map(this.extractData)
      .catch(this.handleError);
  }

  getFilteredPermissionByRoleName(roleName): Observable<SelectData> {
    return this.http.get(this.restUrl + '/filterPermissions/' + roleName).map(this.extractData)
      .catch(this.handleError);
  }

}
