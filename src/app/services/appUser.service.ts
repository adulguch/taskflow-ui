import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { AppUser } from '../model/appuser' ;
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectData } from 'app/model/selected-data';


@Injectable()
export class AppUserService extends handleErrorBaseServices{

private restUrl = 'api/appuser';  // URL to web API

  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router 
    ) {
   super(authenticationService , router );
  }

  create(formData): Observable<AppUser> {
    if(this.checkAuth()){
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.post(this.restUrl, formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

update(id, formData): Observable<AppUser> {
    if(this.checkAuth()){   
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.put(this.restUrl + '/' + id, formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }  
}

  deleteDeliverable(userId, user : AppUser): Observable<AppUser> {
    if(this.checkAuth()){   
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers});
        console.log(this.restUrl + '/' + userId + '/' + 'softDelete');
        return this.http.put(this.restUrl + '/' + userId + '/' + 'softDelete', user, options)
          .map(this.extractData)
          .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  getDeliverableById(DeliverableId): Observable<AppUser> {
    return this.http.get(this.restUrl + '/' + DeliverableId).map(this.extractData)
      .catch(this.handleError);
  }
  
  getAll(): Observable<AppUser> {    
    if(this.checkAuth()){ 
      return this.http.get(this.restUrl+ '/all').map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  appUsersByStudyId(studyId: any): Observable<AppUser> {    
    if(this.checkAuth()){ 
      return this.http.get(this.restUrl+ '/allbystudy' + '/' + studyId).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }
 


  getFilteredUsersByStudyId(studyId): Observable<SelectData> {
    if(this.checkAuth()){
      return this.http.get(this.restUrl + '/filteredUsers/' + studyId).map(this.extractData)
        .catch(this.handleError);
    }else {
      return this.http.get('').map((response: Response) => response.json().data);
   }
  }

  getFilteredUsersByGroupId(groupId): Observable<SelectData> {
    if(this.checkAuth()){
      return this.http.get(this.restUrl + '/groupUsers/' + groupId).map(this.extractData)
        .catch(this.handleError);
    }else {
      return this.http.get('').map((response: Response) => response.json().data);
   }
  }

  getUserDetailsById(userId): Observable<AppUser> {
    return this.http.get(this.restUrl + '/' + userId).map(this.extractData)
      .catch(this.handleError);
  }


}
