import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Actionaudit } from '../model/actionaudit';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { PaginationMetadata } from 'app/model/paginationmetadata';


@Injectable()
export class ActionauditService extends handleErrorBaseServices {

private restUrl = 'api/audit';  // URL to web API

  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router
  ) {
    super(authenticationService , router );
   }

  getAll(limit, start, greater): Observable<PaginationMetadata> { 
    if(this.checkAuth()){
      let url = '?limit=' + limit + '&start=' + start + '&greater=' + greater;
      return this.http.get(this.restUrl + url).map(this.extractData)
      .catch(this.handleError);
      }else {
      return this.http.get('').map((response: Response) => response.json().data);
    }  
  }


  public extractData(res: Response) {
    const body = res.json(); 
    return body.data || {};
  }

  public handleError(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    //const body = error.json() || '';
    //if (body.message) {
    //  errMsg = body.message ? body.message : error.toString();
    //} else {
    //  errMsg = error.status + ': Error occur on server.';
    //}
    errMsg = error.status + ': ' +error.statusText+ '. '+error._body+'.';    
    return Observable.throw(errMsg);
  }

}
