import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { BridgeRaveStatus } from '../model/bridge-rave-status';
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { Router } from '@angular/router';


@Injectable()
export class BridgeRaveStatusService extends handleErrorBaseServices {

  private restUrl = 'api/bridgeRaveStatus';  // URL to web API

  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router 
    ) {
   super(authenticationService , router );
  }


  getById(id): Observable<BridgeRaveStatus> {
    if(this.checkAuth()){   
      return this.http.get(this.restUrl + '/' + id).map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }  
  }

  getAll(): Observable<BridgeRaveStatus> {
    if(this.checkAuth()){       
      return this.http.get(this.restUrl).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }      
  }
}
