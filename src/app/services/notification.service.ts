import { Injectable } from '@angular/core';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import { handleErrorBaseServices } from 'app/services/handleErrorBaseServices.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { Notification } from 'app/model/notification';
import { PaginationMetadata } from 'app/model/paginationmetadata';


@Injectable()
export class NotificationService extends handleErrorBaseServices{

  private restUrl = 'api';  // URL to web API
  private events;
  constructor(private http : AuthHttp, 
              public authenticationService: AuthenticationService, 
              public router : Router ) {
    super(authenticationService, router);
   }

  getNotificationsByUser() : Observable<Notification[]>{
    if(this.checkAuth()){
      return this.http.get(this.restUrl + '/notifications').map(this.extractData)
          .catch(this.handleError);
    }else{
      return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  getLimitedNotificationsByUser(limit, start, greater): Observable<PaginationMetadata> { 
    if(this.checkAuth()){
      let url = '/notifications?limit=' + limit + '&start=' + start + '&greater=' + greater;
      return this.http.get(this.restUrl + url).map(this.extractData)
      .catch(this.handleError);
      }else {
      return this.http.get('').map((response: Response) => response.json().data);
    }  
  }

  archiveNotification(notifications : Notification[]){
    if(this.checkAuth()){
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers});
     
      return this.http.put(this.restUrl + '/archiveNotification', notifications, options).map(this.extractData)
      .catch(this.handleError);
      }else {
      return this.http.get('').map((response: Response) => response.json().data);
    }
  }
}
