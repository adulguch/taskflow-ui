import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { tokenNotExpired } from 'angular2-jwt'
import { SettingsService } from '../core/settings/settings.service';
import { User } from '../model/user';
import { AuthHttp } from 'angular2-jwt';


import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw'


@Injectable()
export class AuthenticationService {

  public token: string;
  public user: User;
  public redirectUrl: string;


  constructor(private http: Http 
      , private router: Router 
      , public settings: SettingsService 
      , private authhttp: AuthHttp
      ) {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }
  login(username: string, password: string): Observable<boolean> {
    return this.http.post('api/login', JSON.stringify({ username: username, password: password }))
      .map((response: Response) => {
      
        // login successful if there's a jwt token in the response
        const token = response.headers.get('Authorization');  
        if (token) {
          // set token property
          this.token = token;
          // store username and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify({ token: token }));
          localStorage.setItem('token' , token );
          
          // if (this.redirectUrl) {
          //   this.router.navigate([this.redirectUrl]);
          //   this.redirectUrl = null;
          // }

          // return true to indicate successful login
          return true;
        
        } else {
          // return false to indicate failed login
          return false;
        }
      })
       .catch(e => {
            if (e.status === 401) {
                return Observable.throw('Unauthorized');
            }
      });

  }

   getAuthUserDetails(): Observable<User> {
    return this.authhttp.get('api/user').map((res: Response) => { 
      const body = res.json();
      this.user =  body.data;

      this.settings.user =  {
            name: this.user.firstName + ' ' + this.user.lastName,
          };       
    })
      .catch(e => {   
          return Observable.throw('User not found');
      });
  }

  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');    
    //this.router.navigate(['/login']);
    window.location.replace('/login');
    
   }

  loggedIn() {
  //console.log("tokenNotExpired() " + tokenNotExpired());
  return tokenNotExpired();
}

  private handleError(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    //const body = error.json() || '';
    //if (body.message) {
    //  errMsg = body.message ? body.message : error.toString();
    //} else {
    //  errMsg = error.status + ': Error occur on server.';
    //}
    errMsg = error.status + ': ' +error.statusText+ '. '+error._body+'.';    
    return Observable.throw(errMsg);
  }

  private extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

}
