import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { User } from '../model/user';
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';



@Injectable()
export class MyprofileService extends handleErrorBaseServices  {

private restUrl = 'api/myprofile';  // URL to web API

  constructor(private http: AuthHttp) {
   super(null , null );
   }

  updateMyProfile(formData): Observable<User> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.http.put(this.restUrl + '/' + 'profile', formData, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  updateMyPassword(formData): Observable<User> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.http.put(this.restUrl + '/' + 'password', formData, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  getMyProfile(): Observable<User> {
    return this.http.get(this.restUrl).map(this.extractData)
      .catch(this.handleError);
  }
}