import { AuthHttp } from 'angular2-jwt';
import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Study } from '../model/study';
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectData } from 'app/model/selected-data';
import { Userprivileges } from 'app/model/userprivileges';
import { PrivilageService } from 'app/services/privilage.service';
import { Task } from 'app/model/task';
import { StudyStats } from '../model/studyStats';


@Injectable()
export class StudyService extends handleErrorBaseServices{

  private restUrl = 'api/study';  // URL to web API
  public appUserPrivilege : Userprivileges;
  public deliverablePrivilege : Userprivileges;
  public groupPrivilege : Userprivileges;
  

  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router,
    private privilage : PrivilageService,
    private zone: NgZone 
    ) {
   super(authenticationService , router );

   this.appUserPrivilege = new Userprivileges();
    this.deliverablePrivilege  = new Userprivileges();
    this.groupPrivilege  = new Userprivileges();

    this.initializePrivileges(this.appUserPrivilege);
    this.initializePrivileges( this.deliverablePrivilege);
    this.initializePrivileges( this.groupPrivilege);

    this.privilage.CheckUserAccess().subscribe((data : Userprivileges) => {
      this.zone.run(() => {
        this.assignPrivileges(data, this.appUserPrivilege);
      });
    });

    this.privilage.CheckDeliverableAccess().subscribe((data : Userprivileges) => {
      this.zone.run(() => {
        this.assignPrivileges(data, this.deliverablePrivilege);
      });
    });

    this.privilage.CheckGroupAccess().subscribe((data : Userprivileges) => {
      this.zone.run(() => {
        this.assignPrivileges(data, this.groupPrivilege);
      });
    });
  }

  create(formData): Observable<Study> {
    if(this.checkAuth()){
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.post(this.restUrl, formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

update(id, formData): Observable<Study> {
    if(this.checkAuth()){   
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.put(this.restUrl + '/' + id, formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }  
}

  deleteStudy(userId, user : Study): Observable<Study> {
    if(this.checkAuth()){   
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers});
        console.log(this.restUrl + '/' + userId + '/' + 'softDelete');
        return this.http.put(this.restUrl + '/' + userId + '/' + 'softDelete', user, options)
          .map(this.extractData)
          .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }


  getStudyById(studyId): Observable<Study> {
    return this.http.get(this.restUrl + '/' + studyId).map(this.extractData)
      .catch(this.handleError);
  }

  getAll(): Observable<Study> {    
    if(this.checkAuth()){ 
      return this.http.get(this.restUrl+ '/all').map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  getStudyByUsers(): Observable<Study> {    
    if(this.checkAuth()){ 
      return this.http.get(this.restUrl+ '/userStudies').map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }
  

  updateUsers(id, formData): Observable<Study> {
    if(this.checkAuth()){   
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.put(this.restUrl + '/updateData/' + id, formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }  
}

getTasksByStudyId(studyId): Observable<Task> {
    if(this.checkAuth()){ 
      return this.http.get(this.restUrl+ '/studyTaskes' + '/'+ studyId ).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }


delete(id): Observable<any> {
    if(this.checkAuth()){                
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.delete(this.restUrl + '/' + id, options)
        .map(res => res)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
   }

  }

  getStudiesByDeliverableId(deliverableId): Observable<Study> {
    if(this.checkAuth()){ 
      return this.http.get(this.restUrl+ '/deliverableStudies' + '/'+ deliverableId ).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  getStudyStatById(studyId): Observable<StudyStats> {
    if(this.checkAuth()){ 
      return this.http.get(this.restUrl+ '/studyStats' + '/'+ studyId ).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }


initializePrivileges(privilege : Userprivileges){
    
  privilege.create = false;
  privilege.view = false;
  privilege.edit = false;
  privilege.delete = false;
  privilege.bridgeStatus = false;
  privilege.dataViewer = false;
}

assignPrivileges(responseData : Userprivileges, privilege : Userprivileges){
  privilege.view = responseData.view;
  privilege.create = responseData.create;
  privilege.edit = responseData.edit;
  privilege.delete = responseData.delete;
  privilege.bridgeStatus = responseData.bridgeStatus;
  privilege.dataViewer = responseData.delete;
}

}
