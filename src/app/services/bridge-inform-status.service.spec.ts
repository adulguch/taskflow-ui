import { TestBed, inject } from '@angular/core/testing';
import { BridgeInformStatusService } from './bridge-inform-status.service';

describe('BridgeInformStatusService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BridgeInformStatusService]
    });
  });

  it('should ...', inject([BridgeInformStatusService], (service: BridgeInformStatusService) => {
    expect(service).toBeTruthy();
  }));
});
