import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Bridge } from '../model/bridge';
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';


@Injectable()
export class BridgeService extends handleErrorBaseServices {

  private restUrl = 'api/bridge';  // URL to web API

  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router 
    ) {
   super(authenticationService , router );
  }


  create(formData): Observable<Bridge> {
    if(this.checkAuth()){ 
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

    return this.http.post(this.restUrl, formData, options)
      .map(this.extractData)
      .catch(this.handleError);
     } else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  update(id, formData): Observable<Bridge> {
    if(this.checkAuth()){ 
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.put(this.restUrl + '/' + id, formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      } else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  updateActive(id, formData): Observable<Bridge> {
    if(this.checkAuth()){ 
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.put(this.restUrl + '/active/' + id, formData, options)
      .map(this.extractData)
      .catch(this.handleError);
    } else {
      return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  delete(id): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.delete(this.restUrl + '/' + id, options)
      .map(res => res)
      .catch(this.handleError);

  }

  isBridgeActive(id): Observable<any> {
    if(this.checkAuth()){   
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.get(this.restUrl + '/isactive/' + id, options).map(this.extractData)
        .catch(this.handleError);
     }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }

  }

  runGetTransaction(id): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.get(this.restUrl + '/runGetTransaction/' + id, options)
      .map(res => res)
      .catch(this.handleError);

  }

  runDownloadMetadata(id): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.get(this.restUrl + '/runDownloadMetadata/' + id, options)
      .map(res => res)
      .catch(this.handleError);

  }

  runCreateSetup(id): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.get(this.restUrl + '/runCreateSetup/' + id, options)
      .map(res => res)
      .catch(this.handleError);

  }

  getById(id): Observable<Bridge> {
    if(this.checkAuth()){ 
    return this.http.get(this.restUrl + '/' + id).map(this.extractData)
      .catch(this.handleError);
    }else {
      return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  getAllByTargetId(id): Observable<Bridge> {
    if(this.checkAuth()){ 
    return this.http.get(this.restUrl + '/bytarget/' + id).map(this.extractData)
      .catch(this.handleError);
    }else {
      return this.http.get('').map((response: Response) => response.json().data);
    }
  }


  getAll(): Observable<Bridge> {
    if(this.checkAuth()){   
      return this.http.get(this.restUrl).map(this.extractData)
        .catch(this.handleError);
     }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }
  
}
