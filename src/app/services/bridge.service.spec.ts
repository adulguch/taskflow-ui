import { TestBed, inject } from '@angular/core/testing';
import { BridgeService } from './bridge.service';

describe('BridgeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BridgeService]
    });
  });

  it('should ...', inject([BridgeService], (service: BridgeService) => {
    expect(service).toBeTruthy();
  }));
});
