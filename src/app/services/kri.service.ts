import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Kri } from '../model/kri';
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserKRIRule } from 'app/model/userKriRule';


@Injectable()
export class KriService extends handleErrorBaseServices {

  private restUrl = 'api/kri';  // URL to web API
  private restDashboardUrl = 'api/kri/dashboard';  // URL to web API
  private restKRIExecution = 'api/kri/execute';


  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router 
    ) {
   super(authenticationService , router );
  }


  create(formData): Observable<Kri> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.post(this.restUrl, formData, options)
      .map(this.extractData)
      .catch(this.handleError);

  }

  update(id, formData): Observable<Kri> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.put(this.restUrl + '/' + id, formData, options)
      .map(this.extractData)
      .catch(this.handleError);

  }

  delete(id): Observable<any> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.delete(this.restUrl + '/' + id, options)
      .map(res => res)
      .catch(this.handleError);

  }

 
  getById(id): Observable<Kri> {
    return this.http.get(this.restUrl + '/' + id).map(this.extractData)
      .catch(this.handleError);
  }

  runKriById(id): Observable<any>{
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.http.post(this.restKRIExecution + '/' + id, options)
      .map(res => res)
      .catch(this.handleError);

  }

  getAll(): Observable<Kri> {
    if(this.checkAuth()){   
      return this.http.get(this.restUrl).map(this.extractData)
        .catch(this.handleError);
     }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  getKRIAllExecution(): Observable<Kri> {
      return this.http.get(this.restDashboardUrl).map(this.extractData)
        .catch(this.handleError);
  }

  userKriSubscribe(userKriList : UserKRIRule[]) : Observable<UserKRIRule>{
    console.log(userKriList);
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });
    return this.http.put(this.restUrl + '/notifications', userKriList, options)
      .map(this.extractData)
      .catch(this.handleError); 
  }

  getUserKriSubscriptionList() : Observable<UserKRIRule>{
    return this.http.get(this.restUrl + '/filteredKri').map(this.extractData)
    .catch(this.handleError);
  }

  
}
