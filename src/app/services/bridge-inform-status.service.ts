import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { BridgeInformStatus } from '../model/bridge-inform-status';
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { AuthenticationService } from './authentication.service';
import { Router } from '@angular/router';


@Injectable()
export class BridgeInformStatusService extends handleErrorBaseServices {

  private restUrl = 'api/bridgeInFormStatus';  // URL to web API

  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router 
    ) {
   super(authenticationService , router );
  }

  getById(id): Observable<BridgeInformStatus> {
    if(this.checkAuth()){
      return this.http.get(this.restUrl + '/' + id).map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  getAll(): Observable<BridgeInformStatus> {
    if(this.checkAuth()){   
      return this.http.get(this.restUrl).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }
}
