import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Trial } from '../model/trial';
import { Bridge } from '../model/bridge';
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable()
export class TrialService extends handleErrorBaseServices{

  private restUrl = 'api/trial';  // URL to web API

  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router 
    ) {
   super(authenticationService , router );
  }

  create(formData): Observable<Trial> {
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.post(this.restUrl, formData, options)
      .map(this.extractData)
      .catch(this.handleError);

  }

  update(id, formData): Observable<Trial> {
    if(this.checkAuth()){
    const headers = new Headers({ 'Content-Type': 'application/json' });
    const options = new RequestOptions({ headers: headers });

    return this.http.put(this.restUrl + '/' + id, formData, options)
      .map(this.extractData)
      .catch(this.handleError);}else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }
  delete(id): Observable<any> {    
    if(this.checkAuth()){   
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this.http.delete(this.restUrl + '/' + id, options)
          .map(res => res)
          .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  getById(id): Observable<Trial> {
    return this.http.get(this.restUrl + '/' + id).map(this.extractData)
      .catch(this.handleError);
  }

  activeBridgeByTrialId(id): Observable<Bridge> {
   if(this.checkAuth()){
      return this.http.get(this.restUrl + '/activeBridges/' + id).map(this.extractData)
        .catch(this.handleError);
     }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  getAll(): Observable<Trial> {    
    if(this.checkAuth()){        
        return this.http.get(this.restUrl).map(this.extractData)
          .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  getAllActive(): Observable<Trial> {    
    if(this.checkAuth()){        
        return this.http.get(this.restUrl + '/active').map(this.extractData)
          .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

}
