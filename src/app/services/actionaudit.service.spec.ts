import { TestBed, inject } from '@angular/core/testing';

import { ActionauditService } from './actionaudit.service';

describe('ActionauditService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActionauditService]
    });
  });

  it('should ...', inject([ActionauditService], (service: ActionauditService) => {
    expect(service).toBeTruthy();
  }));
});
