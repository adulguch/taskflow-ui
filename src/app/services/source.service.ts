import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Source } from '../model/source';
import { Bridge } from '../model/bridge';
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';



@Injectable()
export class SourceService extends handleErrorBaseServices {


  private restUrl = 'api/source';  // URL to web API
  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router 
    ) {
   super(authenticationService , router );
  }

  create(formData): Observable<Source> {
    if(this.checkAuth()){ 
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.post(this.restUrl, formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      } else {
        return this.http.get('').map((response: Response) => response.json().data);
      }

  }

  update(id, formData): Observable<Source> {
    if(this.checkAuth()){
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.put(this.restUrl + '/' + id + '/' + 'profile', formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      } else {
              return this.http.get('').map((response: Response) => response.json().data);
    }
  }
  delete(id): Observable<any> {
    if(this.checkAuth()){           
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });

        return this.http.delete(this.restUrl + '/' + id, options)
          .map(res => res)
          .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }

  }

  getById(id): Observable<Source> {
    return this.http.get(this.restUrl + '/' + id).map(this.extractData)
      .catch(this.handleError);
  }

  activeBridgeBySourceId(id): Observable<Bridge> {
   if(this.checkAuth()){           
      return this.http.get(this.restUrl + '/activeBridges/' + id).map(this.extractData)
        .catch(this.handleError);
     }else {
        return this.http.get('').map((response: Response) => response.json().data);
   }
  }

  getAll(): Observable<Source> {
     if(this.checkAuth()){           
        return this.http.get(this.restUrl).map(this.extractData)
          .catch(this.handleError);
     }else {
        return this.http.get('').map((response: Response) => response.json().data);
   }

  }

  getAllActive(): Observable<Source> {
    if(this.checkAuth()){           
       return this.http.get(this.restUrl + '/active').map(this.extractData)
         .catch(this.handleError);
    }else {
       return this.http.get('').map((response: Response) => response.json().data);
  }

 }

  updatePassword(sourceId, formData): Observable<Source> {
    if(this.checkAuth()){ 
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });
      return this.http.put(this.restUrl + '/' + sourceId + '/' + 'password', formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }
}
