import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { AuthenticationService } from 'app/services/authentication.service';
import { Router } from '@angular/router';
import { Group } from '../model/group';
import { handleErrorBaseServices } from 'app/services/handleErrorBaseServices.service';
import { User } from '../model/user';
import { SelectData } from 'app/model/selected-data';

@Injectable()
export class GroupService extends handleErrorBaseServices {
  private restUrl = 'api/group';  // URL to web API

 
  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router 
    ) {
   super(authenticationService , router );
  }

  create(formData): Observable<Group> {
    if(this.checkAuth()){           
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers });
        return this.http.post(this.restUrl, formData, options)
          .map(this.extractData)
          .catch(this.handleError);
     }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  update(groupId, formData): Observable<Group> {
    if(this.checkAuth()){   
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.put(this.restUrl + '/' + groupId, formData, options)
      .map(this.extractData)
      .catch(this.handleError);
    }else {
      return this.http.get('').map((response: Response) => response.json().data);
     } 
  }

  delete(groupId): Observable<any> {
    if(this.checkAuth()){           
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });
      return this.http.delete(this.restUrl + '/' + groupId, options)
        .map(res => res)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  getGroupById(groupId): Observable<Group> {
    return this.http.get(this.restUrl + '/' + groupId).map(this.extractData)
      .catch(this.handleError);
    
  }

  getAll(): Observable<Group> {    
    if(this.checkAuth()){   
        return this.http.get(this.restUrl).map(this.extractData)
          .catch(this.handleError);
      }else {
          return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  getAllUsers(): Observable<User> {  
    return this.http.get('api/groupusers').map(this.extractData)
      .catch(this.handleError);
  }

  getFilteredUsers(groupId) : Observable<SelectData>{
    if(this.checkAuth()){
      return this.http.get(this.restUrl + '/filterUsers/' + groupId ).map(this.extractData)
          .catch(this.handleError);
    }else{
      return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  getGroupsByUser(): Observable<Group> {    
    if(this.checkAuth()){   
        return this.http.get(this.restUrl + '/userGroups').map(this.extractData)
          .catch(this.handleError);
      }else {
          return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  getFilteredGroupsByStudyId(studyId): Observable<SelectData> {
    if(this.checkAuth()){
      return this.http.get(this.restUrl + '/filteredGroups/' + studyId).map(this.extractData)
        .catch(this.handleError);
    }else {
      return this.http.get('').map((response: Response) => response.json().data);
   }
  }


}
