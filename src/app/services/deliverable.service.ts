import { AuthHttp } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Deliverable } from '../model/deliverable' ;
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { AuthenticationService } from 'app/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SelectData } from 'app/model/selected-data';
import { Task } from '../model/task';
import { DeliverableStats } from '../model/deliverableStats';


@Injectable()
export class DeliverableService extends handleErrorBaseServices{

private restUrl = 'api/deliverable';  // URL to web API
private restImpactUrl = 'api/task';

  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router 
    ) {
   super(authenticationService , router );
  }

  create(formData): Observable<Deliverable> {
    if(this.checkAuth()){
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.post(this.restUrl, formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

update(id, formData): Observable<Deliverable> {
    if(this.checkAuth()){   
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.put(this.restUrl + '/' + id, formData, options)
        .map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }  
}

  deleteDeliverable(userId, user : Deliverable): Observable<Deliverable> {
    if(this.checkAuth()){   
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions({ headers: headers});
        console.log(this.restUrl + '/' + userId + '/' + 'softDelete');
        return this.http.put(this.restUrl + '/' + userId + '/' + 'softDelete', user, options)
          .map(this.extractData)
          .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }
  }

  getDeliverableById(DeliverableId): Observable<Deliverable> {
    return this.http.get(this.restUrl + '/' + DeliverableId).map(this.extractData)
      .catch(this.handleError);
  }
  
  getAll(): Observable<Deliverable> {    
    if(this.checkAuth()){ 
      return this.http.get(this.restUrl+ '/all').map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  getFilteredDeliverablesByStudyId(studyId): Observable<SelectData> {
    if(this.checkAuth()){
      return this.http.get(this.restUrl + '/filteredDeliverables/' + studyId).map(this.extractData)
        .catch(this.handleError);
    }else {
      return this.http.get('').map((response: Response) => response.json().data);
   }
  }

  getDeliverablesByStudyId(studyId): Observable<Deliverable> {
    if(this.checkAuth()){
      return this.http.get(this.restUrl + '/studyDeliverables/' + studyId).map(this.extractData)
        .catch(this.handleError);
    }else {
      return this.http.get('').map((response: Response) => response.json().data);
   }
  }

  delete(id): Observable<any> {
    if(this.checkAuth()){                
      const headers = new Headers({ 'Content-Type': 'application/json' });
      const options = new RequestOptions({ headers: headers });

      return this.http.delete(this.restUrl + '/' + id, options)
        .map(res => res)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
   }

  }
  getTasksByDeliverableId(DeliverableId): Observable<Task> {
    if(this.checkAuth()){
      return this.http.get(this.restUrl + '/deliverableTaskes/' + DeliverableId).map(this.extractData)
        .catch(this.handleError);
    }else {
      return this.http.get('').map((response: Response) => response.json().data);
   }
  }

  getDeliverableStatById(DeliverableId): Observable<DeliverableStats> {
    if(this.checkAuth()){ 
      return this.http.get(this.restUrl+ '/deliverableStats' + '/'+ DeliverableId ).map(this.extractData)
        .catch(this.handleError);
    }else {
        return this.http.get('').map((response: Response) => response.json().data);
     }
  }

  getImpactedTaskByDeliverable(DeliverableId): Observable<Task> {
    if(this.checkAuth()){
      return this.http.get(this.restImpactUrl+ '/impactedTaskByDeliverable/' + DeliverableId).map(this.extractData)
        .catch(this.handleError);
    }else {
      return this.http.get('').map((response: Response) => response.json().data);
   }
  }
  getImpactedTaskStaticsByDeliverable(DeliverableId): Observable<Task> {
    if(this.checkAuth()){
      return this.http.get(this.restImpactUrl+ '/impactedTaskStaticsByDeliverable/' + DeliverableId).map(this.extractData)
        .catch(this.handleError);
    }else {
      return this.http.get('').map((response: Response) => response.json().data);
   }
  }


}
