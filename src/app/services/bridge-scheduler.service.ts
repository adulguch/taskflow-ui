import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { BridgeSchedulerStats } from '../model/BridgeSchedulerStats';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { handleErrorBaseServices } from '../services/handleErrorBaseServices.service';
import { AuthenticationService } from './authentication.service';
import { Router } from '@angular/router';


@Injectable()
export class BridgeSchedulerService extends handleErrorBaseServices {

private restUrl = 'api/bridges/scheduler';  // URL to web API

  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router
  ) {
   super(authenticationService , router  );
   }

    getAll(): Observable<BridgeSchedulerStats> {    
     if(this.checkAuth()){
      return this.http.get(this.restUrl).map(this.extractData)
        .catch(this.handleError);
        } else {
          return this.http.get('').map((response: Response) => response.json().data);
      }
  }

  getStatById(id): Observable<BridgeSchedulerStats> {  
    if(this.checkAuth()){
      return this.http.get(this.restUrl + '/' + id).map(this.extractData)
        .catch(this.handleError);
      } else {
        return this.http.get('').map((response: Response) => response.json().data);
      }
  }
}
