import { Component, OnInit, Input, ViewChild } from '@angular/core';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {

  private popupBody: string;
  private popupTitle: string;
  private popupHeaderClass: string;
  private popupButtonClass: string;

  // @ViewChild('popModal') ps: any;

  constructor() { }

  ngOnInit() {
  }
/*
  public show(popupBody: string, popupTitle: string, popupClass: string) {
    this.popupBody = popupBody;
    this.popupTitle = popupTitle;
    if (popupClass === 'success') {
      this.popupHeaderClass = 'bg-success';
      this.popupButtonClass = 'bg-success';
    }
    if (popupClass === 'error') {
      this.popupHeaderClass = 'bg-danger';
      this.popupButtonClass = 'bg-danger';
    }
    // this.ps.show();
    console.log(this.popupBody);
    console.log(this.popupTitle);
    console.log(this.popupHeaderClass);
    console.log(this.popupButtonClass);

  }
*/
}
