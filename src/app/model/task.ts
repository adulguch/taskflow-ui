import { TaskComment } from 'app/model/taskComment';
import { AppUser } from 'app/model/appuser';
import { Deliverable } from './deliverable';


export class Task {

    taskId:number;
    name: string;
    type: String;
    parentId: number;
    level: number;
    description: String;
    studyId: number;
    studyName: number;
    studyCode: number;
    deliverableId: number;
    deliverableName: string;
    deliverableCOde: string;
    priority: String;
    status: String;
    user: AppUser;    
    assigneeId : number;
    assigneeName: string;
    startDate: string;
    endDate: string;
    comments: Array<TaskComment>
    comment: TaskComment;
    linkedDeliverables: Array<Deliverable> = [];
    estimatedTime: number;
    actualTime: number;

    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;

    hasChildren: boolean = true ;


    documents: Array<File>;
    fileNames: Array<string>;
    
}