import { KriBridgesTables } from 'app/model/kri-bridges-tables';

export class KriDashboard {        
    
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;   
    kriExecutionIdPk: number;
    kriIdFk: number;
    kriStatus: string;
    kriRulename : string ;
    bridgeId : number;
    bridgeName : string;
    runDate : string;
    rules : string;
    description : string;
    bridgeTableAsList: Array<KriBridgesTables> ;
    eligibleToRun : boolean;
}
