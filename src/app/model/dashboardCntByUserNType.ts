export class DashboardCntByUserNType {
	
	studyName: string;
    deliverableName : string;
    userName : string;
    taskType : string;
    taskCount: number;    
}