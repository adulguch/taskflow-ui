
export class BridgeInformStatus {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    bridgeStatusPk: number;
    bridgeFk: number;
    bridgeName: string;    
    action: string;
    bookmark: string;
    previousBookmark: string;
    fileName: string;
    status: string;
    transactionProcessed: string;    
}