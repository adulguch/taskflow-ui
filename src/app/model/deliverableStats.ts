import { StudyStats } from "./studyStats";

export class DeliverableStats {
    
    deliverableId: number;
    deliverableName: string;
    devTask: string;
    testTask: string;
    reviewTask: string;
    studyState : StudyStats[];

}