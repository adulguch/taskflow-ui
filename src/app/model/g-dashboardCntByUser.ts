export class GDashboardCntByUser {
	
    studyName : string;
    deliverableName : string;    
    userName : string;
    taskCount: number;
    
}