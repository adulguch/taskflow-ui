import { Actionaudit } from "app/model/actionaudit";

export class PaginationMetadata{
	totalRecords : number;
	previousData;
	currentData; 
	nextData;
}