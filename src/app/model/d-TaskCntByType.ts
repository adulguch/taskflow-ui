export class DTaskCntByType {
	
    taskType: string;
    taskCount: number;
    countPer : number;

    constructor(taskType, taskCount, countPer){
        this.taskType = taskType;
        this.taskCount = taskCount;
        this.countPer = countPer;
    }
}