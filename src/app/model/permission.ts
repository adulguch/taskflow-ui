
export class Permission {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    permissionIdPk: number;
    permissionName: string;
    permissionDescription: string;
    active: number;
}
