
export class Actionaudit {    
    performedAction: string;        
    dateTime: Date;
    performedBy: string;
    description: string;
    auditId : number;
}