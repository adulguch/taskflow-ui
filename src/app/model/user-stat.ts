export class UserStat{
    totalUsers : number;       
    activeUsers : number;      
    totalLicenses : number;    
    availableLicenses : number;
}