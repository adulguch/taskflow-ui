import { Role } from 'app/model/role';


export class User {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    userIdPk: number;
    userName: string;
    password: string;
    firstName: string;
    lastName: string;
    email: string;
    telephone: string;        
    active: number;
    accountLocked: boolean;
    loginFailAttempt: number;
    roles: Array<Role>;
    
}