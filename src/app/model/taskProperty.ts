import { TaskType } from 'app/model/TaskType';

export class TaskProperty {

   taskTypeList: Array<TaskType> ;
   
   taskStatusList: Array<String>;
   taskPriorityList: Array<String>;

}