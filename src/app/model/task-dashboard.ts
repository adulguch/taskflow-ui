import { GDashboardCntByType } from 'app/model/g-dashboardCntByType';
import { GDashboardCntByUser } from 'app/model/g-dashboardCntByUser';
import { DashboardCntByUser } from 'app/model/dashboardCntByUser';
import { DashboardCntByUserNType } from 'app/model/dashboardCntByUserNType';


import { DTaskCntByType } from 'app/model/d-TaskCntByType';
import { DTaskCntByStatus } from 'app/model/d-TaskCntByStatus';



export class TaskDashboard {    
    
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    
    //gdashboardCntByType : Array<GDashboardCntByType>;        
	//gdashboardCntByUser : Array<GDashboardCntByUser>;
	
    dashboardCntByUser:  Array<DashboardCntByUser>;
	dashboardCntByUserNType: Array<DashboardCntByUserNType>;

    dtaskCntByType : Array<DTaskCntByType>;
    dtaskCntByStatus : Array<DTaskCntByStatus>;

    }