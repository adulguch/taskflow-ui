
export class BridgeRaveStatus {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    bridgeStatusPk: number;
    bridgeFk: number;
    bridgeName: string;    
    action: string;
    currentPageId: number;
    nextPageId: number;
    fileName: string;
    status: string;
}