import { BridgeTransState } from 'app/model/BridgeTransState';
export class BridgeDashboard {        
    
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    totalActiveBridge: number;
    executedLastHour: number;
    failedLastHour: number;
    edcServersCount : number;
    trialNamesCount : number;
    databaseServerCount : number;
    databaseNameCount : number;
    bridgeTransStatisticsList: Array<BridgeTransState>; 
}
