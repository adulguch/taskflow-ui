
export class TaskSearch {
    studyId: number;
    taskId: number;
    deliverableId: string;
    priority: string;
    status: string;   
    assigneeId: number;
    type: string;
    linkedDeliverableId: number;
    combineStatus: string;  
    totalEstimationTime: number;
    totalActualTime: number;
}