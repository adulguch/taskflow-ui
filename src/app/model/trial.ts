
export class Trial {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    trialPk: number;
    sourceFk: number;
    sourceSiteUrl: string;
    name: string;    
    description: string;
    active: number;
}