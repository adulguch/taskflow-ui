
export class Deliverable {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    deliverableId: number;
    deliverableName: string;
    deliverableCode: string; 
    deliverableType: string; 
    scope: string; 
    deliverableDescription: string; 
    active: number;
}