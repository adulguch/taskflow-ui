
export class Target {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    targetPk: number;
    vendorFk: number;
    hostName: string;
    ip: string;
    port: number;
    dbName: string;
    userName: string;
    password: string;
    description: string;
    active: number;
    connectionStatus: number;
}