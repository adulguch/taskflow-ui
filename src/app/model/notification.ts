export class Notification{
	id : number;
	userId : number;
	eventId : number;
	subject : string;
	content : string;
	creationTime : string
	archived : number;
}