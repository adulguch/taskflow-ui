export class UserKRIRule {
	kriRuleId : number;
	userId : number;
	rulePass : number;
    ruleFail : number;
    kriRuleName : string;
    
    constructor(){
        this.kriRuleId = 0;
        this.userId = 0;
        this.ruleFail = 0;
        this.rulePass = 0;
    }
}
