
export class Db {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    dbPk: number;
    targetFk: number;
    hostName: string;
    name: string;    
    description: string;
    active: number;
    dbUsed: number
}