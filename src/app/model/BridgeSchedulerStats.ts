export class BridgeSchedulerStats {
    
    jobRunId: number;
    bridgeId: number;    
    bridgeName: string;
    lastRun: Date;
    nextRun: Date;
    runType: string;
    status: string;
    comment: string;
    modifiedDate : Date;
    
}
