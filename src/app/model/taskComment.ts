
export class TaskComment {
    
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    
    taskId:number;
    action:string;
    userId:number;
    comment:string;

}