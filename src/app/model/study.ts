
export class Study {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    studyId: number;
    studyName: string;
    studyCode: string;
    studyTitle: string;
    studyDescription: string;
    clinicalPhase: string;
    therapeuticArea: string;
    active: number;
    users : any[];
    deliverables : any[];
    groups : any[];
}