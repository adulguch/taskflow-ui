import { Permission } from 'app/model/permission';

export class Role {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    roleIdPk: number;
    roleName: string;
    roleDescription: string;
    active: number;
    permissions: Array<Permission>;
}
