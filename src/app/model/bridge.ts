
export class Bridge {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    bridgePk: number;
    sourceFk: number;
    sourceVendorFk: number;
    sourceSiteUrl: string;
    targetFk: number;
    targetHostName: string;
    trialFk: number;
    trialName: string;
    dbFk: number;
    dbName: string;
    name: string;
    env: string;
    description: string;
    active: number;
    cron: string;
    cronStatus: number;
}