import { Role } from 'app/model/role';
import { Study } from 'app/model/study';
import { Group } from 'app/model/group';

export class AppUser {

    userId: number;
    userName: string;

    roles: Array<Role>;
    groups: Array<Group>;
    studies: Array<Study>;

}