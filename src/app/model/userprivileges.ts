
export class Userprivileges {    
    
    edit: boolean ;        
    delete : boolean ;
    create : boolean ;
    view : boolean;
    
    //below is for soem specif scenario
    bridgeStatus : boolean ;
    dataViewer : boolean ;
    manualRun: boolean;

}