
export class Source {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    sourcePk: number;
    vendorFk: number;
    userName: string;
    active: number;
    siteUrl: string;
    port: number;
    password: string;
    ip: string;
    hostName: string;
    description: string;
}