import { KriBridgesTables } from 'app/model/kri-bridges-tables';


export class Kri {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    kriIdPk: number; 
    bridgeId: number;
    bridgeName: string;    
    kriRulename: string;
    rules: string;
    active: number;
    //rulesOntable: string;
    description: string;
    bridgeTableAsList: Array<KriBridgesTables> ;
}