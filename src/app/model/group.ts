import { User } from "./user";

export class Group {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    id: number;
    name: string;       
    active: number;
    description: string;
    users: Array<User>;
}