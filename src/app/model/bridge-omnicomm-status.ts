
export class BridgeOmniCommStatus {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;    
    bridgeStatusPk: number;
    bridgeFk: number;
    bridgeName: string;    
    action: string;  
    studyOid: string;
    metadataVersionOid: string;
    subjectKey: string;
    studyEventOid: string;
    studyEventRepeatKey: string;
    formOid: string; 
    formRepeatKey: string;
    fileName: string;
    status: string;
    active: number;
    executionType: string;
}

