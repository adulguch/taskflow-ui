import { DeliverableStats } from 'app/model/deliverableStats';

export class StudyStats {
    
    studyId: number;
    studyName: string;    
    devTask: string;
    testTask: string;
    reviewTask: string;
    deliverableState : DeliverableStats[];
}