export class BridgeTransState {
    createdBy: string;
    createdDate: Date;
    modifiedBy: string;
    modifiedDate: Date;
    bridgePk: number;
    dashboardName: string;
    totalTrans: number;
    prossedTrans: number;
    remainingTrans: number;
    completion: string;
}
