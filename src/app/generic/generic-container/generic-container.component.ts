import { GenericTableComponent } from './generic-table/generic-table.component';
import { GenericTableDirective } from './generic-table-directive';
import { GenericModule } from './../generic-ui.module';
import { GenericService } from './../services/generic.service';
import { ScreenConfig } from './../model/screen-config';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import {
  Component, AfterViewInit, OnDestroy, ViewChild, Input,
  OnChanges, SimpleChange, NgZone, ComponentFactoryResolver
} from '@angular/core';

@Component({
  selector: 'app-generic-container',
  templateUrl: './generic-container.component.html',
  styleUrls: ['./generic-container.component.scss']
})
export class GenericContainerComponent implements AfterViewInit, OnDestroy {


  private _screenPk: string;
  private tableData: Array<any>;
  private metaData: ScreenConfig;
  private showSpinner = 1;

  @Input() pageSize: number;
  @Input() bridgeId: number;
  @Input() screenName: string;
  @ViewChild(GenericTableDirective) genericTable: GenericTableDirective;

  constructor(private zone: NgZone,
    private genericService: GenericService, 
    private componentFactoryResolver: ComponentFactoryResolver) {
  }

  @Input()
  set screenPk(id: string) {
    this._screenPk = id;
    if (id) {
      this.reconfigure();
    }
  }

  get screenPk() {
    return this._screenPk;
  }

  private reconfigure() {
    //this.genericService.getScreenMetaData(this.bridgeId, this.screenName + '_' + this.screenPk).subscribe((config) => {
      this.genericService.getScreenMetaData(this.bridgeId, this.screenPk).subscribe((config) => {      
      this.zone.run(() => {
        this.metaData = config;
        this.load();
        // console.log(this.metaData);
      });
    }, (error) => {
      console.log(error);
    });
  }

  private load() {
    const params = {
      'Column': 'data'
    };
    //this.genericService.getData(this.bridgeId, this.screenName + '_' + this.screenPk, params).subscribe((data) => {
    this.genericService.getData(this.bridgeId, this.screenPk, params).subscribe((data) => {    
      this.zone.run(() => {
        this.tableData = data;
      // console.log(this.tableData);
        this.showSpinner = 0;
        this.createtable();
      });
    }, (error) => {
      console.log(error);

    });
  }

  private createtable() {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(GenericTableComponent);
    const viewContainerRef = this.genericTable.viewContainerRef;
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent(componentFactory);
    (<GenericTableComponent>componentRef.instance).metaData = this.metaData;
    (<GenericTableComponent>componentRef.instance).tableData = this.tableData;
    (<GenericTableComponent>componentRef.instance).screenName = this.screenName;

    if (this.pageSize) {
      (<GenericTableComponent>componentRef.instance).pageSize = this.pageSize;
    }

  }

  ngAfterViewInit() {
    // this.reconfigure();
  }

  ngOnChanges() {
    this.showSpinner = 1;
  }
  
  ngOnDestroy() {

  }
}
