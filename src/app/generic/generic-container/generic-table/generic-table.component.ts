import { ScreenConfig } from './../../model/screen-config';
import { Component, OnInit, Input, Renderer } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-generic-table',
  templateUrl: './generic-table.component.html',
  styleUrls: ['./generic-table.component.scss']
})
export class GenericTableComponent implements OnInit {

  @Input() tableData: Array<any>;
  @Input() metaData: ScreenConfig;
  @Input() pageSize = 50;
  @Input() screenName: string;

  genericData: Array<any>;
  colLength = 0;
  start: number;
  pressed: boolean;
  startX: number;
  startWidth: number;
  totalRecords: number = 0;

  constructor(public renderer: Renderer) { }

  onMouseDown(event) {
    this.start = event.target;
    this.pressed = true;
    this.startX = event.x;
    // this.startWidth = $(this.start).parent().width();
    this.initResizableColumns();
  }

  private initResizableColumns() {
    this.renderer.listenGlobal('body', 'mousemove', (event) => {
      // if (this.pressed) {
      //   const width = this.startWidth + (event.x - this.startX);
      //   $(this.start).parent().css({ 'min-width': width, 'max-   width': width });
      //   const index = $(this.start).parent().index() + 1;
      //   $('.glowTableBody tr td:nth-child(' + index + ')').css({ 'min-width': width, 'max-width': width });
      // }
    });

    this.renderer.listenGlobal('body', 'mouseup', (event) => {
      if (this.pressed) {
        this.pressed = false;
      }
    });
  }

  private onQuickFilterChanged($event) {        
        if ($event.target.value) {
            this.genericData = this.tableData.filter(item => {
                return Object.keys(item).some(keyName => {                     
                     if (item[keyName] !== null) {
                        return item[keyName].toString().match(new RegExp($event.target.value, 'i'));
                     }
                });
            });
            this.totalRecords = this.genericData.length;
        } else {
            this.genericData = this.tableData;
            this.totalRecords = this.genericData.length;
        }
    }

  ngOnChanges() {
    this.colLength = this.metaData.columns.length;
    this.genericData = this.tableData;
    this.totalRecords = this.genericData.length;
  }

  ngOnInit() {
    this.colLength = this.metaData.columns.length;
    this.genericData = this.tableData;
    this.totalRecords = this.genericData.length;
  }

}
