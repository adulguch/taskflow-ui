import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    // tslint:disable-next-line:directive-selector
    selector: '[genericTable]',
})
export class GenericTableDirective {
    constructor(public viewContainerRef: ViewContainerRef) { }
}
