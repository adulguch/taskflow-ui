export class Column {
    name: string;
    title: string;
    width: string;
    style = '';
}
