import { Menu } from './menu';
import { Column } from './column';
export class ScreenConfig {

    screenId: string;
    columns: Array<Column>;
    menu: Menu;
    bridgeId: number;

}
