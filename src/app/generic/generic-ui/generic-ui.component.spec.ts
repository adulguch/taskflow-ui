import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericUiComponent } from './generic-ui.component';

describe('GenericUiComponent', () => {
  let component: GenericUiComponent;
  let fixture: ComponentFixture<GenericUiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenericUiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericUiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
