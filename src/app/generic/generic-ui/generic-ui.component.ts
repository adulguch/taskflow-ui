import { GenericService } from './../services/generic.service';
import { Menu } from './../model/menu';
import { Component, NgZone, OnInit, Input } from '@angular/core';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import * as _ from 'lodash';

@Component({
  selector: 'app-generic-ui',
  templateUrl: './generic-ui.component.html',
  styleUrls: ['./generic-ui.component.scss']
})
export class GenericUiComponent implements OnInit {

  @Input() bridgeId: number;
  private menus: Array<Menu> = [];
  private domainMenus: Array<Menu> = [];
  private operationalMenus: Array<Menu> = [];
  private menusCache: Array<Menu> = [];
  private menuType: number;
  private margin = '0px';
  private width = '0px';
  private selectedMenu: Menu;
  private _opened = false;
  private showSpinner = 1;
  private fail_toaster: any;
  private success_toaster: any;
  private errorMessage: string;

  constructor(private zone: NgZone, private genericService: GenericService, private toasterService: ToasterService) {

    this.fail_toaster = {
      type: 'error',
      title: 'Failed',
      text: 'Data could not be loaded.'
    };

    this.success_toaster = {
      type: 'success',
      title: 'Successful',
      text: 'The record has been deleted successfully.'
    };
  }

  ngOnInit() {
    this.menus = null;
    this.selectedMenu = null;
  }

  ngOnChanges() {
    this.menus = null;
    this.selectedMenu = null;

    this.genericService.getMenus(this.bridgeId).subscribe((data) => {
      this.zone.run(() => {        
        this.menusCache = data;
        // Creating two different menus for domain Tables and Operational tables
        this.domainMenus = _.filter(this.menusCache, (a) => {        
          return a.screenName.toString().length == 2;
        });
        this.operationalMenus = _.filter(this.menusCache, (a) => {        
          return a.screenName.toString().length != 2;
        });
        this.menuType = 1; // Default to domain Tables menuType
        this.onChange(this.menuType);        
        this.showSpinner = 0;
      });
    }, (error) => {
      // console.log(error);
      this.errorMessage = error;
      this.showSpinner = 0;
      this.toasterService.pop(this.fail_toaster.type, this.fail_toaster.title, this.errorMessage);
    });

  }

  private _toggleOpened(): void {
    if (this._opened) {
      this.margin = '0px';
      this.width = '0px';
    } else {
      this.width = '250px';
      this.margin = '250px';
    }
    this._opened = !this._opened;

  }

  private menuOnClick(menu: Menu) {
    this.selectedMenu = menu;
  }

  private onQuickFilterMenuChanged(value) {
    if (this.menuType == 1) {
      if (value) {
      this.menus = _.filter(this.domainMenus, (a) => {
        var re = new RegExp(value, 'i');
        return a.title.toString().match(re);
      });
      } else {
        this.menus = this.domainMenus;
      }
    }
    else if (this.menuType == 2) {
      if (value) {
        this.menus = _.filter(this.operationalMenus, (a) => {
        var re = new RegExp(value, 'i');
        return a.title.toString().match(re);
      });
      } else {
      this.menus = this.operationalMenus;
      }
    }  
  }

  private onChange(item: number) {    
    if (item == 1) {      
      this.menus = this.domainMenus;
      this.menuType = 1;
    }
    else if (item == 2) {      
      this.menus = this.operationalMenus;
      this.menuType = 2;
    }
  }

}
