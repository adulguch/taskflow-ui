import { BaseComponent } from './base-component';
export class TextboxComponent extends BaseComponent<string> {
    controlType = 'textbox';
    type: string;

    constructor(options: {} = {}) {
        super(options);
        this.type = options['type'] || '';
    }
}
