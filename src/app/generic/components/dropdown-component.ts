import { BaseComponent } from './base-component';
export class DropdownComponent extends BaseComponent<string> {
    controlType = 'dropdown';
    options: { key: string, value: string }[] = [];

    constructor(options: {} = {}) {
        super(options);
        this.options = options['options'] || [];
    }
}
