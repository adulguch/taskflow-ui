import { AuthHttp } from 'angular2-jwt';
import { ScreenConfig } from './../model/screen-config';
import { Menu } from './../model/menu';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { AuthenticationService } from '../../services/authentication.service';
import { Router } from '@angular/router';
import { handleErrorBaseServices } from '../../services/handleErrorBaseServices.service';
@Injectable()
export class GenericService extends handleErrorBaseServices {

  constructor(private http: AuthHttp
    , public authenticationService: AuthenticationService 
    , public router : Router
  ) {
    super(authenticationService , router );
    }

  getScreenMetaData(bridgeId: number, screenId: string): Observable<ScreenConfig> {
    if(this.checkAuth()){
      // assets/server/generic/screen-metadata.json
      return this.http.get('api/genericConfig/' + bridgeId + '/' + screenId)
        .map((response: Response) => response.json().data);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
    }  
  }

  getData(bridgeId: number, screenId: string, params: any): Observable<Array<Menu>> {
    // if(this.checkAuth()){
      // 'assets/server/generic/screen-data.json'
      return this.http.get('api/getGenericdata/' + bridgeId + '/' + screenId).map(this.extractData)
        .catch(this.handleError);
    //   }else {
    //     return this.http.get('').map((response: Response) => response.json().data);
    //  }  
  }

  getMenus(bridgeId: number): Observable<Array<Menu>> {
    if(this.checkAuth()){
      // assets/server/generic/menu.json
      return this.http.get('api/genericMenus/' + bridgeId).map(this.extractData)
        .catch(this.handleError);
      }else {
        return this.http.get('').map((response: Response) => response.json().data);
      }  
  }

  public extractData(res: Response) {
    const body = res.json();
    return body.data || {};
  }

  public handleError(error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    const body = error.json() || '';
    if (body.message) {
      errMsg = body.message ? body.message : error.toString();
    } else {
      errMsg = error.status + ': Error occur on server.';
    }

    errMsg = error.status + ': ' +error.statusText+ '. '+error._body+'.';    
    //console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
