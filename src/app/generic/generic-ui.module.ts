import { GenericTableDirective } from './generic-container/generic-table-directive';
import { GenericService } from './services/generic.service';
import { SharedModule } from './../shared/shared.module';
import { DataTableModule } from 'angular2-datatable';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenericUiComponent } from './generic-ui/generic-ui.component';
import { GenericContainerComponent } from './generic-container/generic-container.component';
import { GenericTableComponent } from './generic-container/generic-table/generic-table.component';
import { TooltipModule } from 'ngx-bootstrap';
import { StyleSafePipe } from './pipes/style-safe.pipe';
//import { RoundPipe } from './pipes/round.pipe';
//import { DatexPipePipe } from './pipes/datex-pipe.pipe';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    DataTableModule,
    TooltipModule
  ],
  declarations: [GenericUiComponent, GenericContainerComponent, GenericTableComponent, GenericTableDirective, StyleSafePipe],
  providers: [GenericService],
  entryComponents: [GenericTableComponent],
  exports: [
    GenericUiComponent
  ]
})
export class GenericModule { }
