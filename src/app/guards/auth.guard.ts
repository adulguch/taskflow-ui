import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthenticationService } from 'app/services/authentication.service';



@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router , private  authenticationService: AuthenticationService ) { }
  canActivate( next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    //console.log("in auth ");
       
     if(this.authenticationService.loggedIn()) {
       /////////////////console.log("***((()))***" + this.authenticationService.redirectUrl);
        if (this.authenticationService.redirectUrl) {
              this.router.navigate([this.authenticationService.redirectUrl]);
              this.authenticationService.redirectUrl = null;
         }  
      return true;
    } else {
          this.authenticationService.redirectUrl = state.url ;
          ////console.log("URL : " + this.authenticationService.redirectUrl) ;
          this.router.navigateByUrl('/login');
        return false;
    }

/*    if (localStorage.getItem('currentUser')) {
      // logged in so return true
      return true;
    }

    // not logged in so redirect to login page
    this.router.navigate(['/login']);
    return false;
  */
  }
}



